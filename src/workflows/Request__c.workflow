<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Cancel_Request</fullName>
        <description>Cancel Request</description>
        <protected>false</protected>
        <recipients>
            <field>Requesting_Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>iris/Cancel_Request</template>
    </alerts>
    <alerts>
        <fullName>Cancel_Request_Successfully_Processed</fullName>
        <description>Cancel Request Successfully Processed</description>
        <protected>false</protected>
        <recipients>
            <field>Requesting_Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>iris/Cancel_Request_Successfully_Processed</template>
    </alerts>
    <alerts>
        <fullName>Cancel_Request_Successfully_Submitted</fullName>
        <description>Cancel Request Successfully Submitted</description>
        <protected>false</protected>
        <recipients>
            <field>Requesting_Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>iris/Cancel_Request_Successfully_Submitted</template>
    </alerts>
    <alerts>
        <fullName>On_Hold_Request_Successfully_Processed</fullName>
        <description>On Hold Request Successfully Processed</description>
        <protected>false</protected>
        <recipients>
            <field>Requesting_Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>iris/On_Hold_Request_Successfully_Processed</template>
    </alerts>
    <alerts>
        <fullName>On_Hold_Request_Successfully_Submitted</fullName>
        <description>On Hold Request Successfully Submitted</description>
        <protected>false</protected>
        <recipients>
            <field>Requesting_Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>iris/On_Hold_Request_Successfully_Submitted</template>
    </alerts>
    <alerts>
        <fullName>Request_Assignment_Notification</fullName>
        <description>Request Assignment Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>iris/Request_Assignment_Notification</template>
    </alerts>
    <alerts>
        <fullName>Request_Successfully_Submitted</fullName>
        <description>Request Successfully Submitted</description>
        <protected>false</protected>
        <recipients>
            <field>Requesting_Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>iris/Request_Successfully_Submitted</template>
    </alerts>
    <fieldUpdates>
        <fullName>Request_Name</fullName>
        <description>Concatenate the First 10 characters of the Counterparty/Supplier_Record type_ date</description>
        <field>Name</field>
        <formula>LEFT(Counterparty__c, 10)  &amp; &quot;/&quot; &amp; Customer_Supplier_Name__c &amp; &quot;_&quot; &amp; RecordType.Name &amp; &quot;_&quot; &amp;  TEXT(TODAY())</formula>
        <name>Request Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Secrecy_Group_name_update</fullName>
        <field>Secrecy_Group_Name__c</field>
        <formula>Secrecy_Group__c</formula>
        <name>Secrecy Group name update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Owner_to_Request_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>Request_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Set Owner to Request Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Stage</fullName>
        <field>Stage__c</field>
        <literalValue>Accepted</literalValue>
        <name>Set Stage</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Stage_To_Request</fullName>
        <field>Stage__c</field>
        <literalValue>Request</literalValue>
        <name>Set Stage To Request</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status</fullName>
        <field>Status__c</field>
        <literalValue>Complete</literalValue>
        <name>Set Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_To_Pending</fullName>
        <field>Status__c</field>
        <literalValue>Pending</literalValue>
        <name>Set Status To Pending</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Step</fullName>
        <field>Step__c</field>
        <literalValue>Accepted</literalValue>
        <name>Set Step</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Step_To_Initiate</fullName>
        <field>Step__c</field>
        <literalValue>Initiate</literalValue>
        <name>Set Step To Initiate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Stop_Time</fullName>
        <field>Stop_Time__c</field>
        <formula>NOW()</formula>
        <name>Set Stop Time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sharing_Method_Name</fullName>
        <description>Sharing Method Name field populated from Sharing method formula field</description>
        <field>Sharing_Method_Name__c</field>
        <formula>Sharing_Method__c</formula>
        <name>Sharing Method Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Request_Previous_Owner</fullName>
        <description>sets previous owner Id on the request record when owner is changed.</description>
        <field>Previous_Owner_Id__c</field>
        <formula>PRIORVALUE( OwnerId )</formula>
        <name>Update Request Previous Owner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Start_Time</fullName>
        <field>Start_Time__c</field>
        <formula>CreatedDate</formula>
        <name>Update Start Time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Stop_Time</fullName>
        <field>Stop_Time__c</field>
        <formula>NOW()</formula>
        <name>Update Stop Time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Cancel Request</fullName>
        <actions>
            <name>Cancel_Request</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Will alert the Requestor that their cancel request has been successfully submitted</description>
        <formula>ISPICKVAL(Step__c,&quot;Cancelled&quot;) &amp;&amp;  Requesting_Contact__r.Cancelled_Request__c = true</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Cancel Request Successfully Processed</fullName>
        <actions>
            <name>Cancel_Request_Successfully_Processed</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Will alert the requestor that their request has officially been cancelled</description>
        <formula>Officially_Cancelled__c  = &quot;complete&quot;  &amp;&amp;  Requesting_Contact__r.Officially_Cancelled__c  = true</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Cancel Request Successfully Submitted</fullName>
        <actions>
            <name>Cancel_Request_Successfully_Submitted</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Email alert to Requestor to notify them that their cancel request has been successfully submitted</description>
        <formula>ISPICKVAL(Step__c,&quot;Cancelled&quot;) &amp;&amp; Requesting_Contact__r.Cancelled_Request__c = true</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Close Request</fullName>
        <actions>
            <name>Set_Stage</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Step</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Stop_Time</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Request__c.Number_of_Open_Matters__c</field>
            <operation>equals</operation>
            <value>0</value>
        </criteriaItems>
        <description>When there are no open Matters related to the Request it is automatically closed.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Date%2FTimestamp for request rejection</fullName>
        <actions>
            <name>Update_Start_Time</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Stop_Time</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Timestamps start and stop time for a request that has been rejected.</description>
        <formula>ISPICKVAL( Step__c , &quot;Reject&quot;) &amp;&amp; ISCHANGED(TS_of_Request_Rejected__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Default Owner upon Creation</fullName>
        <actions>
            <name>Set_Owner_to_Request_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Request__c.Request_Method__c</field>
            <operation>equals</operation>
            <value>Client Portal,Request Portal</value>
        </criteriaItems>
        <description>Default Owner to Request Queue</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Email Notification to new Request Owner</fullName>
        <actions>
            <name>Request_Assignment_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Workflow rule to send an email notification to a new Request Owner</description>
        <formula>ISCHANGED (OwnerId)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>On Hold Request</fullName>
        <actions>
            <name>On_Hold_Request_Successfully_Submitted</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>An email will be sent to the requestor notifying them that their request for on hold status has successfully been submitted</description>
        <formula>ISPICKVAL ( Step__c, &quot;On-Hold&quot;)  &amp;&amp;  Requesting_Contact__r.On_Hold_Request__c  = true</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>On Hold Request Successfully Processed</fullName>
        <actions>
            <name>On_Hold_Request_Successfully_Processed</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>An email will alert the requestor that their request has officially been placed on hold.</description>
        <formula>Officially_On_Hold__c  = &quot;complete&quot;  &amp;&amp;  Requesting_Contact__r.Officially_On_Hold__c  = true</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Recall Request Successfully Processed</fullName>
        <active>true</active>
        <description>An email will be sent to the requestor alerting them that their request has officially been recalled.</description>
        <formula>Officially_Recalled__c  = &quot;complete&quot;  &amp;&amp;  Requesting_Contact__r.Officially_Recalled__c  = true</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Request Has Been Rejected</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Request__c.Reason_for_Rejection__c</field>
            <operation>equals</operation>
            <value>TBD,Other</value>
        </criteriaItems>
        <description>An email will be sent to the requestor notifying them that their request has been rejected and the reason why.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Request Naming Convention</fullName>
        <actions>
            <name>Request_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Concatenate the First 10 characters of the Counterparty/Supplier_Record type_ date</description>
        <formula>1=1</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Request Recalled</fullName>
        <active>true</active>
        <description>An email will be sent to the requestor notifying them that their recall request has successfully been submitted</description>
        <formula>ISPICKVAL(Step__c, &quot;Recalled&quot;) &amp;&amp;  Requesting_Contact__r.Request_Recalled__c = true</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Request Submitted Notification</fullName>
        <actions>
            <name>Request_Successfully_Submitted</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>An email will be sent to the requestor notifying them that their request has successfully been submitted.</description>
        <formula>1=1</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Request field update of Secrecy Group</fullName>
        <actions>
            <name>Secrecy_Group_name_update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Sharing_Method_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Request__c.Name</field>
            <operation>notEqual</operation>
            <value>Blank</value>
        </criteriaItems>
        <description>Secrecy group and Sharing Method text field to populate from Secrecy Group and Sharing Method formula field</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Request Previous Owner</fullName>
        <actions>
            <name>Update_Request_Previous_Owner</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Saves the ID of the previous owner of this record for reverting ownership if required.</description>
        <formula>ISCHANGED( OwnerId )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <tasks>
        <fullName>Cancel_Request</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Cancel Request</subject>
    </tasks>
    <tasks>
        <fullName>Recall_Request</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Recall Request</subject>
    </tasks>
    <tasks>
        <fullName>Request_Needing_Your_Acceptance</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Request Needing Your Acceptance</subject>
    </tasks>
    <tasks>
        <fullName>Request_On_Hold</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Request On Hold</subject>
    </tasks>
</Workflow>
