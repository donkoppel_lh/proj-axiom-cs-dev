<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Changes_Needed</fullName>
        <ccEmails>christen.sisler@redkitetechnologies.com</ccEmails>
        <description>Changes Needed</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>iris/Changes_Needed_Requester</template>
    </alerts>
    <alerts>
        <fullName>Matter_Assignment_Notification</fullName>
        <description>Matter Assignment Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>iris/Matter_Assignment_Notification</template>
    </alerts>
    <alerts>
        <fullName>QA_Review_Approved</fullName>
        <description>QA Review Approved</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>iris/QA_Review_Approved</template>
    </alerts>
    <fieldUpdates>
        <fullName>Clear_Primary_Activity_Plan_Field</fullName>
        <field>Primary_Activity_Plan__c</field>
        <name>Clear Primary Activity Plan Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Default_MattervOwner_upon_Creation</fullName>
        <field>OwnerId</field>
        <lookupValue>admin@cs.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Default MattervOwner upon Creation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Default_Owner_upon_Creation</fullName>
        <field>OwnerId</field>
        <lookupValue>Matters</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Default Owner upon Creation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Matter_Naming_Convention</fullName>
        <description>Concatenate Request Action_Date</description>
        <field>Name</field>
        <formula>LEFT(Request__r.Customer_Supplier_Name__c, 10) &amp; TEXT(Request_Action__c) &amp; &quot;_&quot; &amp;   TEXT(TODAY())</formula>
        <name>Matter Naming Convention</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Primary_Activity_Plan</fullName>
        <field>Primary_Activity_Plan__c</field>
        <formula>&quot;Standard Checklist&quot;</formula>
        <name>Primary Activity Plan</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Stop_Time</fullName>
        <field>Stop_Time__c</field>
        <formula>NOW()</formula>
        <name>Set Stop Time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_field_to_null</fullName>
        <field>Secondary_Activity_Plan__c</field>
        <name>Set field to null</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Stage_1_Update</fullName>
        <field>Stage_1__c</field>
        <formula>NOW()</formula>
        <name>Stage 1 Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Matter_Previous_Owner</fullName>
        <description>Sets previous owner Id on the matter record when owner is changed.</description>
        <field>Previous_Owner_Id__c</field>
        <formula>PRIORVALUE( OwnerId )</formula>
        <name>Update Matter Previous Owner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Changes Needed</fullName>
        <actions>
            <name>Changes_Needed</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Matter__c.Stage__c</field>
            <operation>equals</operation>
            <value>Finalize</value>
        </criteriaItems>
        <criteriaItems>
            <field>Matter__c.Step__c</field>
            <operation>equals</operation>
            <value>QA Review</value>
        </criteriaItems>
        <criteriaItems>
            <field>Matter__c.Status__c</field>
            <operation>equals</operation>
            <value>Rejected</value>
        </criteriaItems>
        <description>An email alert to the ATM notifying them that their QA Review has received a &quot;rejection&quot; status</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Clear Primary Activity Plan Field</fullName>
        <actions>
            <name>Clear_Primary_Activity_Plan_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Resets the Primary_Activity_Plan__c field to &quot;null&quot;</description>
        <formula>Primary_Activity_Plan__c  &lt;&gt; null</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Clear Secondary Activity Plan Field</fullName>
        <actions>
            <name>Set_field_to_null</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Resets the Secondary_Activity_Plan__c field to &quot;null&quot;</description>
        <formula>Secondary_Activity_Plan__c  &lt;&gt; null</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Default MattervOwner upon Creation</fullName>
        <actions>
            <name>Default_Owner_upon_Creation</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Matter__c.Request_Method__c</field>
            <operation>equals</operation>
            <value>Request Portal,Client Portal</value>
        </criteriaItems>
        <description>Default owner upon creation to Matter Queue</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Default Owner upon Creation</fullName>
        <actions>
            <name>Default_MattervOwner_upon_Creation</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Matter__c.Request_Method__c</field>
            <operation>equals</operation>
            <value>Request Portal,Client Portal</value>
        </criteriaItems>
        <description>Default owner to &quot;Matter Queue&quot; upon creation when the record is created via Request Portal or Client Portal</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Email Notification to new Matter Owner</fullName>
        <actions>
            <name>Matter_Assignment_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Workflow rule to send an email notification to a new Matter Owner</description>
        <formula>ISCHANGED (OwnerId)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Matter Naming Convention</fullName>
        <actions>
            <name>Matter_Naming_Convention</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Matter__c.Name</field>
            <operation>equals</operation>
            <value></value>
        </criteriaItems>
        <description>Concatenate Request Action_Date</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>QA Review Approved</fullName>
        <actions>
            <name>QA_Review_Approved</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Matter__c.Stage__c</field>
            <operation>equals</operation>
            <value>Finalize</value>
        </criteriaItems>
        <criteriaItems>
            <field>Matter__c.Step__c</field>
            <operation>equals</operation>
            <value>QA Review</value>
        </criteriaItems>
        <criteriaItems>
            <field>Matter__c.Status__c</field>
            <operation>equals</operation>
            <value>Complete</value>
        </criteriaItems>
        <description>An email alert to the ATM notifying them that their QA Review has been approved</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Matter Previous Owner</fullName>
        <actions>
            <name>Update_Matter_Previous_Owner</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Saves the ID of the previous owner of this record for reverting ownership if required.</description>
        <formula>ISCHANGED(  OwnerId  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Matter Stop Time</fullName>
        <actions>
            <name>Set_Stop_Time</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Matter__c.Stage__c</field>
            <operation>equals</operation>
            <value>Complete</value>
        </criteriaItems>
        <criteriaItems>
            <field>Matter__c.Step__c</field>
            <operation>equals</operation>
            <value>Complete</value>
        </criteriaItems>
        <criteriaItems>
            <field>Matter__c.Status__c</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <description>Date/Time stamps the Stop Time field on the Matter with the S3 equals the &quot;closed&quot; status</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Stage 1 Update</fullName>
        <actions>
            <name>Stage_1_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Matter__c.Stage__c</field>
            <operation>equals</operation>
            <value>Allocate</value>
        </criteriaItems>
        <criteriaItems>
            <field>Matter__c.Stage_1__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Date/Time Stamps the Stage 1 field when the Matter Stage field equals&quot;Allocate&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Trigger Standard Activity Plan</fullName>
        <actions>
            <name>Primary_Activity_Plan</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Matter__c.Start_Time__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <tasks>
        <fullName>Commercial_Support_3</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Commercial Support [3]</subject>
    </tasks>
    <tasks>
        <fullName>Create_New_Version_Record</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Create New Version Record</subject>
    </tasks>
    <tasks>
        <fullName>General_Request_Info_2</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>General Request Info [2]</subject>
    </tasks>
    <tasks>
        <fullName>Matter_Review</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Matter Review</subject>
    </tasks>
    <tasks>
        <fullName>Obtain_Client_Signature</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Obtain Client Signature</subject>
    </tasks>
    <tasks>
        <fullName>Obtain_Counterparty_Signatures</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Obtain Counterparty Signatures</subject>
    </tasks>
    <tasks>
        <fullName>Purchase_Support_4</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Purchase Support [4]</subject>
    </tasks>
    <tasks>
        <fullName>Requestor_Information_1</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Requestor Information [1]</subject>
    </tasks>
    <tasks>
        <fullName>Upload_Finalized_Version</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Upload Finalized Version</subject>
    </tasks>
</Workflow>
