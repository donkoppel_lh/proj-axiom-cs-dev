<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Secrecy_Group_populated_from_Leg_Ent</fullName>
        <description>Secrecy Group updated from Legal Entity Roll</description>
        <field>Secrecy_Group__c</field>
        <formula>TEXT(Legal_Entity_Name__r.Secrecy_Group__c)</formula>
        <name>Secrecy Group populated from Leg Ent</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Set Secrecy Group</fullName>
        <actions>
            <name>Secrecy_Group_populated_from_Leg_Ent</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set Secrecy Group value using the value from Legal Entity</description>
        <formula>True</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
