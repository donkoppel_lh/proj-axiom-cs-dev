<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Change_Task_Status_on_Reassign</fullName>
        <description>Change the task status to Assigned</description>
        <field>Status</field>
        <literalValue>Assigned</literalValue>
        <name>Change Task Status on Reassign</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Check_Open_Clockstopper_Field</fullName>
        <field>Open_Clockstopper__c</field>
        <literalValue>1</literalValue>
        <name>Check Open Clockstopper Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Start_Time</fullName>
        <field>Start_Time__c</field>
        <formula>NOW()</formula>
        <name>Set Start Time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status</fullName>
        <field>Status</field>
        <literalValue>In Progress</literalValue>
        <name>Set Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Stop_Time</fullName>
        <field>Stop_Time__c</field>
        <formula>NOW()</formula>
        <name>Set Stop Time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Unset_Apex_Context_for_Task</fullName>
        <field>Apex_Context__c</field>
        <literalValue>0</literalValue>
        <name>Unset Apex Context for Task</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>uncheck_open_clockstopper_field</fullName>
        <field>Open_Clockstopper__c</field>
        <literalValue>0</literalValue>
        <name>uncheck open clockstopper field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Change Task Status on Reassign</fullName>
        <actions>
            <name>Change_Task_Status_on_Reassign</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When a task&apos;s owner is changed the status field will be changed to assigned.</description>
        <formula>ISCHANGED(  OwnerId  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Notify Assignee when QC task is assigned</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>contains</operation>
            <value>QC</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Status</field>
            <operation>equals</operation>
            <value>In Progress</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Task Start Time on Complete</fullName>
        <actions>
            <name>Set_Start_Time</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.Status</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Start_Time__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Task Stop Time on Complete</fullName>
        <actions>
            <name>Set_Stop_Time</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.Status</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Stop_Time__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Uncheck Open Clockstopper for Complete Tasks</fullName>
        <actions>
            <name>uncheck_open_clockstopper_field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.RecordTypeId</field>
            <operation>equals</operation>
            <value>Clockstopper</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Status</field>
            <operation>equals</operation>
            <value>Completed,Approved,Rejected</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Open ClockStopper Field</fullName>
        <actions>
            <name>Check_Open_Clockstopper_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.RecordTypeId</field>
            <operation>equals</operation>
            <value>Clockstopper</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
