<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <content>ActivityPlans_Rule</content>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Visualforce</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <content>ActivityPlans_Rule</content>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Visualforce</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <content>ActivityPlans_Rule</content>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Visualforce</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>The rules attached to an activity plan that must be met for the related activity plan task templates to be assigned.</description>
    <enableActivities>true</enableActivities>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <fields>
        <fullName>Activity_Plan_Task_Template__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>The individual task this rule is related to.If blank it is a rule that applies to the whole plan. If populated it applies to this plan and this specific task.</description>
        <externalId>false</externalId>
        <inlineHelpText>The individual task this rule is related to.If blank it is a rule that applies to the whole plan. If populated it applies to this plan and this specific task.</inlineHelpText>
        <label>Activity Plan Task Template</label>
        <lookupFilter>
            <active>true</active>
            <errorMessage>Selected Task Template must be attached to the same Activity Plan as this rule.</errorMessage>
            <filterItems>
                <field>Activity_Plan_Task_Template__c.Id</field>
                <operation>equals</operation>
                <valueField>Activity_Plan_Task_Template__c.Id</valueField>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>Activity_Plan_Task_Template__c</referenceTo>
        <relationshipLabel>Activity Plan Rules</relationshipLabel>
        <relationshipName>Activity_Plan_Rules</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Activity_Plan__c</fullName>
        <description>The activity plan this rule applies to</description>
        <externalId>false</externalId>
        <inlineHelpText>The activity plan this rule applies to</inlineHelpText>
        <label>Activity Plan</label>
        <referenceTo>Activity_Plan__c</referenceTo>
        <relationshipLabel>Activity Plan Rules</relationshipLabel>
        <relationshipName>Activity_Plan_Rules</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Comparison_Type__c</fullName>
        <description>Where is the source data coming from for the comparison? Either a static value entered here, or compare to another field on the same record.</description>
        <externalId>false</externalId>
        <inlineHelpText>Where is the source data coming from for the comparison? Either a static value entered here, or compare to another field on the same record.</inlineHelpText>
        <label>Comparison Type</label>
        <picklist>
            <picklistValues>
                <fullName>Static Value</fullName>
                <default>true</default>
            </picklistValues>
            <picklistValues>
                <fullName>Object Field Value</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Comparison_Value__c</fullName>
        <description>The value to run the logical check against to see test if it returns true</description>
        <externalId>false</externalId>
        <inlineHelpText>The value to run the logical check against to see test if it returns true</inlineHelpText>
        <label>Comparison Value</label>
        <length>255</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Field_Name__c</fullName>
        <description>The API name of the field on the parent object to evaluate</description>
        <externalId>false</externalId>
        <inlineHelpText>The API name of the field on the parent object to evaluate</inlineHelpText>
        <label>Field Name</label>
        <length>255</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Logical_Operator__c</fullName>
        <description>The logical operation to perform on this field</description>
        <externalId>false</externalId>
        <inlineHelpText>The logical operation to perform on this field</inlineHelpText>
        <label>Logical Operator</label>
        <picklist>
            <picklistValues>
                <fullName>Equal</fullName>
                <default>true</default>
            </picklistValues>
            <picklistValues>
                <fullName>Not Equal</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Greater than</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Less Than</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Contains</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Does Not Contain</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <label>Activity Plan Rule</label>
    <listViews>
        <fullName>All</fullName>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <displayFormat>APR-{00000}</displayFormat>
        <label>Activity Plan Rule Name</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Activity Plan Rules</pluralLabel>
    <recordTypes>
        <fullName>Activity_Plan_Rule</fullName>
        <active>true</active>
        <description>A rule that applies for an entire activity plan</description>
        <label>Activity Plan Rule</label>
        <picklistValues>
            <picklist>Comparison_Type__c</picklist>
            <values>
                <fullName>Object Field Value</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Static Value</fullName>
                <default>true</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>Logical_Operator__c</picklist>
            <values>
                <fullName>Contains</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Does Not Contain</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Equal</fullName>
                <default>true</default>
            </values>
            <values>
                <fullName>Greater than</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Less Than</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Not Equal</fullName>
                <default>false</default>
            </values>
        </picklistValues>
    </recordTypes>
    <recordTypes>
        <fullName>Activity_Plan_Task_Template_Rule</fullName>
        <active>true</active>
        <description>A rule that applies to a single task template</description>
        <label>Activity Plan Task Template Rule</label>
        <picklistValues>
            <picklist>Comparison_Type__c</picklist>
            <values>
                <fullName>Object Field Value</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Static Value</fullName>
                <default>true</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>Logical_Operator__c</picklist>
            <values>
                <fullName>Contains</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Does Not Contain</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Equal</fullName>
                <default>true</default>
            </values>
            <values>
                <fullName>Greater than</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Less Than</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Not Equal</fullName>
                <default>false</default>
            </values>
        </picklistValues>
    </recordTypes>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
</CustomObject>
