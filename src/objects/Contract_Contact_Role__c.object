<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Junction between Contact and Matter linking all client and counterparty contacts to the matter.</description>
    <enableActivities>false</enableActivities>
    <enableEnhancedLookup>false</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <fields>
        <fullName>Contact__c</fullName>
        <externalId>false</externalId>
        <label>Contact</label>
        <referenceTo>Contact</referenceTo>
        <relationshipName>Contract_Contact_Roles</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Description__c</fullName>
        <externalId>false</externalId>
        <label>Description</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>External_ID__c</fullName>
        <caseSensitive>false</caseSensitive>
        <externalId>true</externalId>
        <label>External ID</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>Legal_Notice__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Legal Notice</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Matter__c</fullName>
        <externalId>false</externalId>
        <label>Matter</label>
        <referenceTo>Matter__c</referenceTo>
        <relationshipLabel>Matter Contact Roles</relationshipLabel>
        <relationshipName>Contract_Contact_Roles</relationshipName>
        <relationshipOrder>1</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Notice__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Notice</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Primary__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <inlineHelpText>Primary contact for client or counterparty</inlineHelpText>
        <label>Primary?</label>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Role__c</fullName>
        <externalId>false</externalId>
        <label>Role</label>
        <picklist>
            <picklistValues>
                <fullName>Requestor</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Contract Manager</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Single Point of Contact</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Stakeholder</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>TBD</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Signed_Date__c</fullName>
        <externalId>false</externalId>
        <label>Signed Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Signed_Title__c</fullName>
        <externalId>false</externalId>
        <label>Signed Title</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Signed__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Signed</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <label>Matter Contact Role</label>
    <nameField>
        <displayFormat>CCR-{000000000}</displayFormat>
        <label>Matter Contact Role</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Matter Contact Roles</pluralLabel>
    <recordTypeTrackHistory>false</recordTypeTrackHistory>
    <recordTypes>
        <fullName>Client</fullName>
        <active>true</active>
        <description>Axiom Client</description>
        <label>Client</label>
        <picklistValues>
            <picklist>Role__c</picklist>
            <values>
                <fullName>Contract Manager</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Requestor</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Single Point of Contact</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Stakeholder</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>TBD</fullName>
                <default>false</default>
            </values>
        </picklistValues>
    </recordTypes>
    <recordTypes>
        <fullName>Counterparty</fullName>
        <active>true</active>
        <description>Counterparty of Axiom Client</description>
        <label>Counterparty</label>
        <picklistValues>
            <picklist>Role__c</picklist>
            <values>
                <fullName>Contract Manager</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Requestor</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Single Point of Contact</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Stakeholder</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>TBD</fullName>
                <default>false</default>
            </values>
        </picklistValues>
    </recordTypes>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
</CustomObject>
