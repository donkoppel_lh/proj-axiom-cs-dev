trigger Axiom_LegalEntityTriggers on Legal_Entity__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
    if( (Limits.getLimitQueries() - Limits.getQueries()) > 10 && Axiom_LegalEntityController.invokeCount < 3) 
    {
        Axiom_Misc_Settings__c settings = Axiom_Misc_Settings__c.getOrgDefaults();
        
        Axiom_LegalEntityController.invokeCount++;   
        
        if(Trigger.isBefore)
        {        
        }
        
        if(Trigger.isAfter)
        {     
            if(Trigger.isUpdate)
            {
                Axiom_LegalEntityController.updateSecrecyGroup(trigger.new, trigger.old);
            }
        }
    }
}