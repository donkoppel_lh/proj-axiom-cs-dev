trigger Axiom_VersionTriggers on Version__c (after insert, after update) 
{
    Axiom_VersionController.updateTaskOnSectionComplete(trigger.new);
}