trigger Axiom_MatterTriggers on Matter__c (before insert, before update, after insert, after update, after delete, after undelete) 
{
    if( (Limits.getLimitQueries() - Limits.getQueries()) > 10 && Axiom_MatterController.invokeCount < 3) 
    {
        Axiom_Misc_Settings__c settings = Axiom_Misc_Settings__c.getOrgDefaults();
        
        Axiom_MatterController.invokeCount++;   
        if(Trigger.isBefore)
        {        
            if(Trigger.isInsert)
            {
                if(settings.Init_Matter_Fields_To_Request_Fields__c)
                {
                    Axiom_MatterController.initMatterToRequest(Trigger.New);            
                }
            }
            
            if(Trigger.isUpdate)
            {
            	Axiom_MatterController.validateMatterName(trigger.new, trigger.old);
            }
            
            Axiom_MatterController.setNumberOfDaysOpen(trigger.new);
            Axiom_MatterController.setNumberOfDaysToFsr(trigger.new);        
        }
        
        if(Trigger.isAfter)
        {     
            if(!Trigger.isDelete)
            {
                ActivityPlansController.findMatchingActivityPlans(Trigger.New);
                Axiom_MatterController.setOpenClockStoppersFlagOnRequest(trigger.new);
                Axiom_MatterController.setNumMattersOnRequest(trigger.new);
            }
            else if(Trigger.isDelete)
            {
                Axiom_MatterController.setNumMattersOnRequest(trigger.old);
                Axiom_MatterController.setOpenClockStoppersFlagOnRequest(trigger.old);
            }
        }
    }
}