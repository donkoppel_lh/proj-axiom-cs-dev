trigger Axiom_GroupEntitlementTriggers on Group_Entitlement__c (before insert, before update) 
{
    map<string,id> profileNameMap = new map<string,id>();
    map<string,id> groupNameMap = new map<string,id>();
    //map<id,string> profileIdMap = new map<id,string>();
    map<id,string> groupIdMap = new map<id,string>();

    for(Group_Entitlement__c record : trigger.new)
    {
        if(record.Group_Id__c != null && record.Group_Name__c == null) groupIdMap.put(record.Group_Id__c,null);
        //if(record.Profile_Id__c != null && record.Profile_Name__c == null) ProfileIdMap.put(record.Profile_Id__c,null);
        if(record.Group_Id__c == null && record.Group_Name__c != null) groupNameMap.put(record.Group_Name__c,null);
        //if(record.Profile_Id__c == null && record.Profile_Name__c != null) ProfileNameMap.put(record.Profile_Name__c,null);    
    }

/*    
    for(Profile profiles : [select name, id from profile where name in :profileNameMap.values() or id in :profileIdMap.keySet()])
    {
    
        for(Group_Entitlement__c record : trigger.new)
        {
            if(record.Profile_Id__c == profiles.id || record.Profile_Name__c == profiles.name)
            {
                record.Profile_Id__c = profiles.id;
                record.Profile_Name__c = profiles.name;
            }
        }
    }
*/
    for(Group groups : [select name, id from group where name in :groupNameMap.values() or id in :groupIdMap.keySet()])
    {
        for(Group_Entitlement__c record : trigger.new)
        {
            if(record.Group_Id__c == groups.id || record.Group_Name__c == groups.name)
            {
                record.Group_Id__c = groups.id;
                record.Group_Name__c = groups.name;
            }        
        }    
    }
    
    for(Group_Entitlement__c record : trigger.new)
    {
        if(record.Group_Id__c == null || record.Group_Name__c == null)
        {
            record.addError('Invalid Group Name or ID');
        }        
        //if(record.Profile_Id__c == null || record.Profile_Name__c == null)
        //{
        //    record.addError('Invalid Profile Name or ID');
        //}     
    }
    
}