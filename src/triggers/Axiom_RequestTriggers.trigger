trigger Axiom_RequestTriggers on Request__c (before update, after insert, after update) 
{
    if( (Limits.getLimitQueries() - Limits.getQueries()) > 10 && Axiom_RequestController.invokeCount < 3) 
    {

        Axiom_Misc_Settings__c settings = Axiom_Misc_Settings__c.getOrgDefaults();

        Axiom_RequestController.invokeCount++;
        if(Trigger.isBefore)
        {
            if(Trigger.isUpdate)
            {
                Axiom_Utilities.checkUserEntitlement(Trigger.newMap, Trigger.oldMap, 'ownerId');
            }
            Axiom_RequestController.setNumberOfDaysToFsr(Trigger.new);
        }
        if(Trigger.isAfter)
        {
            if(Trigger.isinsert)
            {
                if(settings.Reassign_Matter_Owners_to_Request_Owner__c)
                {
                    Axiom_RequestController.changeMatterOwnersToRequestOwner(new map<id,Request__c>(), trigger.newMap);
                }
            }
            else if(Trigger.isUpdate)
            {
                if(settings.Reassign_Matter_Owners_to_Request_Owner__c)
                {
                    Axiom_RequestController.changeMatterOwnersToRequestOwner(trigger.oldMap, trigger.newMap);
                }
                
                Axiom_RequestController.updateMatterSecrecyGroup(trigger.new, trigger.old);
                Axiom_Utilities.grantGMsSharingToRequestAndMatter(trigger.new, trigger.oldMap);
            }
            ActivityPlansController.findMatchingActivityPlans(Trigger.New);
        }
    }
}