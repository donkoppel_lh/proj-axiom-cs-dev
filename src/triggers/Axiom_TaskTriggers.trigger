trigger Axiom_TaskTriggers on Task (before insert, before update, after insert, after update, after delete) 
{    
    if( (Limits.getLimitQueries() - Limits.getQueries()) > 10 && Axiom_TaskController.invokeCount < 5) 
    {
        Axiom_TaskController.invokeCount++; 
        if(trigger.isBefore)
        {
            //sets default weight on task to prevent errors since field is required.
            Axiom_TaskController.setTaskDefaultWeight_before(trigger.new); 
            Axiom_TaskController.updateMatterNumberofDaysToFSR(trigger.new);
                    
            if(trigger.isInsert)
            {   
                //set the section number based on the last char in the task
                Axiom_TaskController.setTaskSection(trigger.new);
            }
            
            if(trigger.isUpdate)
            {
                Axiom_TaskController.setNumberOfDaysOpen_before(trigger.new);
                Axiom_TaskController.errorOnUpdateOfClosedClockStopper( Trigger.New, Trigger.oldMap );
            }
        }
        else if(trigger.isAfter)
        {    
            if(!Trigger.isDelete)
            {   
                //there is a special kind of task that when it is completed needs to update the matters S3 fields to being completed
                Axiom_TaskController.updateMatterOnAllTasksClosed(trigger.new);
                
                Axiom_TaskController.processTaskUpdateCustomSetting(trigger.new);
                               
                if(trigger.isUpdate)
                {    
                    Axiom_TaskController.updateMatterNumberofDaysToFSR(trigger.new);   
                     
                    ActivityPlan_TaskTriggerHandler.handleFieldUpdates(trigger.newMap, trigger.oldMap);        
                }
                else if(trigger.isInsert)
                {
                    
                    //Axiom_TaskController.createClockStopperFromTask(trigger.newMap);
                    
                    ActivityPlan_TaskTriggerHandler.handleFieldUpdates(trigger.newMap, null);        
                }
                
                ActivityPlan_TaskTriggerHandler.recheckActivityPlans(trigger.new);
                
                Axiom_TaskController.setOpenClockstopperFlagOnParentTask(trigger.new);
                
                Axiom_TaskController.setOpenClockstopperFlagOnMatter(trigger.new);
                
                
            }
            else
            {
                Axiom_TaskController.setOpenClockstopperFlagOnParentTask(trigger.old);
                
                Axiom_TaskController.setOpenClockstopperFlagOnMatter(trigger.old);
            }
        }
    }
}