trigger Axiom_ClockStopperTriggers on Clockstopper__c (before insert, before update, after insert, after update) 
{
 	if( (Limits.getLimitQueries() - Limits.getQueries()) > 10 && Axiom_ClockstopperController.invokeCount < 3) 
    {
        Axiom_ClockstopperController.invokeCount++;  
	    if(Trigger.isBefore)
	    {
	        Axiom_ClockstopperController.calculateNetTime(trigger.new);
	    }
	    if(Trigger.isAfter)
	    {
	         Axiom_ClockstopperController.sumMatterClockstoppers(trigger.new);
	         Axiom_ClockstopperController.setOpenClockstoppersFlagOnTask(trigger.new);
	    }
    }
}