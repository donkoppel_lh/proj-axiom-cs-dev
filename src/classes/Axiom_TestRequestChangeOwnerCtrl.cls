@isTest
private class Axiom_TestRequestChangeOwnerCtrl {
	@isTest
    static void testChangeOwner(){
        Axiom_Utilities.generateTestData();
        
        Test.StartTest();        
        
            Request__c thisRequest = [Select Id, Name From Request__c limit 1];
       		ApexPages.StandardController std = new ApexPages.StandardController( thisRequest );
       		Axiom_RequestChangeOwnerCtrl ctrl = new Axiom_RequestChangeOwnerCtrl( std );
       		ctrl.changeOwner();
        Test.StopTest();
          
    }
}