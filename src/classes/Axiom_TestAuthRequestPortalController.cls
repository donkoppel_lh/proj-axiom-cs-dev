@isTest
global class Axiom_TestAuthRequestPortalController
{
    @isTest
    public static void testAuthRequestPortalController()
    {
        Axiom_Utilities.generateTestData();
        
        Request__c thisRequest = [select id, name from Request__c limit 1];

        
        Test.StartTest();
             
        ApexPages.StandardController sc = new ApexPages.StandardController(thisRequest);
             
        PageReference pageRef = Page.Axiom_AuthRequestPortal;
        Test.setCurrentPage(pageRef);
        Axiom_AuthRequestPortalController controller = new Axiom_AuthRequestPortalController(sc);  
        
        Test.StopTest();      
    }
}