global class Axiom_RequestEntitlementBatchable implements Database.Batchable<sObject> {
	
	/*
	
	 Authors :  David Brandenburg
	 Created Date: 2015-09-04
	 Last Modified: 2014-09-17
	 
	 Purpose:  Batchable class which calls class Axiom_RequestEntitlementBatch for processing.
	*/

	String query = 'select id, Secrecy_Group__c, Sharing_Method__c from Request__c where Sharing_Method__c = \'Manual\' ' ;
	
	global Axiom_RequestEntitlementBatchable() {
		
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
			Axiom_RequestEntitlementBatch.ProcessRecords (scope);
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}
	
}