global class Axiom_RequestEntitlementSchedulable implements Schedulable {

	/*
	
	 Authors :  David Brandenburg
	 Created Date: 2015-09-04
	 Last Modified: 2014-09-17
	 
	 Purpose:  Schedulable class for the batch class called Axiom_RequestEntitlementBatchable
	 
	 Schedule :
	 
		15 mins after the hour
		Axiom_RequestEntitlementSchedulable clsCRON = new  Axiom_RequestEntitlementSchedulable();
		System.Schedule('Request Request Share Batch-15', '0 15 * * * ? ', clsCRON);
	
	
	*/
	global void execute(SchedulableContext sc) {

		Axiom_RequestEntitlementBatchable batchClass = new Axiom_RequestEntitlementBatchable();
		ID idBatch = Database.executeBatch(batchClass, 2);

	}
}