global class ActivityPlans_FieldUpdateController {

    public Activity_Plan_Field_Update__c thisRule { get; set;}
    
    public SelectOption[] allObjectFields      { get; set; }   
    public SelectOption[] allTaskFields      { get; set; }   
    public string selectedSourceField{get;set;}
    public string selectedCompareField{get;set;}
    public string selectedCompareFieldType{get;set;}
    public string parentObjectType{get;set;}
    public String message { get; set; }
    public boolean initError{get;set;}
    public Map<String, Schema.SObjectField> objectFields {get; set;}
    public Map<String, Schema.SObjectField> taskFields {get; set;}
    public string recordTypeName{get;set;}
    public ActivityPlans_Utilities utils = new ActivityPlans_Utilities();
 
    /** used by the task list component to find tasks for the given parent used by the Axiom_TaskRelatedList component. **/
    public id parentObjectId{get;set;} 
    
    /**used by task list component to further filter tasks returned used by the Axiom_TaskRelatedList component**/
    public string relatedTaskFilterString{get;set;} 
    
    /**custom order by statment to be used in the find related tasks query used by the Axiom_TaskRelatedList component**/
    public string orderRelatedTasksBy{get;set;}    
 
    public ActivityPlans_FieldUpdateController(ApexPages.StandardController controller) {
        try
        {
            initError = false;
            thisRule = (Activity_Plan_Field_Update__c) controller.getRecord();
            
            if(thisRule.id != null)
            {
                thisRule = [select Activity_Plan__c,
                               Activity_Plan_Task_Template__c,
                               Activity_Plan_Task_Template__r.Activity_Plan__c,
                               Comparison_Value__c,
                               Comparison_Type__c,
                               Comparison_Field_Name__c,
                               Logical_Operator__c,
                               Name,
                               CreatedDate,
                               CreatedById,
                               Assignment_Type__c,
                               Assignment_Value__c,
                               Assignment_Field_Name__c,
                               Trigger__c,
                               Active__c
                               from Activity_Plan_Field_Update__c where id = :thisRule.id];     
    
            }
            id parentTemplateId = thisRule.Activity_Plan_Task_Template__c != null ? thisRule.Activity_Plan_Task_Template__c :  ApexPages.currentPage().getParameters().get('CF00NC0000005ey5M_lkid');  
            id parentPlanId = thisRule.Activity_Plan__c;
            if(thisRule.Activity_Plan__c == null)
            {
                Activity_Plan_Task_Template__c templateInfo = [select id, Activity_Plan__c from Activity_Plan_Task_Template__c where id =:parentTemplateId];
                parentPlanId = templateInfo.Activity_Plan__c;
                thisRule.Activity_Plan__c = parentPlanId;
            }
            
            
            
            system.debug(parentPlanId);
            if(parentPlanId == null)
            {
                throw new applicationException('Error: Unable to determine parent activity plan. Cannot deduce object type. Please create rules from the Activity Plan Related List');                   
            }                 
            //get parent object type           
            Activity_Plan__c parentPlan = [select id, Object_Types__c from Activity_Plan__c where id = :parentPlanId];
            
            //if we are creating a field update rule, the list of fields to evaluate should be task fields. Otherwise we read from the parent object type fields
            parentObjectType = parentPlan.Object_Types__c;
            
            allObjectFields      = new List<SelectOption>();
            allTaskFields      = new List<SelectOption>();
            selectedSourceField = thisRule.Comparison_Field_Name__c;
            
            String[] types = new String[]{parentObjectType};
            
            //get fields for parent object
            Schema.DescribeSobjectResult[] results = Schema.describeSObjects(types);
            if(!results.isEmpty())
            {            
                
                objectFields = results[0].fields.getMap();

                list<string> fieldNames = new list<string>(objectFields.keySet());
                fieldNames.sort();
                
                for(String field : fieldNames)
                {
                    Schema.DescribeFieldResult dr = objectFields.get(field).getDescribe();
                    allObjectFields.add(new SelectOption(field, dr.getLabel()));
                } 
            } 
            
            //get fields for task
            taskFields = Schema.SObjectType.Task.fields.getMap(); 
            list<string> fieldNames = new list<string>(taskFields.keySet());
            for(String field : fieldNames)
            {
                Schema.DescribeFieldResult dr = taskFields.get(field).getDescribe();
                allTaskFields.add(new SelectOption(field, dr.getLabel()));
            } 
                            
        }
        catch(exception ex)
        {
            initError = true;
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,ex.getMessage());
            ApexPages.addMessage(myMsg);         

        }   
    }

    /**
    * When a user chooses a source field to compare against, we should get data about that field to help the user only create rules which will make sense and not cause errors at runtime. Things like only allowing
    * number fields to have numeric values in the comparison value box if using static value compares, or not allowing a boolean to be compared against a number. Really the number of validations is almost endless
    * so we'll do the best we can to help the user, however I don't pretend to, nor do I aspire to create perfect validations for every possible situation.
    * @param fieldName the API name of the field on the object to get data for
    * @return field describe result/
    **/
    @remoteAction
    global static string getFieldType(string parentObjectType, string fieldName)
    {
        String[] types = new String[]{parentObjectType};
        Schema.DescribeSobjectResult[] results = Schema.describeSObjects(types);
                
        Schema.DescribeFieldResult fieldData = results[0].fields.getMap().get(fieldName).getDescribe();
        return fieldData.getType().name();        

    }

    /**
    * Handles saving of the editing task template record. Any special data massaging that needs to happen to get the data
    * from the UI format to acceptable database format happens here (like constructing a comma separated list of values from an array)
    * happens here.
    * @return pageReference where to return the user after save. Null means return to the same page.
    **/
    public PageReference save() {
        try
        {     
            //get the name of the field to run the comparions against
            
            //ensure the field specified exists on this object
            if(!objectFields.containsKey(thisRule.Assignment_Field_Name__c))
            {
                throw new applicationException('Error: Provided field ' + thisRule.Assignment_Field_Name__c + ' Does not exist on object type ' + parentObjectType + ' Please note you cannot use relationship fields in rules.');
            }

            //ensure the field specified exists on this object
            if(!TaskFields.containsKey(thisRule.Comparison_Field_Name__c))
            {
                throw new applicationException('Error: Provided field ' + thisRule.Comparison_Field_Name__c + ' Does not exist on Task object. Please note you cannot use relationship fields in rules.');
            }
                        
            if(recordTypeName == 'Activity_Plan_Task_Template_Rule' && thisRule.Activity_Plan_Task_Template__c == null)
            {
                throw new applicationException('Error: You must include a related task template');
            }
            

            //Set the comparison value to the propert value
            if(thisRule.Comparison_Type__c == 'Static Value' && thisRule.Comparison_Value__c != null)
            {
                sObjectField fieldData = taskFields.get(thisRule.Comparison_Field_Name__c);

                //Attempt to cast the value provided by the user for comparison to the type of value that the field is. If the casting fails, throw an error. 
                //otherwise it's valid and should work.
                thisRule = (Activity_Plan_Field_Update__c) utils.putValueWithDynamicCasting(thisRule, 'Comparison_Value__c', fieldData, thisRule.Comparison_Value__c);
            }
            else if(thisRule.Comparison_Type__c == 'Object Field Value')
            {
                thisRule.Comparison_Value__c = selectedCompareField;
            }
            
            if(thisRule.Assignment_Type__c == 'Static Value' && thisRule.Assignment_Value__c != null)
            {
                thisRule.Assignment_Value__c = thisRule.Assignment_Value__c;
            }
            else if(thisRule.Assignment_Type__c == 'Object Field Value')
            {
                thisRule.Assignment_Value__c = selectedCompareField;
            }
            
            system.debug(thisRule);
            upsert thisRule;
            
            PageReference newpage = new PageReference('/'+thisRule.id);
    
            newpage.setRedirect(true);
            return newpage;   
        }
        catch(exception ex)
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,ex.getMessage());
            ApexPages.addMessage(myMsg);     
            return null;    

        }          
    }    


    public class applicationException extends Exception {}
}