/******* Axiom Update Clockstopper Batch *********
@Author: Daniel Llewellyn/RedKite (dllewellyn@redkitetechnologies.com)
@Date: ~3/2014
@Description: Handles updating of a fields on the clockstopper; Net_Time__c . Does this for any request where the clockstoppers stop time is null. Meant to be scheduled hourly by the Axiom_Utilities.scheduleBatchJobs() method.
**********************************************/
global class Axiom_UpdateClockStopperBatch implements Database.Batchable < sObject > , Schedulable 
{
    /** Querystring that can be modified after class instantiation. Defauts to finding all requests that have a status__c of "In Progress" */
    public string queryString = 'select name, id, createdDate, Start_Time__c, Stop_Time__c from clockstopper__c where Stop_Time__c = null';
 
     /** 
    * Finds next batch of records to send to the execute method in amounts dictated by the batch size (default 200)
    * @param ctx batch context. An instantiation of this class.
    * @return batch of records for the execute method to process
    **/
    
    global Database.QueryLocator  start(Database.BatchableContext ctx)
    {
        return Database.getQueryLocator(queryString);
    }


    /**
    * Calls one method on the Axiom_RequestController class.
    * 1) Invokes Axiom_ClockStopperController.calculateNetTime() to calculate the number of days between start time and stop time on the clockstopper.
    * see those methods for further description of their logic
    * @param ctx batch context. Is passed automatically by the system.
    * @param scope list of sObjects to operate on.
    * @return null
    **/    
    global void execute(Database.BatchableContext ctx, List <sObject> scope)
    {
        scope = Axiom_ClockStopperController.calculateNetTime(scope);
        
        database.update(scope,false);
    }

    /**
    * Invoked after all batches have been completed. Nothing further needs to be done as this point so we 
    * just put an entry in the system log.
    **/
    global void finish(Database.BatchableContext ctx) 
    {
        System.debug(LoggingLevel.WARN,'Axiom_UpdateClockstopperBatch Process Finished');
      
    }    

    /**
    * This is the method is run when the scheduled job is triggered. It instantiates an instance of this
    * class, and executes the batch job.
    * @param sc SchedulableContext. Passed by salesforce when this scheduled job is triggered to run.
    * @return null
    **/
    global void execute(SchedulableContext SC) 
    {
        Axiom_UpdateClockStopperBatch updateBatch = new Axiom_UpdateClockStopperBatch();
        ID batchprocessid = Database.executeBatch(updateBatch, 50);
    }     
}