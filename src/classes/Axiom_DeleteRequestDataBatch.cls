global class Axiom_DeleteRequestDataBatch implements Database.Batchable<SObject> {
	public List<SObject> listSobject;
	public static final String userId ='005f0000001KQvX';
	public Axiom_DeleteRequestDataBatch(){
		listSobject = new List<SObject>();
		List<String> listObj = new List<String>{
			'Event',
			'Task',
			'Request_Contact_Role__c',
			'Request_Agreement_Title__c',
			'Agreement_Title__c',
			'Legal_Entity_Role__c',
			'Legal_Entity__c',
			'Clause__c',
			'Version__c',
			'Document__c',
			'Matter__c',
			'Request__c',
			'Contact',
			'Account'
		};
		if(listObj != null && listObj.size() > 0){
			for(String Obj:listObj){
				String query = 'SELECT ID FROM ';
					query = query + Obj;
//					query = query + ' WHERE CreatedById '
//							 + ' = \'' 
//							 + userId
					query = query + ' LIMIT 10000'; 
				List<SObject> objRec = Database.query(query);
				if(objRec != null && objRec.size() > 0){
					listSobject.addAll(objRec);
				}
			}
		}
	}

    global Iterable<SObject> start(Database.BatchableContext BC){
    	return listSobject;
    }
    
	global void execute(Database.BatchableContext BC, List<SObject> scope){
	 	if(scope != null && scope.size() > 0){
	 		Database.DeleteResult[] resDels = Database.delete(scope,false);
	 	}
	} 
	global void finish(Database.BatchableContext BC){
	   	
	}  	   
}