global with sharing class UserLookupComponent   {

    @RemoteAction
    global static  UserListClass[] getUserList (String actionType , String requestId , string secrecyGroup) {
        string qry;
        //string secrecyGroup = RequestObj.Secrecy_Group__c ;

        if (actionType == 'GRANT' && secrecyGroup == '')
            qry = 'SELECT Id , Name from User where isActive = true and UserType = \'Standard\' and Id in ( SELECT User__c FROM Request_Entitlement__c WHERE Request__c = \'' + requestId + '\' and Type__c= \'Revoke\' ) Order By Name LIMIT 500 ';

        if (actionType == 'GRANT' && secrecyGroup == 'Singapore Secrecy Group')
            qry = 'SELECT Id , Name from User where isActive = true and UserType = \'Standard\' and Id NOT in (SELECT UserOrGroupId FROM GroupMember WHERE Group.DeveloperName = \'Singapore_Secrecy_Client_Onboarding\' OR Group.DeveloperName = \'Singapore_Secrecy_Legal\' ) Order By Name LIMIT 500 ';

        if (actionType == 'GRANT' && secrecyGroup == 'Seoul Secrecy Group')
            qry = 'SELECT Id , Name from User where isActive = true and UserType = \'Standard\' and Id NOT in (SELECT UserOrGroupId FROM GroupMember WHERE Group.DeveloperName = \'Seoul_Secrecy_Client_Onboarding\' OR Group.DeveloperName = \'Seoul_Secrecy_Legal\' ) Order By Name LIMIT 500 ';

        if (actionType == 'REVOKE' && secrecyGroup == '')
            qry = 'SELECT Id , Name from User where isActive = true and UserType = \'Standard\' and Id not in ( SELECT User__c FROM Request_Entitlement__c WHERE Request__c = \'' + requestId + '\' and Type__c = \'Revoke\' ) Order By Name LIMIT 500 ';

        if (actionType == 'REVOKE' && secrecyGroup == 'Singapore Secrecy Group'   )
            qry = 'SELECT Id , Name from User where isActive = true and UserType = \'Standard\' and Id in (SELECT UserOrGroupId FROM GroupMember WHERE Group.DeveloperName = \'Singapore_Secrecy_Client_Onboarding\' OR Group.DeveloperName = \'Singapore_Secrecy_Legal\' ) Order By Name LIMIT 500 ';

        if (actionType == 'REVOKE' && secrecyGroup == 'Seoul Secrecy Group'   )
            qry = 'SELECT Id , Name from User where isActive = true and UserType = \'Standard\' and Id in (SELECT UserOrGroupId FROM GroupMember WHERE Group.DeveloperName = \'Seoul_Secrecy_Client_Onboarding\' OR Group.DeveloperName = \'Seoul_Secrecy_Legal\' ) Order By Name LIMIT 500 ';

            List<User> tmpList = Database.Query(qry);
            List<User> userList  ;


            if (actionType == 'GRANT' && (secrecyGroup == 'Singapore Secrecy Group' ||  secrecyGroup == 'Seoul Secrecy Group' ) ) {
                userList =  new List<User> ();
                set<string> grantSet = new set<string>() ;

                for (Request_Entitlement__c gl : [SELECT User__c FROM Request_Entitlement__c WHERE Request__c = :requestId and Type__c = 'Grant' ]  ) {
                    grantSet.add(gl.User__c);
                }

                for (User u : tmpList) {
                    if (!grantSet.Contains(u.id) ) {
                        userList.add(u);
                    }
                }
                grantSet = new set<string>() ;
                for (Request_Entitlement__c gl : [SELECT User__c , User__r.Name FROM Request_Entitlement__c WHERE Request__c = :requestId and Type__c = 'Revoke' and User__c
                                                  in (SELECT UserOrGroupId FROM GroupMember WHERE Group.DeveloperName = 'Singapore_Secrecy_Client_Onboarding' OR Group.DeveloperName = 'Singapore_Secrecy_Legal' ) ]  ) {
                    grantSet.add(gl.User__c);
                }

               

               
                userList.addAll([SELECT Id , Name from User where isActive = true and Id in :grantSet]) ;

            } else if (actionType == 'REVOKE' && (secrecyGroup == 'Singapore Secrecy Group' ||  secrecyGroup == 'Seoul Secrecy Group' ) ) {
                userList =  new List<User> ();
                set<string> grantSet = new set<string>() ;
                for (Request_Entitlement__c gl : [SELECT User__c FROM Request_Entitlement__c WHERE Request__c = :requestId and Type__c = 'Revoke']  )
                    grantSet.add(gl.User__c);

                for (User u : tmpList) {
                    if (!grantSet.Contains(u.id) ) {
                        userList.add(u);
                    }
                }
                grantSet = new set<string>() ;

                if (secrecyGroup == 'Singapore Secrecy Group') {
                    for (Request_Entitlement__c gl : [SELECT User__c , User__r.Name FROM Request_Entitlement__c WHERE Request__c = :requestId and Type__c = 'Grant' and User__c
                                                      not in (SELECT UserOrGroupId FROM GroupMember WHERE Group.DeveloperName = 'Singapore_Secrecy_Client_Onboarding' OR Group.DeveloperName = 'Singapore_Secrecy_Legal' ) ]  ) {
                        grantSet.add(gl.User__c);
                    }
                }

                 if (secrecyGroup == 'Seoul Secrecy Group') {
                    for (Request_Entitlement__c gl : [SELECT User__c , User__r.Name FROM Request_Entitlement__c WHERE Request__c = :requestId and Type__c = 'Grant' and User__c
                                                      not in (SELECT UserOrGroupId FROM GroupMember WHERE Group.DeveloperName = 'Seoul_Secrecy_Client_Onboarding' OR Group.DeveloperName = 'Seoul_Secrecy_Legal' ) ]  ) {
                        grantSet.add(gl.User__c);
                    }
                }

                userList.addAll([SELECT Id , Name from User where isActive = true and Id in :grantSet]) ;

            } else {
                userList = tmpList;
            }


        List<UserListClass> returnObj = new  List<UserListClass>();
        for (User usr : UserList) {
        returnObj.add (new UserListClass (usr.id, usr.Name));
        }

        returnObj.sort();
        return returnObj ;

    }


    @RemoteAction
    global static User[] getUserListSearch (String actionType , String requestId , string secrecyGroup , string searchTerm) {
        string qry;

        // ************** Grant Search ***********************/
        if (actionType == 'GRANT' && secrecyGroup == '')
            qry = 'SELECT Id , Name from User where isActive = true and Name like \'%' + searchTerm + '%\' and Id in ( SELECT User__c FROM Request_Entitlement__c WHERE Request__c = \'' + requestId + '\' and Type__c= \'Revoke\'  ) Order By Name LIMIT 500 ';
        if (actionType == 'GRANT' && secrecyGroup == 'Singapore Secrecy Group')
            qry = 'SELECT Id , Name from User where isActive = true and Name like \'%' + searchTerm + '%\' and Id NOT in (SELECT UserOrGroupId FROM GroupMember WHERE Group.DeveloperName = \'Singapore_Secrecy_Client_Onboarding\' OR Group.DeveloperName = \'Singapore_Secrecy_Legal\' ) Order By Name LIMIT 500 ';
        if (actionType == 'GRANT' && secrecyGroup == 'Seoul Secrecy Group')
            qry = 'SELECT Id , Name from User where isActive = true and Name like \'%' + searchTerm + '%\' and Id NOT in (SELECT UserOrGroupId FROM GroupMember WHERE Group.DeveloperName = \'Seoul_Secrecy_Client_Onboarding\' OR Group.DeveloperName = \'Seoul_Secrecy_Legal\' ) Order By Name LIMIT 500 ';

        // ************** Revoke Search ***********************/
        if (actionType == 'REVOKE' && secrecyGroup == '')
            qry = 'SELECT Id , Name from User where isActive = true and Name like \'%' + searchTerm + '%\'  and Id not in ( SELECT User__c FROM Request_Entitlement__c WHERE Request__c = \'' + requestId + '\' and Type__c = \'Revoke\' ) Order By Name LIMIT 500 ';

        if (actionType == 'REVOKE' && (secrecyGroup == 'Singapore Secrecy Group' ||  secrecyGroup == 'Seoul Secrecy Group' ) )
            qry = 'SELECT Id , Name from User where isActive = true and Name like \'%' + searchTerm + '%\' and Id in ( SELECT User__c FROM Request_Entitlement__c WHERE Request__c = \'' + requestId + '\' and Type__c = \'Grant\' ) Order By Name LIMIT 500 ';

        System.Debug('/n/n **** ' + qry);
        List<User> tmpList = Database.Query(qry);
        List<User> userList  ;


        if (actionType == 'GRANT' && (secrecyGroup == 'Singapore Secrecy Group' ||  secrecyGroup == 'Seoul Secrecy Group' ) ) {
            userList =  new List<User> ();
            set<string> grantSet = new set<string>() ;
            for (Request_Entitlement__c gl : [SELECT User__c FROM Request_Entitlement__c WHERE Request__c = :requestId and Type__c = 'Grant']  )
                grantSet.add(gl.User__c);

            for (User u : tmpList) {
                if (!grantSet.Contains(u.id) ) {
                    userList.add(u);
                }
            }

        } else {
            userList = tmpList;
        }

        return userList ;
    }

    global class UserListClass Implements Comparable {

        public string Id {get; set;}
        public string Name {get; set;}

        public UserListClass (string ident, string UserName) {
            Id = ident;
            Name = UserName;
        }
        // Implement the compareTo() method
        global Integer compareTo(Object compareTo) {
            UserListClass compareToEmp = (UserListClass)compareTo;
            if (Name == compareToEmp.Name) return 0;
            if (Name > compareToEmp.Name) return 1;
            return -1;
        }
    }

}