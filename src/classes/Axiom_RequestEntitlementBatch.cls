public with sharing class Axiom_RequestEntitlementBatch {
    /*

      Authors :  David Brandenburg
      Created Date: 2015-09-04
      Last Modified: 2014-09-17

      Purpose:
         Class which is called from Batchable class called Axiom_RequestEntitlementBatch .
         Class is used to keep user shares in sync for the Request__c and Matter_c objects.

     */

    public static void ProcessRecords (List<Request__c> requests) {

        /*==========  Created maps of all the affected Object records  ==========*/
        map<id, User> userMap = new map<id, User> ([SELECT Id , Name from User where isActive = true and UserType = 'Standard'  ]);
        map<id, User> SingaporeMap = new Map<id, User> ( [SELECT Id , Name from User where isActive = true and UserType = 'Standard' and Id in (SELECT UserOrGroupId FROM GroupMember
                WHERE Group.DeveloperName = 'Singapore_Secrecy_Client_Onboarding' OR Group.DeveloperName = 'Singapore_Secrecy_Legal') ]) ;
        map<id, User> SeoulMap = new Map<id, User> ( [SELECT Id , Name from User where isActive = true and UserType = 'Standard' and Id in (SELECT UserOrGroupId FROM GroupMember
                WHERE Group.DeveloperName = 'Seoul_Secrecy_Client_Onboarding' OR Group.DeveloperName = 'Seoul_Secrecy_Legal') ]) ;

        /*==========  Maps to hold individual sharing rights for each request  ==========*/
        map<id, Request_Entitlement__c> entitlementMap = new map<id, Request_Entitlement__c> ();
        map<id, Request_Entitlement__c> entitlementMapRevoke = new map<id, Request_Entitlement__c> ();
        map<id, Request__Share> shareMap =  new map<id, Request__Share> ();
        //string GRANT = Schema.Request__Share.RowCause.Grant__c;
        string GRANT = 'Grant__c' ;

        List<Request__Share> addShares = new List<Request__Share> ();
        List<Request__Share> deleteShares = new List<Request__Share> ();

        For (Request__c req :  requests) {

            System.Debug('\n\n ********* ' + req.Id);
            System.Debug('\n\n ********* ' + [select UserOrGroupId from Request__Share where RowCause = :GRANT and ParentId = :req.id  ] ) ;
            For ( Request__Share share : [select UserOrGroupId from Request__Share where RowCause = :GRANT and ParentId = :req.id  ] )
            shareMap.put(share.UserOrGroupId , share) ;

            /*==========  Populate map for Request_Entitlement object each request  ==========*/
            for (Request_Entitlement__c  reqEntitlement : [select User__c , Type__c from Request_Entitlement__c where Request__c = :req.id and Type__c != 'Revoke']) {
                entitlementMap.put(reqEntitlement.User__c, reqEntitlement);
            }

            for (Request_Entitlement__c  reqEntitlement : [select User__c , Type__c from Request_Entitlement__c where Request__c = :req.id and Type__c = 'Revoke']) {
                entitlementMapRevoke.put(reqEntitlement.User__c, reqEntitlement);
            }

            /*========== Start processing records for the request   ==========*/
            if (String.isBlank(req.Secrecy_Group__c) ) {

                List<Request__Share> newShares = new List<Request__Share> ();

                //Add the shares where the user is missing
                for ( id key : userMap.Keyset() ) {
                    if (!shareMap.Containskey(key)  ) {
                        Request__Share newShare = new Request__Share ();
                        newShare.UserOrGroupId = key ;
                        newShare.Rowcause = GRANT;
                        newShare.AccessLevel = 'Read';
                        newShares.add(newShare);
                    }
                }
            }

            if (!String.isBlank(req.Secrecy_Group__c) && req.Secrecy_Group__c == 'Singapore Secrecy Group' ) {
                 ProcessObjectShares (req.Id, addShares, deleteShares , SingaporeMap , shareMap, entitlementMap) ;
            }

            if (!String.isBlank(req.Secrecy_Group__c) && req.Secrecy_Group__c == 'Seoul Secrecy Group' ) {
                ProcessObjectShares (req.Id, addShares, deleteShares , SeoulMap , shareMap, entitlementMap) ;
            }

        }

        /*==========  DML to Insert and Delete Shares records  ==========*/
        if (!addShares.isEmpty() )
            insert addShares ;
        if (!deleteShares.isEmpty() )
            delete deleteShares ;

    }

    private Static void ProcessObjectShares (Id RequestId
        , List<Request__Share> addSharesIN
        , List<Request__Share> deleteSharesIN 
        ,map<id, User> GroupMapIN 
        ,map<id, Request__Share> shareMapIN 
        ,map<id, Request_Entitlement__c> entitlementMapIN) {

        List<Request__Share> newShares = new List<Request__Share> ();

        //Add the shares where the user is missing
        for ( id key : GroupMapIN.Keyset() ) {
            if (!shareMapIN.Containskey(key)  ) {
                Request__Share newShare = new Request__Share ();
                newShare.ParentId = RequestId ;
                newShare.UserOrGroupId = key ;
                newShare.Rowcause = Schema.Request__Share.RowCause.Grant__c;
                newShare.AccessLevel = 'Read';
                newShares.add(newShare);
            }
        }
        addSharesIN.addall(newShares);

        //Check for users not in the Group
        set<Request__Share> deleteList = new set<Request__Share>();
        for ( id key : shareMapIN.Keyset() ) {
            if (!GroupMapIN.Containskey(shareMapIN.get(key).UserOrGroupId)  && !entitlementMapIN.Containskey(shareMapIN.get(key).UserOrGroupId) ) {
                deleteList.add(shareMapIN.get(key));
            }
        }
        deleteSharesIN.addall(deleteList) ;
    }


}