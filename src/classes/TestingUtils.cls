/**
 * @author      Philip Choi		( pchoi@liquidhub.com )
 * @version     1.0 			(current version number of program)
 * @since       2015-08-03
 */
 @isTest
public with sharing class TestingUtils
{
	/**
	 * Create test accounts
	 * @param	numToCreateIn	: number of records to create
	 * @param	accountNameIn	: name of the test account
	 * @param	doInsert		: boolean variable to decide insert or not
	 * @return List<Account>	: returns the created test accounts
	 */
	public static List<Account> createAccounts( Integer numToCreateIn, String accountNameIn, Boolean doInsert )
	{
		List<Account> testAccounts = new List<Account>();
		for( Integer i = 0; i < numToCreateIn; i++ )
		{
			Account testAccount = new Account( Name = accountNameIn + i );
			testAccounts.add( testAccount );
		}

		if( doInsert )
			insert testAccounts;

		return testAccounts;
	}

	/**
	 * Create test contacts
	 * @param	numToCreateIn	: number of records to create
	 * @param	accountIdIn		: id of associated Account
	 * @param	firstNameIn		: first name of the test contact
	 * @param	lastNameIn		: last name of the test contact
	 * @param	businessUnitIn	: business unit of the test contact
	 * @param	doInsert		: boolean variable to decide insert or not
	 * @return List<Contact>	: returns the created test contacts
	 */
	public static List<Contact> createContacts( Integer numToCreateIn, Id accountIdIn, String firstNameIn, String lastNameIn, Boolean doInsert )
	{
		List<Contact> testContacts = new List<Contact>();
		for( Integer i = 0; i < numToCreateIn; i++ )
		{
			Contact testContact = new Contact();
			testContact.AccountId = accountIdIn;
			testContact.FirstName = firstNameIn;
			testContact.LastName = ( numToCreateIn == i ) ? lastNameIn : lastNameIn + i;
			testContacts.add( testContact );
		}

		if( doInsert )
			insert testContacts;

		return testContacts;
	}

	/**
	 * Create test users
	 * @param	numToCreateIn	: number of records to create
	 * @param	profileNameIn	: profile name of the test user
	 * @param	contactIdIn		: id of portal contact
	 * @param	doInsert		: boolean variable to decide insert or not
	 * @return List<User>		: returns the created test users
	 */
	public static List<User> createUsers( Integer numToCreateIn, String profileNameIn, Id contactIdIn, Boolean doInsert )
	{
		Id profileId;
		try
		{
			profileId = [ SELECT Id FROM Profile WHERE Name = :profileNameIn ][0].Id;
		}
		catch( Exception e )
		{
			System.assert( false, 'Cannot find profile with name ' + profileNameIn );
		}

		List<User> testUsers = new List<User>();
		for( Integer i = 0; i < numToCreateIn; i++ )
		{
			String s = 'test' + Integer.valueOf(Math.random()*10000);

			User testUser = new User();
			testUser.Alias = s;
			testUser.Email = s + '@user.com';
			testUser.EmailEncodingKey = 'UTF-8';
			testUser.LastName = 'Test';
			testUser.LanguageLocaleKey = 'en_US';
			testUser.LocaleSidKey = 'en_US';
			testUser.TimezoneSidKey = 'America/New_York';
			testUser.UserName = s + '@user.com';
			testUser.IsActive = true;
			testUser.ProfileId = profileId;
			if( contactIdIn != null )
				testUser.ContactId = contactIdIn;

			testUsers.add( testUser );
		}

		if( doInsert )
			insert testUsers;

		return testUsers;
	}

	/**
	 * Create test requests
	 * @param	numToCreateIn			: number of records to create
	 * @param	requestNameIn			: name of the request
	 * @param	doInsert				: boolean variable to decide insert or not
	 * @return List<Request__c>			: returns the created test requests
	 */
	public static List<Request__c> createRequests( Integer numToCreateIn, String requestNameIn, Boolean doInsert )
	{
		List<Request__c> testRequests = new List<Request__c>();
		for( Integer i = 0; i < numToCreateIn; i++ )
		{
			Request__c testRequest = new Request__c();
			testRequest.Name = requestNameIn + i;
			testRequests.add( testRequest );
		}

		if( doInsert )
			insert testRequests;

		return testRequests;
	}

	/**
	 * Create test request entitlements
	 * @param	numToCreateIn					: number of records to create
	 * @param	requestIdIn						: id of the parent request
	 * @param	userIdIn						: id of the related user
	 * @param	typeIn							: string value of type
	 * @param	doInsert						: boolean variable to decide insert or not
	 * @return List<Request_Entitlement__c>		: returns the created test request entitlements
	 */
	public static List<Request_Entitlement__c> createRequestEntitlements( Integer numToCreateIn, Id requestIdIn, Id userIdIn, String typeIn, Boolean doInsert )
	{
		List<Request_Entitlement__c> testRequestEntitlements = new List<Request_Entitlement__c>();
		for( Integer i = 0; i < numToCreateIn; i++ )
		{
			Request_Entitlement__c testRequestEntitlement = new Request_Entitlement__c();
			testRequestEntitlement.Request__c = requestIdIn;
			testRequestEntitlement.User__c = userIdIn;
			testRequestEntitlement.Type__c = typeIn;
			testRequestEntitlements.add( testRequestEntitlement );
		}

		if( doInsert )
			insert testRequestEntitlements;

		return testRequestEntitlements;
	}

	/**
	 * Create test request shares
	 * @param	numToCreateIn			: number of records to create
	 * @param	requestIdIn				: id of associated request
	 * @param	userOrGroupIdIn			: id of associated user or group
	 * @param	secrecyGroupIn			: secrecy group string
	 * @param	doInsert				: boolean variable to decide insert or not
	 * @return List<Request__Share>		: returns the created test request shares
	 */
	public static List<Request__Share> createRequestShares( Integer numToCreateIn, Id requestIdIn, Id userOrGroupIdIn, String secrecyGroupIn, Boolean doInsert )
	{
		List<Request__Share> testRequestShares = new List<Request__Share>();
		for( Integer i = 0; i < numToCreateIn; i++ )
		{
			Request__Share testRequestShare = new Request__Share();
			testRequestShare.ParentId = requestIdIn;
			testRequestShare.UserOrGroupId = userOrGroupIdIn;
			testRequestShare.RowCause = secrecyGroupIn;
			testRequestShare.AccessLevel = 'Read';
			testRequestShares.add( testRequestShare );
		}

		if( doInsert )
			insert testRequestShares;

		return testRequestShares;
	}

	/**
	 * Create test matters
	 * @param	numToCreateIn			: number of records to create
	 * @param	matterNameIn			: name of the matter
	 * @param	requestIdIn				: id of related request
	 * @param	doInsert				: boolean variable to decide insert or not
	 * @return List<Matter__c>			: returns the created test requests
	 */
	public static List<Matter__c> createMatters( Integer numToCreateIn, String matterNameIn, Id requestIdIn, Boolean doInsert )
	{
		List<Matter__c> testMatters = new List<Matter__c>();
		for( Integer i = 0; i < numToCreateIn; i++ )
		{
			Matter__c testMatter = new Matter__c();
			testMatter.Name = matterNameIn + i;
			testMatter.Request__c = requestIdIn;
			testMatters.add( testMatter );
		}

		if( doInsert )
			insert testMatters;

		return testMatters;
	}

	/**
	 * Create test matter shares
	 * @param	numToCreateIn			: number of records to create
	 * @param	matterIdIn				: id of associated matter
	 * @param	userOrGroupIdIn			: id of associated user or group
	 * @param	secrecyGroupIn			: secrecy group string
	 * @param	doInsert				: boolean variable to decide insert or not
	 * @return List<Matter__Share>		: returns the created test matter shares
	 */
	public static List<Matter__Share> createMatterShares( Integer numToCreateIn, Id matterIdIn, Id userOrGroupIdIn, String secrecyGroupIn, Boolean doInsert )
	{
		List<Matter__Share> testMatterShares = new List<Matter__Share>();
		for( Integer i = 0; i < numToCreateIn; i++ )
		{
			Matter__Share testMatterShare = new Matter__Share();
			testMatterShare.ParentId = matterIdIn;
			testMatterShare.UserOrGroupId = userOrGroupIdIn;
			testMatterShare.RowCause = secrecyGroupIn;
			testMatterShare.AccessLevel = 'Read';
			testMatterShares.add( testMatterShare );
		}

		if( doInsert )
			insert testMatterShares;

		return testMatterShares;
	}

	/**
	 * Create test legal entities
	 * @param	numToCreateIn			: number of records to create
	 * @param	legalEntityNameIn		: name of the legal entity
	 * @param	parentLEIdIn			: id of parent legal entity
	 * @param	secrecyGroupIn			: secrecy group of the legal entity
	 * @param	doInsert				: boolean variable to decide insert or not
	 * @return List<Legal_Entity__c>	: returns the created test legal entities
	 */
	public static List<Legal_Entity__c> createLegalEntities( Integer numToCreateIn, String legalEntityNameIn, Id parentLEIdIn, String secrecyGroupIn, Boolean doInsert )
	{
		List<Legal_Entity__c> testLegalEntities = new List<Legal_Entity__c>();
		for( Integer i = 0; i < numToCreateIn; i++ )
		{
			Legal_Entity__c testLegalEntity = new Legal_Entity__c();
			testLegalEntity.Legal_Entity_Name__c = legalEntityNameIn + i;
			testLegalEntity.Company__c = parentLEIdIn;
			testLegalEntity.Secrecy_Group__c = secrecyGroupIn;
			testLegalEntities.add( testLegalEntity );
		}

		if( doInsert )
			insert testLegalEntities;

		return testLegalEntities;
	}

	/**
	 * Create test legal entity roles
	 * @param	numToCreateIn				: number of records to create
	 * @param	requestIdIn					: id of parent request
	 * @param	legalEntityIdIn				: id of parent legal entity
	 * @param	doInsert					: boolean variable to decide insert or not
	 * @return List<Legal_Entity_Role__c>	: returns the created test legal entity roles
	 */
	public static List<Legal_Entity_Role__c> createLegalEntities( Integer numToCreateIn, Id requestIdIn, Id legalEntityIdIn, Boolean doInsert )
	{
		List<Legal_Entity_Role__c> testLegalEntityRoles = new List<Legal_Entity_Role__c>();
		for( Integer i = 0; i < numToCreateIn; i++ )
		{
			Legal_Entity_Role__c testLegalEntityRole = new Legal_Entity_Role__c();
			testLegalEntityRole.Request_Name__c = requestIdIn;
			testLegalEntityRole.Legal_Entity_Name__c = legalEntityIdIn;
			testLegalEntityRoles.add( testLegalEntityRole );
		}

		if( doInsert )
			insert testLegalEntityRoles;

		return testLegalEntityRoles;
	}

	/**
	 * Create test group members
	 * @param	numToCreateIn			: number of records to create
	 * @param	groupIdIn				: id of associated group
	 * @param	userIdIn				: id of associated user
	 * @param	doInsert				: boolean variable to decide insert or not
	 * @return List<GroupMember>		: returns the created test group members
	 */
	public static List<GroupMember> createGroupMembers( Integer numToCreateIn, Id groupIdIn, Id userIdIn, Boolean doInsert )
	{
		List<GroupMember> testGroupMembers = new List<GroupMember>();
		for( Integer i = 0; i < numToCreateIn; i++ )
		{
			GroupMember testGroupMember = new GroupMember();
			testGroupMember.GroupId = groupIdIn;
			testGroupMember.UserOrGroupId = userIdIn;
			testGroupMembers.add( testGroupMember );
		}

		if( doInsert )
			insert testGroupMembers;

		return testGroupMembers;
	}
}