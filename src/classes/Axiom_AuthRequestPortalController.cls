public with sharing class Axiom_AuthRequestPortalController {

    public id userId                             { get; set; }
    public SelectOption[] matterRecordTypes      { get; set; } 
    public user userData                         { get; set; }
    public string selectedMatterRecordType       { get; set; } 
    public boolean initException                 { get; set; }
    public Axiom_AuthRequestPortalController(ApexPages.StandardController controller) {
        try
        {
            userData = [select name, id, ContactId, Contact.Geography__c, Contact.Member_of_Legal_Team__c, userType from user where id = :UserInfo.getUserId()];
            
            if(userData.ContactId == null)
            {
                throw new applicationException('Current user is not linked to a contact. Enable this user as a portal user to access the Request Portal');
            } 
            
            
            matterRecordTypes = new list<selectOption>();
            list<RecordType> matterRecordTypesQuery = [select developername, name from recordType where sObjectType = 'matter__c'];
            for ( RecordType r : matterRecordTypesQuery)
            {
                matterRecordTypes.add(new SelectOption(r.developerName, r.Name));
            }    
        }
        catch(exception e)
        {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,e.getMessage()));
            initException = true;
        }
                   
    }
    public class applicationException extends Exception {}
}