/******* ActivityPlans_Utilities *********
Author: Daniel Llewellyn/RedKite (dllewellyn@redkitetechnologies.com)
Date: ~5/2014
Description: Common/shared and abstracted methods that can be used by other classes/methods.
**********************************************/

global class ActivityPlans_Utilities
{
   
    
    public Map<string,Map<String, Schema.SObjectField>> objectFields = new Map<string,Map<String, Schema.SObjectField>>();
    
    /**
    * builds an SOQL query string that will select all fields for a given object type.
    * @param objectType a string that is the API name of the object to build the select all string for
    * @return string that can be used in a dynamic SOQL query to select all fields
    */
    public static string buildSelectAllStatment(string objectType)
    {
        Map<String, Schema.SObjectField> fldObjMap = Schema.getGlobalDescribe().get(objectType).getDescribe().fields.getMap();
        List<Schema.SObjectField> fldObjMapValues = fldObjMap.values();
        String theQuery = 'SELECT ';
        for(Schema.SObjectField s : fldObjMapValues)
        {  
           theQuery += s.getDescribe().getName() + ',';
        }
        
        // Trim last comma
        theQuery = theQuery.subString(0, theQuery.length() - 1);
        
        // Finalize query string
        theQuery += ' FROM '+objectType;    
        
        return theQuery;
    }
    
    public sObject putValueWithDynamicCasting(sobject thisObject, string fieldToWriteTo, sObjectField sObjectFieldToEval, object value)
    {
        string objectType = thisObject.getSObjectType().getDescribe().getName();
        if(!objectFields.containsKey(objectType))
        {       
    
            String[] types = new String[]{objectType};
            
            //get fields for parent object
            Schema.DescribeSobjectResult[] results = Schema.describeSObjects(types);
                        
            Map<String, Schema.SObjectField> thisObjectFields = results[0].fields.getMap();
            
            objectFields.put(objectType,thisObjectFields);
        }        
        
        Schema.DescribeFieldResult fieldData = sObjectFieldToEval.getDescribe();
        string fieldType =  fieldData.getType().name();
        string castedVal = (string) value;
        
        if(value != null)
        {    
            if(fieldType == 'ID' && value != 'null') value = Id.valueOf(castedVal );                
            else if(fieldType == 'Boolean' && value != 'null') value = Boolean.valueOf(castedVal );                  
            else if((fieldType == 'Currency' || fieldType == 'Double' || fieldType == 'Percent') && value != 'null') value = Decimal.valueOf(castedVal );                    
            else if(fieldType == 'Integer' && value != 'null')value = Integer.valueOf(castedVal );
            else if(fieldType == 'Date' && value != 'null') value = Date.parse(castedVal );                  
            else if(fieldType == 'DateTime' && value != 'null' ) value =DateTime.parse(castedVal );                                                       
            else value = value;    
        }
        thisObject.put(fieldToWriteTo,value);
        
        return thisObject;
    }
}