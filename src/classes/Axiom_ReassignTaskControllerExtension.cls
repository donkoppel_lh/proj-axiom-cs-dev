public class Axiom_ReassignTaskControllerExtension {

    public task thisTask {get;set;}
    public boolean isError {get;set;}
    
    public Axiom_ReassignTaskControllerExtension(ApexPages.StandardController controller) 
    {
        try
        {
            controller.addFields(new List<String>{'Created_by_Template__c'});
            thisTask = (Task) controller.getRecord();
            if(thisTask.Id == null)
            {
                throw new customException('Please provide a task to update by passing in the Id parameter in the URL'); 

            }
            
            if(thisTask.Created_by_Template__c != null)
            {
                Activity_Plan_Task_Template__c parentTemplate = [select name, id from Activity_Plan_Task_Template__c where id = :thisTask.Created_by_Template__c];
                
                if(parentTemplate.Name != 'QA Review')
                {
                    throw new customException('This utility may only be used for QA Review Tasks'); 
                }
            }
            else
            {
                throw new customException('This utility may only be used for QA Review Tasks'); 
            }
        }
        catch(exception e)
        {
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());
                ApexPages.addMessage(myMsg);  
                isError= true;              
        }
        
        
    }
    public pageReference save()
    {
        isError= false;
        try
        {
            update thisTask;
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Task Reassigned Successfully!');
            ApexPages.addMessage(myMsg);          
        }
        catch(exception e)
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());
            ApexPages.addMessage(myMsg);  
            isError= true;            
        }
        return null;
    }
    
    public class customException extends Exception {}

 

}