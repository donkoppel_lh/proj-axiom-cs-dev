/*

	 Authors :  David Brandenburg
	 Created Date: 2015-09-04
	 Last Modified: 2014-09-17

	 Purpose:  Batch Process to clean up orphaned usered which have share records to the Matter__c and Request__c Object
	
*/

global class Axiom_RequestCleanupBatch implements Database.Batchable<sObject> , Axiom_RequestCleanupSchedulable.IScheduleDispatched {

	string query = 'select id, Secrecy_Group__c, Sharing_Method__c from Request__c where Sharing_Method__c = \'Sharing Rule\' and Id = \'a0Ug0000004DdgR\' ' ;
	global Axiom_RequestCleanupBatch() {

	}

	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

	public void execute(SchedulableContext sc) { 
		// starts batch
		Axiom_RequestCleanupBatch batchClass = new Axiom_RequestCleanupBatch();
		ID idBatch = Database.executeBatch(batchClass, 5);
	}

	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		ProcessRecords(scope);
	}

	global void finish(Database.BatchableContext BC) {

	}


	private void ProcessRecords (List<Request__c> requests) {

		/*==========  Created maps of all the affected Object records  ==========*/
		map<id, User> userMap = new map<id, User> ([SELECT Id , Name from User where isActive = true and UserType = 'Standard'  ]);
		map<id, User> SingaporeMap = new Map<id, User> ( [SELECT Id , Name from User where isActive = true and UserType = 'Standard' and Id in (SELECT UserOrGroupId FROM GroupMember
		        WHERE Group.DeveloperName = 'Singapore_Secrecy_Client_Onboarding' OR Group.DeveloperName = 'Singapore_Secrecy_Legal') ]) ;
		map<id, User> SeoulMap = new Map<id, User> ( [SELECT Id , Name from User where isActive = true and UserType = 'Standard' and Id in (SELECT UserOrGroupId FROM GroupMember
		        WHERE Group.DeveloperName = 'Seoul_Secrecy_Client_Onboarding' OR Group.DeveloperName = 'Seoul_Secrecy_Legal') ]) ;

		/*==========  Maps to hold individual sharing rights for each request  ==========*/

		map<id, Request_Entitlement__c> entitlementMap = new map<id, Request_Entitlement__c> ();
		map<id, Request_Entitlement__c> entitlementMapRevoke = new map<id, Request_Entitlement__c> ();
		map<id, Request__Share> shareMap =  new map<id, Request__Share> ();
		map<id, Matter__Share> matterShareMap =  new map<id, Matter__Share> ();
		//string GRANT = Schema.Request__Share.RowCause.Grant__c;
		string GRANT = 'Grant__c' ;

		/*==========  Lists to store the Add/Delete records  ==========*/
		List<Request__Share> addShares = new List<Request__Share> ();
		List<Request__Share> deleteShares = new List<Request__Share> ();
		List<Matter__Share> matterDeleteShares = new List<Matter__Share> ();
		List<Request_Entitlement__c> deleteEntitlement = new List<Request_Entitlement__c> ();

		For (Request__c req :  requests) {

			/*==========  Populate the maps for the current requt  ==========*/

			For ( Request__Share share : [select UserOrGroupId from Request__Share where RowCause = :GRANT and ParentId = :req.id  ] ) {
				shareMap.put(share.UserOrGroupId , share) ;
			}

			For ( Matter__Share share : [select UserOrGroupId from Matter__Share where RowCause = :GRANT and ParentId in (Select Id from Matter__c where  Request__c = :req.id  ) ] ) {
				matterShareMap.put(share.UserOrGroupId , share) ;
			}


			for (Request_Entitlement__c  reqEntitlement : [select User__c , Type__c from Request_Entitlement__c where Request__c = :req.id and Type__c != 'Revoke']) {
				entitlementMap.put(reqEntitlement.User__c, reqEntitlement);
			}

			/*==========  Process to populate the Singpore Shares  ==========*/

			if (!String.isBlank(req.Secrecy_Group__c) &&  req.Secrecy_Group__c == 'Singapore Secrecy Group' ) {

				ProcessShares (deleteShares, matterDeleteShares, deleteEntitlement, matterShareMap, entitlementMap, SingaporeMap , shareMap) ;
			}

			/*==========  Process to populate the Seoul Shares   ==========*/

			if (!String.isBlank(req.Secrecy_Group__c) && req.Secrecy_Group__c == 'Seoul Secrecy Group' ) {

				ProcessShares (deleteShares, matterDeleteShares, deleteEntitlement, matterShareMap, entitlementMap, SeoulMap , shareMap) ;

			}



			System.Debug ('\n\n\n **** delete : ' + deleteShares ) ;
			delete deleteEntitlement ;
			delete deleteShares ;

		}

	}


	/**
	*
	* Method to compare the shares and add records to the Add/Delete Lists
	* Use to find orphans between the object shares and the Public Groups users.
	*
	**/

	private void ProcessShares (
	    List<Request__Share> deleteSharesIN
	    , List<Matter__Share> matterDeleteSharesIN
	    , List<Request_Entitlement__c> deleteEntitlementIN
	    , map<id, Matter__Share> matterShareMapIN
	    , map<id, Request_Entitlement__c> entitlementMapIN
	    , map<id, User> userMap
	    , map<id , Request__Share> shareMapIN ) {

		for (Id key : shareMapIN.KeySet())  {
			if (userMap.Containskey(key))
				deleteSharesIN.add(shareMapIN.get(key));
		}

		for (Id key : matterShareMapIN.KeySet())  {

			if (userMap.Containskey(key))
				matterDeleteSharesIN.add(matterShareMapIN.get(key));
		}

		for (Id key : entitlementMapIN.KeySet())  {
			if (userMap.Containskey(key))
				deleteEntitlementIN.add(entitlementMapIN.get(key));
		}


	}




}