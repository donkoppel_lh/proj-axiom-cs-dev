global class Axiom_RibbonController
{

 // Ribbon controller - the ribbon will call this webservice and pass
 // the action, the objectName, the id as strings
 // returns a string probably fix this to allow more things with an object
 
 // doing it this way so we can don't have to regenerate the WSDL 
 // and recompile the Ribbon code when changing the button calls

 webservice static RibbonResponse Dispatch(RibbonRequest req)
    {
                  
         if(req.objname == 'Request__c'){
            if(req.action == 'Accept'){
                return RequestAccept(req);          
            } else if(req.action == 'Reject'){          
                return RequestReject(req);          
            }
         }      
         
         if(req.objname == 'Matter__c'){
            if(req.action == 'Accept'){
                return MatterAccept(req);           
            } else if(req.action == 'Decline'){             
                return MatterDecline(req);          
            }
         }               
         
         if(req.objname == 'Task'){
            if(req.action == 'Accept'){
                return TaskAccept(req);             
            } else if(req.action == 'Complete'){            
                return TaskComplete(req);           
            } else if(req.action == 'Approve'){             
                return TaskApprove(req);            
            } else if(req.action == 'Reject'){          
                return TaskReject(req);             
            } else if(req.action == 'Clockstopper'){            
                return TaskClockstopper(req);           
            }
         }   
         
         
         RibbonResponse res = new RibbonResponse();
         res.success = false;
         res.message = 'Action does not exist!';
         return res;                  
    }
    
    
    
    global class RibbonResponse
    {
        webService String message;
        webService Boolean success;      
        webService String nextactionmessage; // Next Action - if this has entries then the ribbon knows to put up the options and offer the options then call the selected nextaction
        webService List<String> nextaction;   
        webService List<String> nextactionoptions;
        webService String reload; // tells the ribbon what to reload - either Parent/Object/None or Filter-Name - if the filter is to be refreshed
        webService String selectid; // if we have created a new record then select that
                 
        public RibbonResponse()
        {
            message = '';
            success = true;
            nextactionmessage = '';            
            nextaction = new List<String>();
            nextactionoptions = new List<String>();
            reload = '';
            selectid = '';
        }
    }        
    
    global class RibbonRequest
    {
        webService String action;
        webService String objname;
        webService String id;
    }
    
    // Functions that do stuff
    
    
    
    // Request -----------------------------------------------------------------------------------
    // Accept the request
    private static RibbonResponse RequestAccept(RibbonRequest req){
        RibbonResponse res = new RibbonResponse();

        if(req.id==''){
            res.message = 'Id Of Request is required';
            res.success = false;
            return res;         
        } 
        
        // Get the Request
        List<Request__c> lr = [select Id,OwnerId,Start_Time__c from Request__c where Id = :req.Id]; 
        if(lr.isEmpty()){
            res.message = 'Id Of Request does not exist';
            res.success = false;
            return res;                     
        }
        Request__c r = lr.get(0);
        
        // Check if it is already accepted
        if(r.Start_Time__c != null){
            res.message = 'Request is already accepted';
            res.success = false;
            return res;                     
        }
                
        // Check if we are the owner
        if(r.OwnerId != UserInfo.getUserId()){
            res.message = 'You must be the owner of the Request to accept it';
            res.success = false;
            return res;                     
        }
        
        // Update the start date
        try{
            r.Start_Time__c = System.now();     
            update r;       
            res.message = '';
            res.reload = 'Object';
            res.success = true;
        } catch(Exception e) {
            res.message = e.getMessage();
            res.success = false;
        }
        
        return res; 
    }
    
     // Reject the request
    private static RibbonResponse RequestReject(RibbonRequest req){
        RibbonResponse res = new RibbonResponse();

        if(req.id==''){
            res.message = 'Id Of Request is required';
            res.success = false;
            return res;         
        } 
        
        // Get the Request
        List<Request__c> lr = [select Id,OwnerId,Start_Time__c,Reason_for_Rejection__c,Stage__c,Step__c,Status__c,TS_of_Request_Rejected__c from Request__c where Id = :req.Id]; 
        if(lr.isEmpty()){
            res.message = 'Id Of Request does not exist';
            res.success = false;
            return res;                     
        }
        
        Request__c r = lr.get(0);
        
        // Check if it is already accepted
        if(r.Start_Time__c != null){
            res.message = 'Request is already accepted and cannot be rejected';
            res.success = false;
            return res;                     
        }
                
        // Check if we are the owner
        if(r.OwnerId != UserInfo.getUserId()){
            res.message = 'You must be the owner of the Request to reject it';
            res.success = false;
            return res;                     
        }
        
        // Check we have a rejection reason
        if(r.Reason_for_Rejection__c == null){
            res.message = 'Please enter a Reason for Rejection and save before rejecting this request';
            res.success = false;
            return res;                     
        }
        
        try{            
            // update the Matters - shouldn't actually be any but no harm done - copying the javascript code 
            list<Matter__c> matters = [select id,Stage__c,Step__c,Status__c from matter__c where request__c = :req.Id];
            for(Matter__c m : matters){
                m.Stage__c = 'Reject';
                m.Step__c = 'Reject';
                m.Status__c = 'Closed';
                update m;
            }
                    
            // update the Request
            r.TS_of_Request_Rejected__c = System.now();
            r.Stage__c = 'Request';
            r.Step__c = 'Reject';
            r.Status__c = 'Complete';
            update r;       
                        
            res.message = '';
            res.reload = 'Object';
            res.success = true;
        } catch(Exception e) {
            res.message = e.getMessage();
            res.success = false;
        }
        
        return res; 
    }
    
    
    // Matter -----------------------------------------------------------------------------------
    // Accept the Matter
    private static RibbonResponse MatterAccept(RibbonRequest req){
        RibbonResponse res = new RibbonResponse();

        if(req.id==''){
            res.message = 'Id Of Matter is required';
            res.success = false;
            return res;         
        } 
        
        // Get the Matter
        List<Matter__c> lm = [select Id,OwnerId,Start_Time__c,Primary_Activity_Plan__c,Matter_Type__c from Matter__c where Id = :req.Id]; 
        if(lm.isEmpty()){
            res.message = 'Id Of Matter does not exist';
            res.success = false;
            return res;                     
        }
        
        Matter__c m = lm.get(0);
        
        // Check if it is already accepted
        if(m.Start_Time__c != null){
            res.message = 'Matter is already accepted';
            res.success = false;
            return res;                     
        }
                
        // Check if we are the owner
        if(m.OwnerId != UserInfo.getUserId()){
            res.message = 'You must be the owner of the Matter to accept it';
            res.success = false;
            return res;                     
        }
        
        // Aug 26 - Update to match Javascript
        // Check we have entitlement
        if(m.Matter_Type__c == 'Agreement'){        	
        	boolean canAcceptAgreementRequest = Axiom_Utilities.isUserEntitledForFunction(UserInfo.getUserId(), 'Accept Agreement request');
        	if(!canAcceptAgreementRequest){
        		res.message = 'User does not have "Accept Agreement request" entitlement';
           	 	res.success = false;
            	return res;   
        	}
        }
        
        
        // Update the start date
        try{
            // update the Matter - set the start time and trigger the Activity Plan
            m.Start_Time__c = System.now();
            m.Primary_Activity_Plan__c = 'generate';                
            update m;
            
            // find the assign owner task and update
            List<Task> t = [select id,Status,Start_Time__c,Apex_Context__c from Task where Subject = 'Assign Owner' and status != 'Completed' and WhatId = :req.Id LIMIT 1];
            if(t.size() == 1){
                t[0].Apex_Context__c = true;
                t[0].Status = 'Completed'; 
                t[0].Start_Time__c = System.now();
                update t[0];
            }
                    
            res.message = '';
            res.reload = 'Object';
            res.success = true;
            
        } catch(Exception e) {
            res.message = e.getMessage();
            res.success = false;
        }
        
        return res; 
    }
    
     // Reject the Matter
    private static RibbonResponse MatterDecline(RibbonRequest req){
        RibbonResponse res = new RibbonResponse();

        if(req.id==''){
            res.message = 'Id Of Matter is required';
            res.success = false;
            return res;         
        } 
        
        // Get the Matter
        List<Matter__c> lm = [select Id,OwnerId,Stage__c,Step__c,Status__c from Matter__c where Id = :req.Id]; 
        if(lm.isEmpty()){
            res.message = 'Id Of Request does not exist';
            res.success = false;
            return res;                     
        }
        
        Matter__c m = lm.get(0);
                
        // Check if we are the owner
        if(m.OwnerId != UserInfo.getUserId()){
            res.message = 'You must be the owner of the Matter to reject it';
            res.success = false;
            return res;                     
        }
                
        try{
            // get the Queue id
            list<QueueSObject> q = [select Queue.Id from QueueSObject where Queue.Name like '%Matter%'];
            if(q.size()==0){
                res.message = 'Matter Queue is missing';
                res.success = false;
                return res;                             
            } 
                        
            // update the Matter
            m.OwnerId = q[0].Queue.Id;
            m.Stage__c = 'Assignment'; 
			m.Step__c = 'Declined'; 
			m.Status__c = 'Declined'; 
            update m;       
                        
            res.message = '';
            res.reload = 'Object';
            res.success = true;
        } catch(Exception e) {
            res.message = e.getMessage();
            res.success = false;
        }
        
        return res; 
    }    
    
    
    // Task -----------------------------------------------------------------------------------
    // Accept the Task
    
    // Aug 24, 2015 - Update to match the change to the Javascript button
    
    private static RibbonResponse TaskAccept(RibbonRequest req){
        RibbonResponse res = new RibbonResponse();

        if(req.id==''){
            res.message = 'Id Of Task is required';
            res.success = false;
            return res;         
        } 
        
        // Get the Task
        List<Task> lt = [select Id,OwnerId,Start_Time__c,Status,Been_Accepted__c,Apex_Context__c,Created_by_Template__c,WhatId,What.Type from Task where Id = :req.Id]; 
        if(lt.isEmpty()){
            res.message = 'Id Of Task does not exist';
            res.success = false;
            return res;                     
        }
        
        Task t = lt.get(0);
        
        
		//variable that stores if a user is entitled to accept this task. Defaults to true, may be changed to false if
		//a task is in a certain status and the current user does not have an entitlement record.
		boolean permissionToOverrideOwnerEnabled = true;
	
	
		// ** the Javascript code has a confirmation if they want to change the owner and accept the task - we don't have 
		// the ability (yet) to do this from the Ribbon - should have a method to fire callbacks SO just do it but give them 
		// a message after
				
		/* code from javascript 
		//variable that holds if a user wishes to override the current owner of the task and accept it. This gets set by a confirmation box if the current user
		//is not the owner, but does have permission to reassign tasks.
		 bool wantToOverrideCurrentOwner = true;
		 */
		
		//variable to track what the record type of a the related object is. Will only be populated if task is related to a matter__c object.
		string taskRelatedToRecordTypeName = '';
	
		//variable that contains the name of the activity plan template that created this task, if it was created by activity plans.
		string createdByTemplateName = '';        
        
        
        // Check if it is already accepted
        if(t.Been_Accepted__c){
            res.message = 'Task is already accepted';
            res.success = false;
            return res;                     
        }

		// old check                
		/*                
        // Check if we are the owner
        if(t.OwnerId != UserInfo.getUserId() && t.Created_by_Template__c !=''){
            res.message = 'You must be the owner of an activity plan generated Task to accept it';
            res.success = false;
            return res;                     
        }
        */
        
        if(t.WhatId != null && t.What.Type == 'Matter__c' ){        	
        	//find the Id of the record type for the matter related to this task
        	List<Matter__c> recordData = [select Id,RecordTypeId,Name from Matter__c where Id=:t.WhatId];
        	if(recordData.size()>0){
        		List<RecordType> recordTypeResult = [select Id,Name from RecordType where Id=:recordData[0].RecordTypeId];
        		if(recordTypeResult.size()>0){
        			taskRelatedToRecordTypeName = recordTypeResult[0].Name;
        		}
        	}        	
        }
        
        
       	//if this task was created by activity plans, then get the name of the template that created it.
       	if(t.OwnerId != UserInfo.getUserId() && t.Created_by_Template__c !=''){
       		List<Activity_Plan_Task_Template__c> result = [select Id, Name from Activity_Plan_Task_Template__c where Id = :t.Created_by_Template__c ];
       		if(result.size()>0){
       			createdByTemplateName = result[0].Name;
       		}
       	}
        
		//if this task was created by activity plans, and the template that created it was named "QA Review" then check to see if this user is entitled to the 'Complete QA Checklist' function
		if(createdByTemplateName == 'QA Review')
		{
			permissionToOverrideOwnerEnabled = Axiom_Utilities.isUserEntitledForFunction(UserInfo.getUserId(), 'Bypass Subsequent Draft QA Review');
			
			//if they do not have permission to Bypass Subsequent Draft QA Review then they may still be able to accept the task if they have the 'Complete QA Checklist' permission.
			if(!permissionToOverrideOwnerEnabled && taskRelatedToRecordTypeName == 'Agreement'){
				permissionToOverrideOwnerEnabled = Axiom_Utilities.isUserEntitledForFunction(UserInfo.getUserId(), 'Complete QA Checklist');	
			}			
		} else if(createdByTemplateName == 'Final QA Review' && taskRelatedToRecordTypeName == 'Agreement'){
			permissionToOverrideOwnerEnabled = Axiom_Utilities.isUserEntitledForFunction(UserInfo.getUserId(), 'Complete QA Checklist');
		}        
        
        
        
        // Check that they are either overiding the owner and have permission - if so set the message to display after
        // or if not then make sure they are the owner
        string message = '';
        if(permissionToOverrideOwnerEnabled && t.OwnerId != UserInfo.getUserId() && t.Created_by_Template__c !=''
        	&& (createdByTemplateName == 'QA Review' || createdByTemplateName == 'Final QA Review')        
        ){
        	message = 'You have changed the task owner by accepting it';
        	   
        } else if (t.OwnerId != UserInfo.getUserId() && t.Created_by_Template__c !=''){
        	
            res.message = 'You must be the owner of an activity plan generated Task to accept it';
            res.success = false;
            return res;                             
        }
              

        // Update the Task
        try{
            // update the Task - set the start time and the status
            t.Status = 'In Progress';
            t.Been_Accepted__c = true;
            t.Start_Time__c = System.now();
            t.Apex_Context__c = true;
            t.OwnerId = UserInfo.getUserId();                  
            update t;            // java script does this by calling the controller cause of permissions but as this is already apex don't beed to

            
            
            res.message = message;
            res.reload = 'Parent'; // update the parent so the Stage/Step/Status get updated
            res.success = true;
        } catch(Exception e) {
            res.message = e.getMessage();
            res.success = false;
        }
        
        return res; 
    }
    // Complete the Task
    private static RibbonResponse TaskComplete(RibbonRequest req){
        RibbonResponse res = new RibbonResponse();

        if(req.id==''){
            res.message = 'Id Of Task is required';
            res.success = false;
            return res;         
        } 
                
        // Get the Task
        List<Task> lt = [select Id,whatId,Open_Clockstopper__c,Been_Rejected__c,Been_Accepted__c,OwnerId,Stop_Time__c,Status,Apex_Context__c,Been_Approved__c from Task where Id = :req.Id]; 
        if(lt.isEmpty()){
            res.message = 'Id Of Task does not exist';
            res.success = false;
            return res;                     
        }
        
        Task t = lt.get(0);
        
        // Aug 26 - new checks from the javascript
        if(t.Been_Approved__c){
        	res.message = 'Task has already been approved';
            res.success = false;
            return res; 
        }
        	
        if(t.Been_Rejected__c){
        	res.message = 'Task has already been rejected';
            res.success = false;
            return res; 
        }
                
        // Check if it is already been completed
        if(t.Status == 'Completed'){
            res.message = 'Task is already completed';
            res.success = false;
            return res;                     
        }
                
        // Check if we are the owner
        if(t.OwnerId != UserInfo.getUserId()){
            res.message = 'You must be the owner of the Task to complete it';
            res.success = false;
            return res;                     
        }
        
        if(!t.Been_Accepted__c){
        	res.message = 'Please accept the task before completing it';
            res.success = false;
            return res; 
        }        
        
        // Check for any open clockstoppers - not owned by user 
        list<Task> cs = [select Id from Task where Related_Task_ID__c = :req.Id and Status != 'Completed' and OwnerId <> :UserInfo.getUserId() and RecordType.Name ='Clockstopper'];
        if(cs.size() > 0){
            res.message = 'There are related Clockstoppers that are not open and not owned by you';
            res.success = false;
            return res; 
        }
        
        
        // Update the Task
        try{
                    
            t.Status = 'Completed';
            t.Stop_Time__c = System.now();
            t.Apex_Context__c = true;     
            t.Open_Clockstopper__c = false;  // having issues with the trigger not setting this correctly (when the task aslo generates a plan task) - SO set manually
            update t;
            
            // Close out any CStoppers            
            cs = [select Id,Status,Stop_Time__c from Task where Related_Task_ID__c = :req.Id and Status != 'Completed' and OwnerId = :UserInfo.getUserId() and RecordType.Name ='Clockstopper'];
            for(Task cst : cs){
                cst.Status = 'Completed';
                cst.Stop_Time__c = System.now();
                // cst.Apex_Context__c = true;
            }
            update cs;
            
            Axiom_TaskController.setOpenClockstopperFlagOnMatter(lt); // having issues with the trigger not setting this correctly so call manually
            
            res.message = '';
            res.reload = 'Parent'; // update the parent so the Stage/Step/Status get updated
            res.success = true;
        } catch(Exception e) {
            res.message = e.getMessage();
            res.success = false;
        }
        
        return res; 
    }
    
     // Accept the Task
    private static RibbonResponse TaskApprove(RibbonRequest req){
        RibbonResponse res = new RibbonResponse();

        if(req.id==''){
            res.message = 'Id Of Task is required';
            res.success = false;
            return res;         
        } 
        
        // Get the Task
        List<Task> lt = [select Id,whatId,Open_Clockstopper__c,Been_Accepted__c,OwnerId,Stop_Time__c,isClosed,Status,Apex_Context__c,Been_Approved__c,Been_Rejected__c from Task where Id = :req.Id]; 
        if(lt.isEmpty()){
            res.message = 'Id Of Task does not exist';
            res.success = false;
            return res;                     
        }
        
        Task t = lt.get(0);
        
        // Check if its been approved or rejected
        if(t.Been_Approved__c){
            res.message = 'Task has already been approved';
            res.success = false;
            return res;                     
        }
        
        if(t.Been_Rejected__c){
            res.message = 'Task has already been rejected';
            res.success = false;
            return res;                     
        }
        
        // Check if it is already been completed
        if(t.isClosed){
            res.message = 'Task is already completed';
            res.success = false;
            return res;                     
        }
                
        // Check if we are the owner
        if(t.OwnerId != UserInfo.getUserId()){
            res.message = 'You must be the owner of the Task to complete it';
            res.success = false;
            return res;                     
        }
        
        // Check it has been accepted
        if(!t.Been_Accepted__c){
        	res.message = 'Please accept the task before completing it';
            res.success = false;
            return res; 
        }          
        
        // Check for any open clockstoppers - not owned by user 
        list<Task> cs = [select Id from Task where Related_Task_ID__c = :req.Id and Status != 'Completed' and OwnerId <> :UserInfo.getUserId() and RecordType.Name ='Clockstopper'];
        if(cs.size() > 0){
            res.message = 'There are related Clockstoppers that are not open and not owned by you';
            res.success = false;
            return res; 
        }
        
        
        // Update the Task
        try{
                            
            // update the Task - set the stop time and the status
            t.Status = 'Approved';
            t.Open_Clockstopper__c = false;
            t.Been_Approved__c = true;
            t.Stop_Time__c = System.now();
            t.Apex_Context__c = true;       
            update t;
                    
            // Close out any CStoppers
            cs = [select Id,Status,Stop_Time__c from Task where Related_Task_ID__c = :req.Id and Status != 'Completed' and OwnerId = :UserInfo.getUserId() and RecordType.Name ='Clockstopper'];
            for(Task cst : cs){
                cst.Status = 'Completed';
                cst.Stop_Time__c = System.now();
                // cst.Apex_Context__c = true;                
            }
            update cs;
            
            Axiom_TaskController.setOpenClockstopperFlagOnMatter(lt);
                        
            res.message = '';
            res.reload = 'Parent'; // update the parent so the Stage/Step/Status get updated
            res.success = true;
        } catch(Exception e) {
            res.message = e.getMessage();
            res.success = false;
        }
        
        return res; 
    }
    
    // Reject the Task
    private static RibbonResponse TaskReject(RibbonRequest req){
        RibbonResponse res = new RibbonResponse();

        if(req.id==''){
            res.message = 'Id Of Task is required';
            res.success = false;
            return res;         
        } 
        
        // Get the Task
        List<Task> lt = [select Id,WhatId,Open_Clockstopper__c,Been_Accepted__c,OwnerId,Stop_Time__c,Status,Apex_Context__c,isClosed,Been_Approved__c,Been_Rejected__c from Task where Id = :req.Id]; 
        if(lt.isEmpty()){
            res.message = 'Id Of Task does not exist';
            res.success = false;
            return res;                     
        }
        
        Task t = lt.get(0);
        
        // Check if its been approved or rejected
        if(t.Been_Approved__c){
            res.message = 'Task has already been approved';
            res.success = false;
            return res;                     
        }
        
        if(t.Been_Rejected__c){
            res.message = 'Task has already been rejected';
            res.success = false;
            return res;                     
        }
        
        // Check if it is already been completed
        if(t.isClosed){
            res.message = 'Task is already completed';
            res.success = false;
            return res;                     
        }
                
        // Check if we are the owner
        if(t.OwnerId != UserInfo.getUserId()){
            res.message = 'You must be the owner of the Task to complete it';
            res.success = false;
            return res;                     
        }
        
        // Check it has been accepted
        if(!t.Been_Accepted__c){
        	res.message = 'Please accept the task before completing it';
            res.success = false;
            return res; 
        }            
        
        // Check for any open clockstoppers - not owned by user 
        list<Task> cs = [select Id from Task where Related_Task_ID__c = :req.Id and Status != 'Completed' and OwnerId <> :UserInfo.getUserId() and RecordType.Name ='Clockstopper'];
        if(cs.size() > 0){
            res.message = 'There are related Clockstoppers that are not open and not owned by you';
            res.success = false;
            return res; 
        }
        
        
        // Update the Task
        try{
       
            // update the Task - set the stop time and the status
            t.Status = 'Rejected';
            t.Open_Clockstopper__c = false;
            t.Been_Rejected__c = true;
            t.Stop_Time__c = System.now();
            t.Apex_Context__c = true;       
            update t;           
            
            // Close out any CStoppers
            cs = [select Id,Status,Stop_Time__c from Task where Related_Task_ID__c = :req.Id and Status != 'Completed' and OwnerId = :UserInfo.getUserId() and RecordType.Name ='Clockstopper'];
            for(Task cst : cs){
                cst.Status = 'Completed';               
                cst.Stop_Time__c = System.now();
                // cst.Apex_Context__c = true;                
            }
            update cs;              
            
            Axiom_TaskController.setOpenClockstopperFlagOnMatter(lt);  
                                    
            res.message = '';
            res.reload = 'Parent'; // update the parent so the Stage/Step/Status get updated
            res.success = true;
        } catch(Exception e) {
            res.message = e.getMessage();
            res.success = false;
        }
        
        return res; 
    }
    
    // Create a Clockstopper
    private static RibbonResponse TaskClockstopper(RibbonRequest req){
        RibbonResponse res = new RibbonResponse();

        if(req.id==''){
            res.message = 'Id Of Task is required';
            res.success = false;
            return res;         
        } 
        
        // Get the Task
        List<Task> lt = [select Id,isClosed,OwnerId,Stop_Time__c,Status,Apex_Context__c,What.Name,What.Id from Task where Id = :req.Id]; 
        if(lt.isEmpty()){
            res.message = 'Id Of Task does not exist';
            res.success = false;
            return res;                     
        }
        
        Task t = lt.get(0);
        
        // Check if it is already been completed
        if(t.isClosed){
            res.message = 'Closed Tasks cannot have new Clockstoppers';
            res.success = false;
            return res;                     
        }
        
        // Check if we are the owner
        if(t.OwnerId != UserInfo.getUserId()){
            res.message = 'Only the owner of a Task can create a Clockstopper';
            res.success = false;
            return res;                     
        }
        
        // Create a clockstopper
        
        try{
        // get the record type
        RecordType rt = [select id from RecordType where sObjectType='task' and name = 'Clockstopper'];
        if(rt == null){
            res.message = 'Cannot find RecordType for Clockstopper';
            res.success = false;
            return res; 
        }
        
        String taskname = t.What.Name;
        if(taskname.length()>60){
            taskname = t.What.Name.substring(0,60);
        }
        
        Task cs = new Task();
        cs.Subject = 'Clockstopper for ' + taskname;
        cs.Status = 'Open';
        cs.Start_Time__c = System.now();
        cs.WhatId = t.What.Id;
        cs.Apex_Context__c = true;        
        cs.Related_Task_ID__c = t.Id;
        cs.RecordTypeId = rt.Id;
        insert cs;
        
        // return the id of the new object
        res.selectid = cs.Id;
        res.message = '';
        // res.reload = 'Filter:Manual'; // update the parent and select the manual filter so we can see the clockstopper
        res.reload = 'Filter:Open Clockstopper';
        res.success = true;
        }
        catch(Exception e) {
            res.message = e.getMessage();
            res.success = false;
        }
            
        return res;
    
    }
}