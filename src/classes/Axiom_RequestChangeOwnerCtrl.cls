public with sharing class Axiom_RequestChangeOwnerCtrl {

	private final Request__c request;

    public Axiom_RequestChangeOwnerCtrl(ApexPages.StandardController stdController) {
        this.request = (Request__c)stdController.getRecord();
    }

    public PageReference changeOwner(){
        PageReference retPage;
        this.request.OwnerId = UserInfo.getUserId();
        try{
            update this.request;
            retPage = new PageReference( '/' + this.request.Id );
        }catch ( DmlException dmlEx ){
            ApexPages.addMessages( dmlEx );
        }
        
        return retPage;
    }
}