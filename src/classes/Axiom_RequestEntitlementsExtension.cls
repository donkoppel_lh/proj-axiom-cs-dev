/*

     Authors :  David Brandenburg
     Created Date: 2015-09-04
     Last Modified: 2014-09-17

     Purpose:  
        Extension Controller for the Axiom_RequestEntitlements page.
        Page is used as a inline VF page for a Request__c object  page layout.
    
*/

public with sharing class Axiom_RequestEntitlementsExtension {

    public string GrantUserList { get; set; }
    public string RevokeUserList { get; set; }
    public string RequestId  {get; set;}
    public Request__c RequestObj {get; set;}

    public Axiom_RequestEntitlementsExtension(ApexPages.StandardController sc) {
        RequestId = sc.getId() ;
        RequestObj = (Request__c)sc.getRecord();
    }

    public void Init () {
        populateUserList();
    }
    private void populateUserList () {
        /*
        List<User> usrList = [select id, name from User where isActive = true Order by Name] ;
        leftOptions = new SelectOption[]{};
        for (User u : usrList) {
            leftOptions.add( new SelectOption(u.id, u.name));
        }
        */
    }

    public void SaveGrantRequest () {
        set<Id> userList = new set<Id> ();
        for (string s : GrantUserList.Split(',') ) {
            userList.add(s);
        }

        Axiom_Utilities.grantOrRevokeAccessToRequest( 'Grant', requestId , userList ) ;

        GrantUserList = '' ;

    }

    public void SaveRevokeRequest () {
        set<Id> userList = new set<Id> ();
        for (string s : RevokeUserList.Split(',') ) {
            userList.add(s);
        }

        Axiom_Utilities.grantOrRevokeAccessToRequest( 'Revoke', requestId , userList ) ;

        RevokeUserList = '' ;

    }

   
}