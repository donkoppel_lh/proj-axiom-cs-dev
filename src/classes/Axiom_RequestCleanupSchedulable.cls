/*
	
	 Authors :  David Brandenburg
	 Created Date: 2015-09-04
	 Last Modified: 2014-09-17
	 
	 Purpose:  Schedulable class for the batch class called Axiom_RequestCleanupBatch

	 Schedule :
	 
		30 mins after the hour
		Axiom_RequestCleanupSchedulable clsCRON = new  Axiom_RequestCleanupSchedulable();
		System.Schedule('Request CleanUp Batch-30', '0 30 * * * ? ', clsCRON);
	
	

*/

global class Axiom_RequestCleanupSchedulable implements Schedulable {

	public Interface IScheduleDispatched 
    { 
        void execute(SchedulableContext sc); 
    } 

	global void execute(SchedulableContext sc) {

		Type targettype = Type.forName('Axiom_RequestCleanupBatch');   
        if(targettype!=null) {
            IScheduleDispatched obj = (IScheduleDispatched)targettype.NewInstance();
            obj.execute(sc);   
       	}
	}
}