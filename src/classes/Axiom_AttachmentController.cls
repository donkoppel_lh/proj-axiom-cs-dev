/******* Axiom Attachment Controller *********
@Author Daniel Llewellyn/RedKite (dllewellyn@redkitetechnologies.com)
@Date ~9/2013
@Description Methods and properties related to the salesforce Attachment object.
**********************************************/

global class Axiom_AttachmentController
{
    /**
    * Counts the number of attachments for a parent object for any attachment passed in and sets the
    * Number_Of_Attachments__c field on the parent Request__c object accordingly.
    * @param attachments a list of Attachment objects for which to find parent records and calculate their total number of attachments
    * @return null */
    public static void setAttachmentCountOnRequest(list<Attachment> attachments)
    {
        list<id> attachmentsToProcess = new list<id>();
        
        //we are only interested in counting attachments for Request objects currently (maybe this should be abstracted? Worth the extra overhead?)
        for(attachment thisAttachment : attachments)
        {
            //due to namespacing issues (since in the future this object type will be namespaced but is not as of this code writting)
            //we use .contains to test the object type instead of equals.
            if(thisAttachment.parentId.getSobjectType().getDescribe().getName().contains('Request__c'))
            {
                attachmentsToProcess.add(thisAttachment.parentId);
            }
        }
        
        //if any of those attachments belonged to request objects, lets do our updates now.
        if(!attachmentsToProcess.isEmpty())
        {
            //Get the total count of attachments for the parent objects
            map<id,integer> attachmentCounts = Axiom_Utilities.countNumberOfAttachments(attachmentsToProcess);
            list<Request__c> requestsToUpdate = new list<Request__c>();
            
            //iterate over the key set of the map, which is the ID's the parent objects (requests)
            for(id record : attachmentCounts.keySet())
            {
                Request__c thisRequest = new Request__c(id=record);
                thisRequest.Number_Of_Attachments__c = attachmentCounts.get(record);
                requestsToUpdate.add(thisRequest);
            }
            update requestsToUpdate;
        }
    }
}