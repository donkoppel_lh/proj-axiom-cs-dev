@isTest
public class Axiom_TestGroupEntitlementTrigger {

    @isTest
    static void testGroupEntitlements()
    {
		Function__c thisFunction = new Function__c();
		thisFunction.Name = 'Test Function';
		
		Database.SaveResult srFunction = database.insert(thisFunction,false);
		//for (Database.SaveResult srFunction : srFunctionList ){
			if (srFunction.isSuccess()) {
				Group_Entitlement__c thisGroupEntitlement = new Group_Entitlement__c();
				thisGroupEntitlement.Function__c = srFunction.Id;
				List<Group> groupList = [SELECT Id from Group WHERE Name = 'Assignor - Axiom']; 
				thisGroupEntitlement.Group_Id__c = groupList[0].Id; 
		
				database.insert(thisGroupEntitlement,false);
			}
//		}
    }
}