@isTest
public class Axiom_TestRequestController
{
    @isTest
    static void testsetNumberOfDaysToFsr()
    {
        Axiom_Utilities.generateTestData();
        
        Test.StartTest();        
        
        //get request and update it's start and stop times to trigger the logic to run
        Request__c thisRequest = [select id,createdDate, name,Start_Time__c,Stop_Time__c from request__c limit 1];
        thisRequest.Start_Time__c = dateTime.now();
        thisRequest.Stop_Time__c = dateTime.now().addHours(5);

        update thisRequest;
        
        //get the request details to see if the days_open__c got updated
        thisRequest = [select id,createdDate, name,Start_Time__c,Stop_Time__c,Days_Open__c from request__c where id = :thisRequest.Id];
        
        //because we don't actually know the business hours of the target org, we just check to make it isn't null, since it should at leeast get set to SOMETHING, even 0
        system.assert(thisRequest.Days_Open__c != null);
        Test.StopTest();
          
    }

    @isTest
    static void testchangeMatterOwnersToRequestOwner()
    {
    
        Axiom_Utilities.generateTestData();

        user thisUser = [select id from user where id = :UserInfo.getUserId()];
        user adminAccount1;
        
        system.runAs(thisUser)
        {
            //Create portal account owner
            Profile admin = [Select Id from Profile where name = 'System Administrator'];
            adminAccount1 = new User(
                ProfileId = admin.Id,
                Username = System.now().millisecond() + 'test2@test.com',
                Alias = 'batman2',
                Email='bruce.wayne_admin@wayneenterprises.com',
                EmailEncodingKey='UTF-8',
                Firstname='Bruce',
                Lastname='Wayne',
                LanguageLocaleKey='en_US',
                LocaleSidKey='en_US',
                TimeZoneSidKey='America/Chicago'         
            );
            Database.insert(adminAccount1);
        }
                
        Test.StartTest();        
        
        //get the request
        Request__c thisRequest = [select id,ownerId from request__c limit 1];
        
        //find a new owner, it doesn't matter who. Anyone who isn't the current owner
        User newOwner = [select id from user where Id != :thisRequest.OwnerId and Profile.Name = 'System Administrator' limit 1];
        
        //set the request owner as the new owner
        thisRequest.ownerId = newOwner.Id;
        thisRequest.Start_Time__c = dateTime.now();
        //update the request
        try{
        update thisRequest;
        }catch(exception e){}
        map<id,Request__c> reqMap = new map<id,Request__c>();
        reqMap.put(thisRequest.id,thisRequest);
        
        Axiom_RequestController.changeMatterOwnersToRequestOwner(new map<id,Request__c>(),reqMap);
        //get the requests associated matters so we can see if they are the same as the new owner
        
        thisRequest = [select id,ownerId, (select name, ownerId, id from request__r) from request__c limit 1];
        for(Matter__c thisMatter : thisRequest.request__r)
        {
            //system.assertEquals(thisMatter.OwnerId,thisRequest.ownerId);
        }
                
        Test.StopTest();
          
    }  

    @isTest
    static void testCreateMattersFromRequest()
    {
        Axiom_Utilities.generateTestData();
        
        Test.StartTest();
        
        list<request__c> requests = [select id from request__c];
        list<account> parentAccount = [select id, name from account];
        list<Request_Agreement_Title__c> rats = new list<Request_Agreement_Title__c>();
        list<Legal_Entity_Role__c> lers = new list<Legal_Entity_Role__c>();
        list<Legal_Entity__c> legalEntities = new list<Legal_Entity__c>();
        
        Legal_Entity__c counterPartyLegalEntity = new Legal_Entity__c();
        counterPartyLegalEntity.Country_of_Incorporation__c = 'US';
        counterPartyLegalEntity.CSID__c = '999999';
        counterPartyLegalEntity.Description__c = 'Test Legal Entity';
        counterPartyLegalEntity.Company__c = parentAccount[0].id;
        counterPartyLegalEntity.Legal_Entity_Name__c = 'Test 1234';
        
        legalEntities.add(counterPartyLegalEntity);
        
        Legal_Entity__c creditSuisseLegalEntity = counterPartyLegalEntity.clone();
        legalEntities.add(creditSuisseLegalEntity);
        
        Legal_Entity__c indirectPrincipalLegalEntity = counterPartyLegalEntity.clone();
        legalEntities.add(indirectPrincipalLegalEntity);
        
        insert legalEntities;
                 
        list<id> requestIds = new list<id>();
        for(Request__c thisRequest : requests)
        {
            //create Request_Agreement_Titles__c for request
            Request_Agreement_Title__c thisRat = new Request_Agreement_Title__c();
            thisRat.Request__c = thisRequest.Id;
            thisRat.Agency__c = 'Test Agency Field';
            thisRat.CNID__c = '12345';
            thisRat.Description__c = 'Test Description Field';
            thisRat.Master_Agreement_Type__c = 'Test Master Agreement Type';
            
            rats.add(thisRat);
            
            //create Legal_Entity_Roles__c for request
            
            Legal_Entity_Role__c counterPartyLer = new Legal_Entity_Role__c();
            counterPartyLer.Counterparty_Role__c = 'CP-001';
            counterPartyLer.Role__c = 'Counterparty';
            counterPartyLer.Legal_Entity_Name__c = counterPartyLegalEntity.id;
            counterPartyLer.Request_Name__c = thisRequest.Id;
            
            lers.add(counterPartyLer);

            Legal_Entity_Role__c creditSuisseLer = new Legal_Entity_Role__c();
            creditSuisseLer.Counterparty_Role__c = 'CS-001';
            creditSuisseLer.Role__c = 'Credit Suisse Entity';
            creditSuisseLer.Legal_Entity_Name__c = creditSuisseLegalEntity.id;
            creditSuisseLer.Request_Name__c = thisRequest.Id;
                        
            lers.add(creditSuisseLer);

            Legal_Entity_Role__c indirectPrincipalLegalEntityLer = new Legal_Entity_Role__c();
            indirectPrincipalLegalEntityLer.Counterparty_Role__c = 'IP-001';
            indirectPrincipalLegalEntityLer.Role__c = 'Indirect Principal';
            indirectPrincipalLegalEntityLer.Legal_Entity_Name__c = indirectPrincipalLegalEntity.id;
            indirectPrincipalLegalEntityLer.Request_Name__c = thisRequest.Id;
                        
            lers.add(indirectPrincipalLegalEntityLer);            
            
            requestIds.add(thisRequest.id);
        }
        
        database.insert(rats);
        
        database.insert(lers);
        Axiom_RequestController.createMattersFromRequest(requestIds, true);
        
        Axiom_RequestController.createMattersFromRequest(requestIds, false);
        
        Test.StopTest();
    }      
}