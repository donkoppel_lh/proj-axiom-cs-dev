/******* Axiom Audit History *********
@Author Daniel Llewellyn/RedKite (dllewellyn@redkitetechnologies.com)
@Date ~9/2013
@Description Methods and properties for querying object field audit history and sending the changes via email or to a web service.
             This class is scheduable and currently will email the audit history only since there is no receiving webservice defined. 
             So while there is a scaffolding in place for making the HTTP request, there is no listener. Currently since this is scheduled in
             salesforce and makes a call to a listner, this would be considered a push style service. By exposing the getObjectFieldHistory method 
             via some kind of REST service we could easily make it a pull service as well where the remote service asks for the data whenever it wants it.
**********************************************/

global class Axiom_AuditHistory implements Schedulable
{
    public static Axiom_Misc_Settings__c settings = Axiom_Misc_Settings__c.getOrgDefaults();
    
    public static string httpEndpoint = settings.Audit_History_Endpoint__c != null ? settings.Audit_History_Endpoint__c : '';
    public static string username = settings.Audit_History_Endpoint_Username__c;
    public static string password = settings.Audit_History_Endpoint_Password__c;
    public static string[] emailRecipients = new string[]{settings.Audit_History_Email_Recipients__c}; 

    global void execute(SchedulableContext SC) {
        emailAuditHistory(); 
    }

    /**
    * takes an object type, a start time and and end time to get all audit history data for. Returns a custom data type/class called objectHistoryContainer that
    * is meant to be serialized into JSON before being sent via email or to the webservice. Class defenition is at the bottom of this class.
    * @param sObjectType a string that is the name of the type of sObject to get audit history data for
    * @param startdate a datetime string that acts as the beginning of the audit history
    * @param endDate a datetime string that acts as the ending of the audit history
    * @return a list of objectHistoryContainer records that detail what has been changed. See objectHistoryContainer inner class
    */
    public static list<objectHistoryContainer> getObjectFieldHistory(string sObjectType, date startDate, date endDate)
    {     
        //dynamic querystring for allowing querying of any kind of object    
        string queryString = 'Select Id, Name, CreatedById, CreatedDate, (Select Field, IsDeleted, NewValue, OldValue, ParentId, Id, CreatedDate, CreatedById From Histories where CreatedDate >= :startdate and CreatedDate <= :endDate) From ' + sObjectType;        
        list<sobject> records = database.query(queryString);
        list<objectHistoryContainer> objectHistories = new list<objectHistoryContainer>();
        
        
        for(sObject record : records)
        {
            //as far as I know there is no way to check for the existance of the histories property of the sObject without throwing a null error if it does 
            //not exist. Because it's an sObject you can't use contains key, and checking for null seems to error as well. It could just be a dumb oversight on my part
            //but for safety sake I wrap the logic in a try/catch before doing my if statment. We may also want to consider making an object history container for an object
            //event if it doesn't have any audit history records just for completness sake. That's really up to the receiving service if they want that or not though I guess.
            try
            {
                if(((list<sObject>) record.getSObjects('Histories')).size() > 0)
                {
                    objectHistoryContainer thisContainer = new objectHistoryContainer();
                    thisContainer.objectName = (string) record.get('name');
                    thisContainer.objectId = (id) record.get('id');
                    thisContainer.objectType =  sObjectType;
                    thisContainer.createdById = (id) record.get('CreatedById');
                    thisContainer.createdDate = (datetime) record.get('createdDate');
                    
                    for(sObject historyRecord : (list<sObject>) record.getSObjects('Histories'))
                    {
                        objectHistoryRecord thisHistoryRecord = new objectHistoryRecord();
                        thisHistoryRecord.oldValue = (string) historyRecord.get('oldValue');
                        thisHistoryRecord.newValue = (string) historyRecord.get('newValue');
                        thisHistoryRecord.createdById = (id) historyRecord.get('CreatedById');
                        thisHistoryRecord.objectId = (id) historyRecord.get('ParentId');
                        thisHistoryRecord.historyRecordId = (id) historyRecord.get('Id');
                        thisHistoryRecord.field = (string) historyRecord.get('field');
                        thisHistoryRecord.createdDate = (datetime) historyRecord.get('createdDate');
                        thisContainer.historyData.add(thisHistoryRecord);
                    }
                    objectHistories.add(thisContainer);
                    
                }
            }
            catch(exception e)
            {
                system.debug('\n\n\n\n---------------- ERROR GETTING OBJECT HISTORY DATA. AUDITNG MOST LIKELY NOT ENABLED FOR OBJECT ' + sObjectType);
                system.debug(e.getMessage() + ' ' + e.getLineNumber() );
                system.debug(e);
            }
        }
        return objectHistories;
    }
    
    /**
    * call to webservice that would be awaiting the data. Endpoint defined in Axiom_Misc_Settings__c custom setting
    * @param a JSON serialized string of objectHistory records
    * @return An httpresponse object that contains details about the result of the http request
    */
    public static HTTPResponse postObjectHistoryToRemote(string objectHistory)
    {

        HttpRequest req = new HttpRequest();
        
        if(httpEndpoint == null)
        {
            System.debug(LoggingLevel.ERROR,'Could not perform HTTP callout. No endpoint set. Please set in Axiom Misc Settings, HTTP Endpoint');
        }
        req.setEndpoint(httpEndpoint);
        req.setMethod('POST');
        
        // Specify the required user name and password to access the endpoint
        // As well as the header and header information. Feel free to comment out if 
        // the service is not password protected. Figured it was easier to leave auth in place and 
        // remove rather than try to dig up the code for auth if we need it. 
        

        
        Blob headerValue = Blob.valueOf(username + ':' + password);
        String authorizationHeader = 'BASIC ' +
        EncodingUtil.base64Encode(headerValue);
        req.setHeader('Authorization', authorizationHeader);
        req.setBody(objectHistory);
        
        // Create a new http object to send the request object
        // A response object is generated as a result of the request  
        
        Http http = new Http();
        HTTPResponse res = new HttpResponse();
        
        //can't make actual callouts in test mode, so a simple conditional to avoid such an error.
        //also don't bother making an actual callout if the httpEndpoint is null.
        if(!Test.isRunningTest() && httpEndpoint != null && httpEndpoint != '')
        {
            res = http.send(req);
        }
        else
        {
            res.setBody('[{"objectType":"Matter__c","objectName":"Christen Test","objectId":"a0Bi0000002noGkEAI","historyData":[{"oldValue":null,"objectId":"a0Bi0000002noGkEAI","newvalue":null,"historyRecordId":"017i0000017HOssAAG","field":"created","createdDate":"2013-08-16T00:06:27.000Z","createdById":"005i00000017nnnAAA"},{"oldValue":null,"objectId":"a0Bi0000002noGkEAI","newvalue":"Acknowledge","historyRecordId":"017i0000017JSxOAAW","field":"Step__c","createdDate":"2013-08-16T13:12:42.000Z","createdById":"005i00000017nnnAAA"},{"oldValue":null,"objectId":"a0Bi0000002noGkEAI","newvalue":"Pending","historyRecordId":"017i0000017JER2AAO","field":"Status__c","createdDate":"2013-08-16T13:12:42.000Z","createdById":"005i00000017nnnAAA"},{"oldValue":null,"objectId":"a0Bi0000002noGkEAI","newvalue":"Allocate","historyRecordId":"017i0000017JER1AAO","field":"Stage__c","createdDate":"2013-08-16T13:12:42.000Z","createdById":"005i00000017nnnAAA"}],"createdDate":"2013-08-16T00:06:27.000Z","createdById":"005i00000017nnnAAA"}]');
        }
        
        return res;      
    }
    
    /**
    * simple invocation that will send audit history data for the matter record for this month to the webservice.
    * @return HttpResponse of webservice callout that sends audit data to remote webservice.
    **/
    public static HTTPResponse sendHistoryData()
    {
        Date firstDayOfMonth = System.today().toStartOfMonth();
        Date lastDayOfMonth = firstDayOfMonth.addDays(Date.daysInMonth(firstDayOfMonth.year(), firstDayOfMonth.month()) - 1);
        
        list<objectHistoryContainer> matterHistory = getObjectFieldHistory('Matter__c', firstDayOfMonth, lastDayOfMonth );
        
        if(!matterHistory.isEmpty())
        {
            postObjectHistoryToRemote(JSON.serialize(matterHistory));
        }
        
        return null;
    }
    
    /**
    * Emails audit history for matter, request, and version objects for the last week and emails
    * the logs to the users defined in the emailRecipients public variable.
    * @return null
    */
    public static void emailAuditHistory()
    {
        //get the name of this org so we can include it in the email subject since we may be getting lots of reports from lots of different orgs.
        //its nice to know which report belongs to which org.
        Organization myOrg = [Select name From Organization];
    
        list<string> recipients  = emailRecipients;
        
        //find the objects we want to get data for. To make this more flexible due to namespacing concerns,
        //we will describe the org, find the objects we want (whether they be namespaced or not) and then include those.
        //so while we know we want history data for matter, request and version, we don't know if they will will have a namespace or not.
        List<Schema.SObjectType> gd = Schema.getGlobalDescribe().Values();   
        map<string,string> objectsLabelsToGetHistoryFor = new map<string,string>();
        
        objectsLabelsToGetHistoryFor.put('matter',null);
        objectsLabelsToGetHistoryFor.put('request',null);
        objectsLabelsToGetHistoryFor.put('version',null);
        
        set<string> objectNamesToGetHistoryFor = new set<string>();
        
        for(Schema.SObjectType f : gd)
        {
            string label = f.getDescribe().getLabel().toLowerCase();
            if(objectsLabelsToGetHistoryFor.containsKey(label))
            {
                objectsLabelsToGetHistoryFor.put(label,f.getDescribe().getName());
            }
        }  

        for(string objectLabel : objectsLabelsToGetHistoryFor.keySet())
        {
            string objectName = objectsLabelsToGetHistoryFor.get(objectLabel);
            if(objectName != null)
            {
                list<objectHistoryContainer> objectAuditHistoryData = getObjectFieldHistory(objectName, date.today().addDays(-7), date.today());
                sendAuditHistoryEmails(json.serializePretty(objectAuditHistoryData), 'Field Audit History Report for ' + objectLabel + ' From Org ' + myOrg.name, objectLabel, recipients);
            }
        }
    }
    
    /**
    * Handles the actual emailing of the audit history logs. Ensures that email governor limits are not exceeded.
    * @param content A string that contains the content of the email to send. The body basically.
    * @param subject A string to use for the email subject
    * @param objectType Just a label to use for the email to tell what kind of audit history data is contained
    * @param list<string> recipients, list of email addresses to send the email to.
    * @return null
    */
    public static void sendAuditHistoryEmails(string content, string subject, string objectType, list<string> recipients)
    {
        try
        {
            // First, reserve email capacity for the current Apex transaction to ensure                               
            // that we won't exceed our daily email limits when sending email after                             
            // the current transaction is committed.
            Messaging.reserveSingleEmailCapacity(recipients.size());
            
            // Processes and actions involved in the Apex transaction occur next,
            // which conclude with sending a single email.        
            // Now create a new single email message object
            // that will send out a single email to the addresses in the To, CC & BCC list.
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                          
            // Assign the addresses for the To and CC lists to the mail object.
            mail.setToAddresses(recipients);
            
            // Specify the address used when the recipients reply to the email. 
            mail.setReplyTo('noReply@axiomLaw.com');
            
            // Specify the name used as the display name.
            mail.setSenderDisplayName('Axiom Audit History Tracker bot');
            
            // Specify the subject line for your email address.
            mail.setSubject(subject);
            
            // Specify the text content of the email.
            mail.setPlainTextBody(content);
            
            mail.setHtmlBody('<b>Field History Audit For ' + objectType + ' For last 7 days</b>Please note the included content is JSON encoded. You can evaluate it and format it at http://jsonlint.com/<hr/><pre> '+ content +'</pre>');
        
            // Take the content
            Blob b = Blob.valueOf(content);
    
            // Create the email attachment
            Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
            efa.setFileName('Audit History '+objectType+'.json');
            efa.setBody(b);
            
            mail.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});         
            // Send the email you have created.

            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });    
        }
        catch(exception e)
        {
            System.debug(LoggingLevel.WARN,'Could not send audit history email to: ' +recipients+'. Error was: ' +e.getMessage() );
        }
    }
    /**
    * inner class that contains serializable form of object audit history data (default audit history record cannot be serialized)
    * contains list of objectHistoryRecords each of which is a single field change
    */
    public class objectHistoryContainer
    {
        public string     objectName;
        public Id         objectId;
        public string     objectType;
        public datetime   createdDate;
        public id         createdById;        
        public list<objectHistoryRecord> historyData = new list<objectHistoryRecord>();      
    }
    /**
    * inner class that represents a single field change
    */
    public class objectHistoryRecord
    {
        public string     oldValue;
        public string     newvalue;
        public datetime   createdDate;
        public id         createdById;
        public id         objectId;
        public id         historyRecordId;
        public string     field;
    }
}