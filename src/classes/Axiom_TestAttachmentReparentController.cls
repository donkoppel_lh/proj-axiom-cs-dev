@isTest
global class Axiom_TestAttachmentReparentController
{
    @isTest
    public static void testAttachmentReparentController()
    {
        Axiom_Utilities.generateTestData();
        
        Test.StartTest();
        
        Matter__c thisMatter = [select id from matter__c limit 1];
        Request__c thisRequest = [select id from request__c limit 1];
        Attachment thisAttachment = new Attachment();
        thisAttachment.name = 'Test Attachment';
        thisAttachment.body = blob.valueOf('Test Attachment File Body');
        thisAttachment.parentId = thisMatter.Id;
        insert thisAttachment;
        
        PageReference pageRef = Page.Axiom_AttachmentReparent;
         
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('sourceRecordId', thisMatter.id);
        ApexPages.currentPage().getParameters().put('targetRecordId', thisRequest.id);
        
        Axiom_AttachmentReparentController controller = new Axiom_AttachmentReparentController();
        
        system.assertEquals(controller.sourceAttachmentNames.size(),1);
        
        controller.targetAttachmentNames.addAll(controller.sourceAttachmentNames);
        controller.sourceAttachmentNames.clear();
        
        controller.save();
        
        list<attachment> targetAttachments = [select id from attachment where parentId = :thisRequest.id];
        list<attachment> sourceAttachments = [select id from attachment where parentId = :thisMatter.id];
        

        controller.sourceAttachmentNames.addAll(controller.targetAttachmentNames);
        controller.targetAttachmentNames.clear();    

        controller.save();
        
        targetAttachments = [select id from attachment where parentId = :thisRequest.id];
        sourceAttachments = [select id from attachment where parentId = :thisMatter.id];
                 
    }
    
}