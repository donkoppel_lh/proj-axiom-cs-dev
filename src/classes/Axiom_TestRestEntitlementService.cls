@isTest
global class Axiom_TestRestEntitlementService
{
    @isTest
    global static void testIsUserEntitledForFunction()
    {
        Test.StartTest();        

        map<string,string> params = new map<string,string>();
        params.put('userId',userInfo.getUserId());
        params.put('function','Accept Agreement Matter');
        RestResponse response = testService('isUserEntitledForFunction',params);             
        Test.StopTest();       
            
    }

    public static RestResponse testService(string methodName, map<string,string> params)
    {
        
        RestRequest req = new RestRequest();
        

        RestResponse res = new RestResponse();

        req.requestURI = '/entitlement/'+methodName;  //Request URL
        RestContext.request = req;
        RestContext.response= res;
        for(string param : params.keySet())
        {
            RestContext.request.params.put(param,params.get(param));
        }                
        Axiom_RestEntitlementService.doGet();
        
        system.debug(res.responseBody.toString());
        
        return RestContext.response;
    }               
}