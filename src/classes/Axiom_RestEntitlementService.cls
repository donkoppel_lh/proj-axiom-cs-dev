@RestResource(urlMapping='/entitlement/*')
global class Axiom_RestEntitlementService {

    @HttpGet   
    global static void doGet()
    {
        RestContext.response.addHeader('Content-Type', 'application/json');
        restResponseWrapper response = new  restResponseWrapper(RestContext.request);
        response.responseData = 'Waiting to invoke method';
        try
        {
            if(response.uriComponents.size() < 2) throw new customException('Please provid a method name to invoke');   
            response.methodName = response.uriComponents[1];
            
            if(response.methodName == 'isUserEntitledForFunction')
            {
                string userId = RestContext.request.params.containsKey('userId') ? RestContext.request.params.get('userId') : userInfo.getUserId();
                string functionName = encodingUtil.urlDecode(RestContext.request.params.get('function'),'UTF-8');
        
                response.responseData = Axiom_Utilities.getUserEntitlement(userId, functionName);            
            }
            
            if(response.methodName == 'acceptRequest')
            {
                id requestId= Id.valueOf(encodingUtil.urlDecode(RestContext.request.params.get('requestId'),'UTF-8'));
                boolean multiEntityTemplate = Boolean.ValueOf(encodingUtil.urlDecode(RestContext.request.params.get('multiEntityTemplate'),'UTF-8'));     
                
                response.responseData = Axiom_RequestController.acceptRequest(requestId, multiEntityTemplate );            
            }
            
            else
            {
                response.responseData = 'Invalid method name specified. Please provid a valid method and arguments';
            }
                        
         }
         catch(exception e)
         {
            response.success = false;
            response.responseData = 'Error ' + e.getMessage() + ' on line ' + e.getLineNumber() + ' Request Data ' + response;
         }
        
         RestContext.response.responseBody = formatResponse(response.responseData);        
    }

    //simple wrapper response class. Makes it so all replies via this API have the same basic structure
    //including a boolean success flag, a message, any sObjects affected and some debugging params.
    public class restResponseWrapper
    {
        public string message;
        public boolean success;
        private list<string> uriComponents; 
        public map<string,string> params;       
        public object responseData;
        public object methodName;
        private string requestURI;
        
        public restResponseWrapper(RestRequest reqContext)
        {
            uriComponents = getResourcePathChunks(reqContext.requestURI);
            message = 'run successful';
            success = true;
            params = reqContext.params;
            requestURI = reqContext.requestURI;
        }
    }    
    //take one of those wrapper objects and format it by wrapping it in a callback if needed, and serializing 
    //the result into json.    
    public static blob formatResponse(object responseData)
    {
        string response;
        String callback = RestContext.request.params.get('callback');
        if(callback != null)
        {
            response = callback + '(' + JSON.serialize(responseData) + ');';
        }    
        else
        {
            response = JSON.serialize(responseData);
        }
        return blob.valueOf(response);
    }  

    global static list<string> getResourcePathChunks(string url)
    {
        list<string> returnParams = new list<string>();
        
        
        integer endOfUrl = url.indexOf('?');
        if(endOfUrl == -1)
        {
            endOfUrl = url.length();
        }
        //clean up the url, make sure we are only dealing with the section we want to. After the host name, before the ? mark.
        //i've seen in come in with the full host name, and also just the relative portion, so we gotta make sure it's exactly the same
        //from here on in, or else the array it returns could get all messed up.
        if(url.indexOf('/apexrest/') > 0)
        {   
            url = url.substring(url.indexOf('/apexrest/')+10,endOfUrl);
        }  
        else
        {
            url = url.substring(1,endOfUrl);
        }            
        list<String> URLParams = url.split('/'); 
         
        for(string thisParam : urlParams)
        {
            returnParams.add(EncodingUtil.urlDecode(thisParam, 'UTF-8'));
         
        }   
        
        return returnParams;           
    }
    public class customException extends Exception {}
  
}