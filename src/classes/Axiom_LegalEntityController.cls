/******* Axiom Request Controller *********
Author Don Koppel/LiquidHub (dkoppel@liquidhub.com)
Date 8/2015
Description Methods and properties related to the salesforce Legal_Entity__c object.
**********************************************/
global class Axiom_LegalEntityController {

    //variable that tracks the related triggers amount of invocations to order to stop recurisve calls that cause governor limit errors
    public static integer invokeCount = 0;

	//This method applies Secrecy Group updates to Legal Entity Role records where the Secrecy Group has been changed on the Legal Entity.
	//This is needed to maintain the Secrecy Group value on the Request which is used to drive data visibility 
	public static void updateSecrecyGroup(List<Legal_Entity__c> newList, List<Legal_Entity__c> oldList){
		Map<Id, String> legalEntSecrecyGroupChanged = new Map<Id, String>();	//Map of Legal Entity records where Secrecy Gtoup has been modified
		
		//Loop through all updated Legal Entities; add to the map and records where the Secrecy Group has been changed
		for(integer i = 0; i< newList.size(); i++){
			if (newList[i].Secrecy_Group__c <> oldList[i].Secrecy_Group__c)
				legalEntSecrecyGroupChanged.put(newList[i].Id, newList[i].Secrecy_Group__c);
		}
		
		//For all Legal Entities where the Secrecy Group has been changed, find all of their child Legal Entity Role records and 
		//update the Secrecy Group on those records
		if(legalEntSecrecyGroupChanged.size() > 0){
			List<Legal_Entity_Role__c>legalEntRoleSecrecyGroupChanged = [SELECT Id, Legal_Entity_Name__c, Secrecy_Group__c 
																			FROM Legal_Entity_Role__c
																			WHERE Legal_Entity_Name__c IN :legalEntSecrecyGroupChanged.keyset()];

			for (Legal_Entity_Role__c legalEntRole : legalEntRoleSecrecyGroupChanged){
				legalEntRole.Secrecy_Group__c = legalEntSecrecyGroupChanged.get(legalEntRole.Legal_Entity_Name__c);
			}
			
			if (legalEntRoleSecrecyGroupChanged.size() > 0){
				update legalEntRoleSecrecyGroupChanged;
			}	
		}
	}
}