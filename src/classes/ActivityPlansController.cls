/******* Activity Plans Controller *********
@Author Daniel Llewellyn/RedKite (dllewellyn@redkitetechnologies.com)
@Date ~1/2014
@Description Handles the creation of 'activity plans'. An activity plan is basically just a pre defined list of tasks that need to get created
             for a given object type when certain conditions are met. It also allows for fields on the task to get set in a certain way. So for
             example we could have an activity plan for matters with a condition that says 'stage' 'equals' 'started' then when a matter meets
             that condition (and any other ones defined for that plan) the tasks defined for that plan will be created and attached to that matter.
             Please note that activity plans are not processed when invoked via a batch process. The processing overhead is currently too much to 
             stay within governor limits safely and so if a batch process invocation is detected, the process will abort. Activity plans also has
             recursive invocation detection that will prevent the process from being invoked a second time as a result any possible record creation/update
             that would normally cause activity plans to fire. These two safety settings can be toggled via the Activity Plans Settings custom setting.
**********************************************/
global class ActivityPlansController
{
    global static boolean hasRun = false;
    global static integer invokeCount = 0;
    
    //gets org default activity plans settings record. If one does not exist, creates one and inserts it, then returns it.
    public static Activity_Plans_Settings__c getSettings()
    {
        Activity_Plans_Settings__c CS = Activity_Plans_Settings__c.getOrgDefaults();
        if(CS == null)
        {
            CS = new Activity_Plans_Settings__c();
            CS.Prevent_Batch_Job_Invocation__c = true;
            CS.Prevent_Recursive_Invocation__c = true;
            insert CS;
        }
        
        return CS;
    }

    //given a list of objects find any matching activity plans. This also handles the creation of said tasks. This could probably be abstracted a tad
    //but its convienient to only have one method to call from a trigger, especially when the two actions go hand in hand like this.    
    public static void findMatchingActivityPlans(list<sObject> sourceObjects)
    {
        System.debug(LoggingLevel.WARN,'\n\n\n ***********************   Beginning activity plans logic *******************\n\n\n');
        
        //get the settings
        Activity_Plans_Settings__c settings = getSettings();
        //prevent accidental recursive runs (tasks getting closed could cause a recursive loop possibly when the parent object gets updated)
        if( (hasRun && settings.Prevent_Recursive_Invocation__c) || (system.isBatch() && settings.Prevent_Batch_Job_Invocation__c) || invokeCount > settings.Maximum_Iterations__c)
        {
            System.debug(LoggingLevel.WARN,'\n\n\n ***********************   Aborting Activity Plans Processing. Recursive invocation or batch job invocation detected. *******************\n\n\n');
            return;
            
        }
        hasRun = true;
        invokeCount++;
        //find out what kind of object we are dealing with. First try to figure it out from the list. If the list is generic and cannot deduce
        //object type from it, then just use the first object in the list as the type.
        string sobjectType;
        try
        {
            sobjectType = sourceObjects.getSObjectType().getDescribe().getName();
        }
        catch(exception e)
        {
            sobjectType = sourceObjects[0].getSObjectType().getDescribe().getName();
        }
        //create a container for all the templates that will apply to each object
        map<sobject,list<Activity_Plan_Task_Template__c>> objectsToCreateTasksFor = new map<sobject,list<Activity_Plan_Task_Template__c>>() ;
        
        //map that contains ID's of task templates and the rules that apply to them specifically.
        map<Id,list<Activity_Plan_Rule__c>> taskTemplateRules = new map<id,list<Activity_Plan_Rule__c>>();
                
        //find all activity plans that apply to this object type that are active. Also find the associated rules and task templates.
        list<Activity_Plan__c> matchingPlans = [select 
                                                    name,
                                                    id,
                                                    Active__c,
                                                    Object_Types__c,
                                                    (select
                                                         Activity_Plan__c,
                                                         Comparison_Value__c,
                                                         Comparison_Field_Name__c,
                                                         Comparison_Type__c,
                                                         Activity_Plan_Task_Template__c,                                                        
                                                         Logical_Operator__c,
                                                         Assignment_Field_Name__c,
                                                         Assignment_Type__c,
                                                         Assignment_Value__c                                                        
                                                     from
                                                         Activity_Plan_Field_Updates__r),
                                                    (select 
                                                         Name,
                                                         Activity_Plan__c,
                                                         Assigned_To_Type__c,
                                                         Assigned_To_Value__c,
                                                         Due_Date_Type__c,
                                                         Due_Date_Value__c,
                                                         Priority__c,
                                                         Related_To_Type__c,
                                                         Related_To_Value__c,
                                                         Task_Status__c,
                                                         Record_Type_Name__c,
                                                         Allow_Duplicate_Tasks__c,
                                                         Depends_On__c,
                                                         Depends_On_Completion_Statuses__c,
                                                         Depends_On__r.Name,
                                                         Description__c,
                                                         Notification_Email_Template__c,
                                                         Task_Weight__c,
                                                         Due_Date_Offset_Source__c,
                                                         Order__c,
                                                         Substantive_Response__c 
                                                     from 
                                                         Activity_Plan_Task_Templates__r),
                                                         
                                                    (select
                                                         Name,
                                                         Activity_Plan__c,
                                                         Comparison_Value__c,
                                                         Activity_Plan_Task_Template__c,
                                                         Field_Name__c,
                                                         Logical_Operator__c,
                                                         Comparison_Type__c
                                                     from
                                                         Activity_Plan_Rules__r)      
                                                 from Activity_Plan__c
                                                 where Object_Types__c like :sobjectType and Active__c = true];  
        
        //iterave over all the passed in objects to see what plans apply
        for(sObject thisObj : sourceObjects)
        {
            //by default we will say the template applies, then set it to false if it doesnt pass any of the rules.
            boolean templateApplies = true;
            
            //iterate over every plan to see if this object matches all the conditions.
            for(Activity_Plan__c thisPlan : matchingPlans)
            {
                //reset variable incase previous plan didnt pass.
                templateApplies = true;
                
                //evaluate all the rules on this activity plan to see if object matches all of them
                //if so, we'll add it to the list. Otherwise, skip it.
                for(Activity_Plan_Rule__c thisRule : thisPlan.Activity_Plan_Rules__r)
                {
                    //Activity_Plan_Task_Template__c must be null to continue processing as normal. Rules that do not have a Activity_Plan_Task_Template__c value specified apply to the entire plan
                    //and rules that do have a value specified apply to a specific task within this plan. If it is null, record that this rule belongs to a specific activity plan task
                    //template and move onto the next rule.                    
                    if(thisRule.Activity_Plan_Task_Template__c != null)
                    {
                        //get or create list of activity plan rules for the task template referenced by this rule.
                        list<Activity_Plan_Rule__c> thisTemplatesRules = taskTemplateRules.containsKey(thisRule.Activity_Plan_Task_Template__c) ?  taskTemplateRules.get(thisRule.Activity_Plan_Task_Template__c) : new list<Activity_Plan_Rule__c>();
                        thisTemplatesRules.add(thisRule);
                        
                        taskTemplateRules.put(thisRule.Activity_Plan_Task_Template__c,thisTemplatesRules);
                        continue;
                    }
                    //perform dynamic evalation of condition. Since the syntax of apex doesnt allow for an eval type statment, or a dynamic if statment
                    //use this method to figure out if this rule is true for this object. If it doesnt, abandon further rule process for this plan and move 
                    //to the next one.       
                    
                    //There are two different types of comparisons that can be made currently. Either compare the source value against a static value entered by the user
                    //at rule creation time, or compare the field against another field on the object. If the user is comparing a static value the comparison value field will
                    //hold the value to compare against. If the rule is setup to compare against another field, the comparison value field will hold the name of the field to evaluate.

                    object compareVal;
                    if(thisRule.Comparison_Type__c == 'Static Value' || thisRule.Comparison_Type__c == '')
                    {
                        compareVal = thisRule.Comparison_Value__c;
                        
                        //special case handler for if the user wants to compare against null. Since you cannot leave the compareVal field blank
                        //users can enter the word null to do an actual null comparison.
                        if(string.valueOf(compareVal).toLowerCase() == 'null')
                        {
                            system.debug('\n\n\n\n-----------------------COMPARISON VALUE IS NULL. SETTING TO EMPTY STRING!');
                            compareVal = null;
                        }    
                        
                        //special case handler for user wants to compare against blank. Since you cannot leave the compareVal field blank, users
                        //can enter a pair of single quotes to do a compare against a blank field value.
                        if(compareVal == '\'\'')
                        {
                            system.debug('\n\n\n\n-------------------------COMPARISON VALUE IS EMPTY STRING. SETTING TO EMPTY STRING!');
                            compareVal = '';
                        }
                    }     
                    else if(thisRule.Comparison_Type__c == 'Object Field Value')
                    {
                        compareVal = thisObj.get(thisRule.Comparison_Value__c);
                    }
                    
                    system.debug('\n\n\n\n\n----------------------------- COMPARING: ' + thisRule.Field_Name__c + ' - ' + thisObj.get(thisRule.Field_Name__c) + ' ' + thisRule.Logical_Operator__c + ' ' + compareVal);
                    if(!dynamicIf(thisObj.get(thisRule.Field_Name__c), 
                                 thisRule.Logical_Operator__c,
                                 compareVal))
                    {
                        system.debug('\n\n\n\n\n----------------------------- COMPARISON WAS NOT TRUE! BREAKING PLAN');    
                        templateApplies = false;
                        break;
                    }
                } 
                
                //if after all logical checks this template applies, then add it to the list
                //of objects to create templates for.
                if(templateApplies)
                {                   
                    list<Activity_Plan_Task_Template__c> thisObjectsPlans = objectsToCreateTasksFor.containsKey(thisObj) ? objectsToCreateTasksFor.get(thisObj) : new list<Activity_Plan_Task_Template__c>();
                    thisObjectsPlans.addAll(thisPlan.Activity_Plan_Task_Templates__r);
                    
                    objectsToCreateTasksFor.put(thisObj,thisObjectsPlans);                
                    
                }                
            }
            if(!objectsToCreateTasksFor.isEmpty())
            {
                list<task> createdTasks = createTasksFromTemplates(objectsToCreateTasksFor,taskTemplateRules);
            }

        }
    }
    
    //performs logical comparisons on generic objects. Can compare data types that are only known at runtime
    //using logic only known at runtime. one thing i love is that you can pass in generic objects and then simply
    //cast them to whatever you need.
    public static boolean dynamicIf(object srcValueOne, string logicalOperator, object srcValueTwo)
    {

    
        //cast these generic objects to strings for easy comparison.
        string valueOne = srcValueOne == null ? '' : string.valueOf(srcValueOne).toLowerCase(); //make sure to handle nulls
        string valueTwo = srcValueTwo == null ? '' : string.valueOf(srcValueTwo).toLowerCase(); //make sure to handle nulls             
        string logical = logicalOperator.toLowerCase().replace(' ','');

        //yes you can compare generic objects. Awesome huh?!
        if(logical == 'equal' && valueOne == valueTwo )
        {
            return true;
        }
        else if((logical == 'notequal' || logical == 'doesnotequal') && valueOne != valueTwo)
        {
            return true;
        }
        //for less than/greater than comparisons our objects have to be cast to some kind of numeric. Double is probably the safest.
        //of course if the values passed in are not numerics and the user has attempted to compare them as such, we are going to get an error.
        //not much you can do about that. Garbage in, garbage out, etc.
        else if(logical == 'greaterthan' && double.valueOf(valueOne) > double.valueOf(valueTwo))
        {
            return true;
        }         
        else if(logical == 'lessthan' && double.valueOf(valueOne) < double.valueOf(valueTwo))
        {
            return true;
        }                          
        else if(logical == 'contains' && valueOne.indexOf(valueTwo) > -1)
        {
            return true;
        }       
        else if(logical == 'doesnotcontain' && valueOne.indexOf(valueTwo) < 0)
        {
            return true;
        }             
        return false;
    }
   
    //actually creates the tasks on an activity plan for given sObjects.
    public static list<task> createTasksFromTemplates(map<sobject,list<Activity_Plan_Task_Template__c>> objectMap, map<Id,list<Activity_Plan_Rule__c>> templateRules)
    {
        ActivityPlans_Utilities utilities = new  ActivityPlans_Utilities();
        Map<String, Schema.SObjectField> taskObjectFields = Task..sObjectType.getDescribe().fields.getMap();
         
        list<task> createdTasks = new list<task>();
        map<string,map<string,task>> existingTasksMap = new map<string,map<string,task>>();
        map<id,list<Activity_Plan_Task_Field__c>> templateToFieldsMap = new map<id,list<Activity_Plan_Task_Field__c>>();
        
        
        map<string,id> taskRecordTypes = new map<string,id>();
        map<id,Activity_Plan_Task_Template__c> taskTemplateMap = new map<id,Activity_Plan_Task_Template__c>();
        set<Id> activityPlanIds = new set<id>();
        
        //we need to figure out what tasks the objects passed in already have so we don't create duplicates (unless the user has set that flag on the plan).
        //so what we'll do is query for all existings tasks that have a Created_By_Plan__c that is one of the plans that is passed in
        
        //we are also going to need to get the Id of every potential task template so we can find all associated task fields (rules that set additional values on the task at creation time beyond
        //those available in the stanard config). 
        
        for(list<Activity_Plan_Task_Template__c> plans : objectMap.values())
        {
            for(Activity_Plan_Task_Template__c thisTemplate : plans)
            {
                activityPlanIds.add(thisTemplate.Activity_Plan__c);
                templateToFieldsMap.put(thisTemplate.id,new list<Activity_Plan_Task_Field__c>());
            }          
        }

        list<Activity_Plan_Task_Field__c> taskFields = [select
                                                             Name,
                                                             Active__c,
                                                             Assignment_Type__c,
                                                             Assignment_Value__c,
                                                             Task_Field__c,
                                                             Activity_Plan_Task_Template__c,
                                                             Assignment_Field_Name__c
                                                          from
                                                             Activity_Plan_Task_Field__c
                                                          where Activity_Plan_Task_Template__c in :templateToFieldsMap.keySet() and
                                                                active__c = true];
                                                          
        //put all these task fields into a map keyed by the template so when we go to actually create the task we can look in this map to find all the fields we need to set
        for(Activity_Plan_Task_Field__c thisField : taskFields)
        {
            list<Activity_Plan_Task_Field__c> templateFields = templateToFieldsMap.get(thisField.Activity_Plan_Task_Template__c);
            templateFields.add(thisField);
            templateToFieldsMap.put(thisField.Activity_Plan_Task_Template__c,templateFields);
        }
                                                          
        //find all tasks that have a whatId of one of our objects
        list<task> existingsTasks = [select id, 
                                            WhatId, 
                                            Created_by_Template__c, 
                                            Created_By_Plan__c,
                                            Activity_Plan_Series_ID__c,
                                            Status, 
                                            Subject 
                                     from task 
                                     where Created_By_Plan__c in :activityPlanIds];
        
        system.debug('\n\n\n\n ALL EXISTING TASKS ON OBJECT');
        system.debug(existingsTasks);
        
        for(task thisTask : existingsTasks)
        {
            //tricky little ternary operation to either get the existing task map for this object, or create a new one
            //if it doesn't exist. Just know this line will return a set of task titles to add onto.
            map<string,task> thisObjTasks = existingTasksMap.containsKey(thisTask.Activity_Plan_Series_ID__c) ? existingTasksMap.get(thisTask.Activity_Plan_Series_ID__c) : new map<string,task>();
            thisObjTasks.put(thisTask.Created_by_Template__c,thisTask);
            existingTasksMap.put(thisTask.Activity_Plan_Series_ID__c,thisObjTasks);
        }
        
        //get all the task record types
        list<RecordType> recTypes = [select id, developerName from RecordType where SobjectType = 'task'];
        for(RecordType thisRecordType : recTypes)
        {
            taskRecordTypes.put(thisRecordType.developerName,thisRecordType.Id);
        }
        
        for(sobject thisObj : objectMap.keySet())
        {
            for(Activity_Plan_Task_Template__c thisTemplate : objectMap.get(thisObj))
            {
                string seriesId = thisTemplate.Activity_Plan__c + '-' + thisObj.Id;
                
                taskTemplateMap.put(thisTemplate.Id,thisTemplate);

                map<string,task> existingObjTasks = existingTasksMap.get(seriesId);
                string ownerId = (string) thisObj.get((string) thisTemplate.get('Assigned_To_Value__c'));            
                //if the assigned owner is null then just default it to the current user.
                ownerId = ownerId != null ? ownerId : userInfo.getUserId();
                
                //figure out the name of the task that this task depends on, if it has one.
                string parentTaskId = thisTemplate.Depends_On__r.Id;                
                              
                //now we know the NAME of the task that this one depends on, get a reference to that task so we can check its status             
                Task parentTask = thisTemplate.Depends_On__c != null && existingObjTasks != null &&  existingObjTasks.containsKey(parentTaskId) ? existingObjTasks.get(parentTaskId) : null;

                system.debug('FOUND PARENT TASK FOR TEMPLATE ' + thisTemplate.name);
                system.debug(parentTask);
                system.debug('\n\n\n----------------------Looking for  ' + parentTaskId);
                
                //check to see if this object has this task already. If so, skip it so we don't 
                //create a ton of duplicate tasks. Also ensure the assigned owner value for the task is a user,
                //to avoid trying to assign to a queue or other invalid value. Additionally check to see if this task has a parent task, and if so, is it it 
                //one of the completion statuses that is required to trigger creation of the task.                
                
                //make sure this isn't null.
                if(existingObjTasks == null)
                {
                    existingObjTasks = new map<string,task>();
                }

                boolean templateApplies = true;
                if(templateRules.containsKey(thisTemplate.Id))
                {
                    for(Activity_Plan_Rule__c thisRule : templateRules.get(thisTemplate.Id))
                    {
                        system.debug('\n\n\n\n------------------------------------- LOOKING AT RULE: ' + thisRule.Name);
                        //perform dynamic evalation of condition. Since the syntax of apex doesnt allow for an eval type statment, or a dynamic if statment
                        //use this method to figure out if this rule is true for this object. If it doesnt, abandon further rule process for this plan and move 
                        //to the next one.       
                        
                        //There are two different types of comparisons that can be made currently. Either compare the source value against a static value entered by the user
                        //at rule creation time, or compare the field against another field on the object. If the user is comparing a static value the comparison value field will
                        //hold the value to compare against. If the rule is setup to compare against another field, the comparison value field will hold the name of the field to evaluate.
    
                        object compareVal;
                        if(thisRule.Comparison_Type__c == 'Static Value' || thisRule.Comparison_Type__c == '')
                        {
                            compareVal = thisRule.Comparison_Value__c;
                            
                            //special case handler for if the user wants to compare against null. Since you cannot leave the compareVal field blank
                            //users can enter the word null to do an actual null comparison.
                            if(compareVal == 'null')
                            {
                                compareVal = null;
                            }    
                            
                            //special case handler for user wants to compare against blank. Since you cannot leave the compareVal field blank, users
                            //can enter a pair of single quotes to do a compare against a blank field value.
                            if(compareVal == '\'\'')
                            {
                                compareVal = '';
                            }
                        }     
                        else if(thisRule.Comparison_Type__c == 'Object Field Value')
                        {
                            compareVal = thisObj.get(thisRule.Comparison_Value__c);
                        }
                        
                        system.debug('\n\n\n----------------- RESULT OF DYNAMIC IF FOR TEMPLATE APPLIES: ' + dynamicIf(thisObj.get(thisRule.Field_Name__c), thisRule.Logical_Operator__c,compareVal));
                        
                        if(!dynamicIf(thisObj.get(thisRule.Field_Name__c), 
                                     thisRule.Logical_Operator__c,
                                     compareVal))
                        {
                            templateApplies = false;
                        }
                    }                   
                }
                
                boolean taskExists = existingObjTasks.containsKey(thisTemplate.Id); 
                thisTemplate.Depends_On_Completion_Statuses__c =  thisTemplate.Depends_On_Completion_Statuses__c != null ?   thisTemplate.Depends_On_Completion_Statuses__c : ''; 
                
                
                system.debug('Looking at task template ' + thisTemplate.Name);
                system.debug('Task already exists? : ' + taskExists);
                system.debug('Owner is legit?: ' + ownerId.substring(0,3));
                system.debug('Template applies?: ' + templateApplies);
                system.debug('THIS TASK DEPENDS ON ' + parentTaskId);
                system.debug('Depends on Completion Status: ' + thisTemplate.Depends_On_Completion_Statuses__c); 
                try
                {
                    system.debug('Parent Status in status list' + thisTemplate.Depends_On_Completion_Statuses__c.toLowerCase().contains(parentTask.status.toLowerCase()));
                }
                catch(exception ex)
                {
                    system.debug('Error reading data! ' + ex.getMessage() );
                }               
                if( (existingObjTasks == null || !taskExists || thisTemplate.Allow_Duplicate_Tasks__c == true) 
                    && ownerId.substring(0,3) == '005' && templateApplies
                    && ( parentTaskId ==null || (parentTask != null && parentTask.status != null && thisTemplate.Depends_On_Completion_Statuses__c != null
                    && thisTemplate.Depends_On_Completion_Statuses__c.toLowerCase().contains(parentTask.status.toLowerCase()))))
                {
                    task thisTask = new Task();
                    thisTask.status = (string) thisTemplate.Task_Status__c;
                    thisTask.priority = (string) thisTemplate.Priority__c;
                    thisTask.subject = (string) thisTemplate.Name;
                    thisTask.description =  parseMergeText(thisTemplate.Description__c, thisObj);                            
                    thisTask.Created_by_Template__c = (string) thisTemplate.Id;
                    thisTask.Task_Weight__c = (double) thisTemplate.Task_Weight__c;
                    thisTask.Creation_Method__c = 'Automatic';
                    thisTask.Order__c = thisTemplate.Order__c;
                    thisTask.First_Substantive_Response__c = thisTemplate.Substantive_Response__c;
                    thisTask.Activity_Plan_Series_ID__c = thisTemplate.Activity_Plan__c + '-' + thisObj.id;
                    thisTask.Created_By_Plan__c = thisTemplate.Activity_Plan__c;                    
                    //figure out what this task is related to
                    if(thisTemplate.get('Related_To_Type__c') == 'Field Value')
                    {
                        thisTask.whatId = (id) thisObj.get((string) thisTemplate.get('Related_To_Value__c'));
                    }
                    else if(thisTemplate.get('Related_To_Type__c') == 'Static ID')
                    {
                        thisTask.whatId = (id) thisTemplate.get('Related_To_Value__c');
                    }
                    
                    //figure out when this task is due
                    if(thisTemplate.get('Due_Date_Type__c') == 'Static Date')
                    {
                        thisTask.ActivityDate = date.parse((string) thisTemplate.get('Due_Date_Value__c'));
                    }
                    else if(thisTemplate.get('Due_Date_Type__c') == 'Day Offset')
                    {
                        //figure out what field in any the offset is based on. If null, then the offset is based on the current date.
                        string sourceDateField = (string) thisTemplate.get('Due_Date_Offset_Source__c');
                        
                        //get the date from the parent object as long as the sourceDateField isn't null. Otherwise just default to today.
                        date sourceDate = sourceDateField != null && sourceDateField != '' ? (date) thisObj.get(sourceDateField) : date.today();
                        
                        //modify the date using the user specified offset.
                        thisTask.ActivityDate = sourceDate.addDays(integer.valueOf(thisTemplate.get('Due_Date_Value__c')));
                    } 
    
                    //figure out who this task is assigned to
                    if(thisTemplate.get('Assigned_To_Type__c') == 'Static Value')
                    {
                        thisTask.OwnerId = (id) ownerId;
                    }
                    else if(thisTemplate.get('Assigned_To_Type__c') == 'Field Value')
                    {
                        thisTask.OwnerId =ownerId;
                    }  
                    if(thisTemplate.Record_Type_Name__c != null &&  taskRecordTypes.containsKey(thisTemplate.Record_Type_Name__c))
                    {
                        thisTask.recordTypeId = taskRecordTypes.get(thisTemplate.Record_Type_Name__c);
                    }

                    system.debug('\n\n\n\n\n-------------------------------------------- Adding task ' + thisTask);
                    
                    //finally lets iterate over all the task fields defined for this task template that will set any additonal fields for this task
                    if(templateToFieldsMap.containsKey(thisTemplate.Id))
                    {
                        list<Activity_Plan_Task_Field__c> templateFields = templateToFieldsMap.get(thisTemplate.Id);
                        
                        for(Activity_Plan_Task_Field__c thisField : templateFields)
                        {
                            try
                            {
                                sObjectField  fieldData = taskObjectFields.get(thisField.Task_Field__c);
                                if(thisField.Assignment_Type__c == 'Static Value')
                                {
                                    thisTask = (task) utilities.putValueWithDynamicCasting(thisTask, thisField.Task_Field__c,  fieldData, thisField.Assignment_Value__c);
                                }    
                                else if(thisField.Assignment_Type__c == 'Field Value')
                                {
                                    thisTask = (task) utilities.putValueWithDynamicCasting(thisTask, thisField.Task_Field__c,  fieldData, thisObj.get(thisField.Assignment_Field_Name__c));
                                }                             
                            }
                            catch(exception e)
                            {
                                system.debug('\n\n\n\n\n-------------------------------------------- ERROR ADDING VALUE TO TASK. FIELD: ' + thisField.Task_Field__c + ' VALUE: ' + thisField.Assignment_Value__c);
                                system.debug(e);
                            }
                        }
                    }
                    createdTasks.add(thisTask);
                    
                }
            }
        }
        
        //save tasks to the database
        list<database.saveResult> results = new list<database.saveResult>();
        //save tasks to the database
        integer totalCounter = 0;
 
        //tricky method for inserting tons of tasks at once. 
        list<task> taskBatch = new list<task>();
        for(task thisTask : createdTasks)
        {
            taskBatch.add(thisTask);
            totalCounter++;
            if(taskBatch.size() == 200 || totalCounter == createdTasks.size())
            {
                results.addAll(database.insert(createdTasks,false));
                system.debug('\n\n\n\n -------------- INSERTED TASKS!');
                taskBatch.clear();
            }
        }
        
        
        //iterate the results to find which tasks were saved correctly. For every saved task that has it's send email flag set to true
        //inform the owner.
        for(Integer i=0; i < results.size(); i++)
        {
            Database.SaveResult result = results[i];
            if(result.isSuccess())
            {
                //get a reference to the task this is a save result for. the save result order is the same as the order of the inserted collection
                //so using the index of this current save result, we can get at the correct task it represents.
                sObject task = createdTasks[i];
                
                //now to find the email template we are supposed to use we need to the fint the correct task template this task was created from.
                //the task is branded with the id of the template that created it, so using that we can look in the map of template ids to template
                //data and get the notification email field. If that is not null, then attempt to send an email to the assigned user. Crazyness I know.
                string emailTemplate = taskTemplateMap.get((string) task.get('Created_by_Template__c')).Notification_Email_Template__c;
                if(emailTemplate != null)
                {
                    sendEmailTemplate(emailTemplate, (string) task.get('OwnerId'),(string) task.get('id'));
                }
            }
            else
            {
                system.debug('\n\n\n\n------------------------- ERROR INSERTING TASK');
                system.debug(result);
            }
        }
        return createdTasks;
    }

    

    public static string parseMergeText(string mergeText, sobject sourceObject)
    {
        if(mergeText == null)
        {
            return mergeText;
        }
        set<String> allMatches = new set<String>();
        
        Pattern MyPattern = Pattern.compile('\\[(.*?)\\]');   
        
        Matcher MyMatcher = MyPattern.matcher(mergeText);
        
        while(MyMatcher.find())
        {
            allMatches.add(MyMatcher.group());    
        }
        
        //the matcher finds all parts of the merge text that have [something.something] syntax. Iterate over all those items
        for(string match : allMatches)
        {
            string cleanMatch = match;
            
            //remove the brackets from either side of the match
            cleanMatch = cleanMatch.replace(']','');
            cleanMatch = cleanMatch.replace('[','');
            
            //split the match string into the it's separate parts (before and after the period). Before the period is the object type
            //and after is the property to read from that object. Currently only 'object' is supported as the object to read data from. Later we
            //might support things like 'user' or 'org'
            list<string> matchParts = cleanMatch.split('\\.');
            if(matchParts[0] == 'object' && matchParts[1] != null)
            {
                try
                {
                    mergeText = mergeText.replace(match,(string) sourceObject.get(matchParts[1]));
                }
                catch(exception e)
                {
                    mergeText = '[ERROR: Field '+ matchParts[1] + ' Is not available on parent object]';
                }
            }
        }
        return mergeText;
    }
    
    public static boolean sendEmailTemplate(string templateId, string userId, id objectInfo)
    {
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
         
        EmailTemplate et = [SELECT id FROM EmailTemplate WHERE developerName = :templateId or Id = :templateId limit 1];
         
        mail.setSenderDisplayName('Taskbot 5000');
         
        mail.setTargetObjectId(userid); // Specify who the email should be sent to.
        mail.setTemplateId(et.id);
        mail.setWhatId(objectInfo);
        mail.saveAsActivity = false;
         
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail}); 
        
        return true;   
    }
   
}