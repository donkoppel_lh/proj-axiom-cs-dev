/******* Axiom File Upload Controller *********
@Author Daniel Llewellyn/RedKite (dllewellyn@redkitetechnologies.com)
@Date ~4/2013
@Description  Provides controller logic for visualforce page Axiom_UploadFile for
              uploading files from the client portal. An ID of a parent record is passed to the
              page (?parent=) and when a file is uploaded it is then attached to that record as an attachment.
 
**********************************************/
public class Axiom_FileUploadController {
 
    public id parentRecordId{get;set;}
    public sObject parentObject{get;set;}
    public string parentObjectLabel{get;set;}
    public String contentType {get; set;}
    
    /*
    Constructor for the class, invoked when the page is loaded. Gets the parent record id and puts it in scope
    as well as the some information about the record (name, object type). Does not take an parameters directly
    but does expect an sObject ID to be passed in the 'parent' url argument.
    @return null
    */
    public Axiom_FileUploadController() {
        parentRecordId = ApexPages.currentPage().getParameters().get('parent');
        if(parentRecordId == null)
        {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Parent Record Not Specified. Please pass parent record id in url using ?parent=objectId'));
        }
        else
        {
            string objectType = parentRecordId.getSObjectType().getDescribe().getName();
            parentObjectLabel = parentRecordId.getSObjectType().getDescribe().getLabel();
            string queryString = 'select name, id from ' +objectType+ ' where id = :parentRecordId';
            list<sobject> objectData = database.query(queryString);
            if(!objectData.isEmpty())
            {
                parentObject = objectData[0];
            }
        }
    }
    
    /* Constructor for attachment object. Either gets the current attachment in scope or creates a new one*/
    public Attachment attachment {
    get {
        if (attachment == null)
            attachment = new attachment();
        return attachment;
        }
        set;
    }
    
    /*
    Handles the actual uploading of the file. Uses the current user id as the owner and the parent object
    ID as the record to attach the file to. Modifies the public attachment object (to include it's ID after its uploaded)
    and returns any errors to the page.
    @return null
    */
    public PageReference upload() 
    {
    
        attachment.OwnerId = UserInfo.getUserId();
        attachment.parentId = parentRecordId; // put it in running user's folder
        attachment.ContentType = contentType;
        
        try
        {
            insert attachment;
        } 
        catch (DMLException e)
        {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading file'));
            return null;
        } 
        finally
        {
            attachment.body = null; // clears the viewstate
            attachment = new attachment();
        }
        
        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'File uploaded successfully'));
        return null;
    }
    
}