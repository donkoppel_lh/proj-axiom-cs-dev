@isTest
private class Axiom_TestUtilities
{
	static testMethod void testScheduleBatchJob()
	{
		Test.StartTest();

			Axiom_Utilities.scheduleBatchJobs();

		Test.StopTest();
	}

	static testMethod void testBuildSelectAllStatment()
	{
		Test.StartTest();

			Axiom_Utilities.BuildSelectAllStatment('Account');

		Test.StopTest();
	}

	static testMethod void testCancelBatchJob()
	{
		Test.StartTest();

		Axiom_Utilities.cancelBatchJobs();

		Test.StopTest();
	}

	static testMethod void testCountNumberOfAttachments()
	{
		Axiom_Utilities.generateTestData();
		Test.StartTest();
 
			Map<ID, Contact> m = new Map<ID, Contact>([SELECT Id, LastName FROM Contact]);

			Attachment attachment = new Attachment();
			attachment.Body = Blob.valueOf('Body of Attachment');
			attachment.Name = String.valueOf('test.txt');
			attachment.ParentId = new list<id>(m.keySet())[0]; 
			insert attachment;

			Axiom_Utilities.countNumberOfAttachments(new list<id>(m.keySet()));

		Test.StopTest();
	}

	static testMethod void testGetCompletedTaskPercent()
	{
		Axiom_Utilities.generateTestData();

		Test.StartTest();

			list<matter__c> allMatters = [select id from matter__c];
			Axiom_Utilities.getCompletedTaskPercent(allMatters[0].Id); 

		Test.StopTest();
	} 

	static testMethod void testCalculateBusinessMinutes()
	{
		Test.StartTest();

			Axiom_Utilities.calculateBusinessMinutesBetweenDates(dateTime.now(), dateTime.now().addHours(5));

		Test.StopTest();
	}

	static testMethod void testCalculateBusinessDays()
	{
		Test.StartTest();

			Axiom_Utilities.calculateBusinessDaysBetweenDates(dateTime.now(), dateTime.now().addHours(1024));

		Test.StopTest();
	}

	static testMethod void testPrepopulateObject()
	{
		Axiom_Utilities.generateTestData();
		Test.StartTest();

			map<id,sobject> parentToChildIds = new map<id,sobject>();
			Request__c parentRequest = (Request__c) database.query(Axiom_Utilities.BuildSelectAllStatment('Request__c') + ' limit 1');
			Matter__c childMatters = (Matter__c) database.query(Axiom_Utilities.BuildSelectAllStatment('Matter__c') + ' limit 1');

			parentToChildIds.put(parentRequest.id,childMatters);

			Axiom_Utilities.prepopulateObject(parentToChildIds, new set<string>());

		Test.StopTest();
	}

	static testMethod void testRunBatchJobsImmediate()
	{
		Test.StartTest();

			Axiom_Utilities.runBatchJobsImmediate();

		Test.StopTest();
	}

	static testMethod void testParseCSV()
	{
		Test.StartTest();

			string csvContent = 'firstname,lastname\nTest,Guy';
			List<List<String>> parseResult = Axiom_Utilities.parseCSV(csvContent,false);

		Test.StopTest(); 
	}

	static testMethod void testDeserializeString()
	{
		Test.StartTest();
			
			string testParms = 'testparm1=x&testparm2=y';
			map<string,string> mapResult = Axiom_Utilities.deserializeString(testParms);

		Test.StopTest(); 
	}

	@isTest(seeAllData=true)
	static void testDocumentImport()
	{
		Test.StartTest();

			string documentData = 'Active__c,Default_Value__c,Display_Order__c,Input_Label_Override__c,Input_Type_Override__c,Input_Validation_Classes__c,Matter_Field_Name__c,Matter_Record_Type__c,Name,Required__c';
			documentData += '\nTRUE,www.bt.com,52,Please complete the checklist linked below and attach to this request,,,BT_Checklist__c,"Other - Product, Portfolio and Standards;Review, analyse or revise documents and agreements;","Product, Portfolio-BT Checklist",FALSE';
			documentData += '\nTRUE,,12,Name of Non-BT Party,,,Counterparty__c,Order Forms (AMEA);,Order Forms-Counterparty,TRUE';

			list<Folder> importDocFolder = [select id from folder where name  = 'iris import files' and type = 'Document'];
			folder docFolder;

			if(importDocFolder.isEmpty())
				system.debug('\n\n\n\n------------------ ERROR TESTING IMPORT FILE. NO FOLDER NAMED "iris import files" FOUND. PLEASE CREATE AND RETRY');                
			else
				docFolder = importDocFolder[0];

			Document thisImportDoc = new Document();
			thisImportDoc.name = 'Test Import Doc';
			thisImportDoc.body = blob.valueOf(documentData);
			thisImportDoc.type = 'csv';
			thisImportDoc.description = 'Client_Portal_Matter_Fields__c';
			thisImportDoc.folderId = docFolder.id;

			insert thisImportDoc;

			//test the import documents feature
			map<string,list<database.saveResult>> importResult = Axiom_Utilities.importCustomSettingFiles(false, true, true);

		Test.StopTest();
	}

	static testMethod void testUrlEncodeObject()
	{
		Axiom_Utilities.generateTestData();
		Test.StartTest();

			Request__c parentRequest = (Request__c) database.query(Axiom_Utilities.BuildSelectAllStatment('Request__c') + ' limit 1');
			string  encodeResult = Axiom_Utilities.urlEncodeObject(parentRequest);

		Test.StopTest(); 
	}
/*
	static testMethod void testGrantGMsSharingToRequestAndMatter()
	{
		Account testAccount = TestingUtils.createAccounts( 1, 'Test Legal Entity', true )[0];

		User existingSingaporeGMUser = TestingUtils.createUsers( 1, 'Axiom Assignor', null, false )[0];
		User singaporeGMUser = TestingUtils.createUsers( 1, 'Axiom Assignor', null, false )[0];
		User existingSeoulGMUser = TestingUtils.createUsers( 1, 'Axiom Assignor', null, false )[0];
		User seoulGMUser = TestingUtils.createUsers( 1, 'Axiom Assignor', null, false )[0];
		User existingRestOfWorldGMUser = TestingUtils.createUsers( 1, 'Axiom Assignor', null, false )[0];
		User restOfWorldGMUser = TestingUtils.createUsers( 1, 'Axiom Assignor', null, false )[0];
		List<User> testUsers = new List<User>{ existingSingaporeGMUser, singaporeGMUser, existingSeoulGMUser, seoulGMUser, existingRestOfWorldGMUser, restOfWorldGMUser };
		insert testUsers;

		Group singaporeSecrecyGroup = [ SELECT Id FROM Group WHERE DeveloperName = 'Singapore_Secrecy_Client_Onboarding' ];
		GroupMember testSingaporeGroupMemberOne = TestingUtils.createGroupMembers( 1, singaporeSecrecyGroup.Id, existingSingaporeGMUser.Id, false )[0];
		GroupMember testSingaporeGroupMemberTwo = TestingUtils.createGroupMembers( 1, singaporeSecrecyGroup.Id, singaporeGMUser.Id, false )[0];

		Group seoulSecrecyGroup = [ SELECT Id FROM Group WHERE DeveloperName = 'Seoul_Secrecy_Client_Onboarding' ];
		GroupMember testSeoulGroupMemberOne = TestingUtils.createGroupMembers( 1, seoulSecrecyGroup.Id, existingSeoulGMUser.Id, false )[0];
		GroupMember testSeoulGroupMemberTwo = TestingUtils.createGroupMembers( 1, seoulSecrecyGroup.Id, seoulGMUser.Id, false )[0];
		insert new List<GroupMember>{ testSingaporeGroupMemberOne, testSingaporeGroupMemberTwo, testSeoulGroupMemberOne, testSeoulGroupMemberTwo };

		Legal_Entity__c testSeoulLegalEntity = TestingUtils.createLegalEntities( 1, 'SeoulLegalEntity', testAccount.Id, 'Seoul Secrecy Group', true )[0];
		Request__c seoulSecrecyRequest = TestingUtils.createRequests( 1, 'Seoul Request', true )[0];
		Matter__c seoulSecrecyMatter = TestingUtils.createMatters( 1, 'Seoul Matter', seoulSecrecyRequest.Id, true )[0];
		Legal_Entity_Role__c testSeoulLER = TestingUtils.createLegalEntities( 1, seoulSecrecyRequest.Id, testSeoulLegalEntity.Id, true )[0];
		Request_Entitlement__c testSeoulRE = TestingUtils.createRequestEntitlements( 1, seoulSecrecyRequest.Id, existingSeoulGMUser.Id, 'Revoke', true )[0];

		Test.startTest();

			Request__c testRequest = [ SELECT Id, Secrecy_Group__c, Sharing_Method__c FROM Request__c WHERE Id = :seoulSecrecyRequest.Id ];
			Axiom_Utilities.grantGMsSharingToRequestAndMatter( new List<Request__c>{ testRequest }, null );
			List<Request__Share> actualRequestShares = [ SELECT Id FROM Request__Share WHERE UserOrGroupId IN :testUsers ];
			System.assertEquals( 1, actualRequestShares.size(), 'All Users in Seoul Secrecy Group except the ones in Request Entitlement Revoked list should have access to the Request' );

			List<Matter__Share> actualMatterShares = [ SELECT Id FROM Matter__Share WHERE UserOrGroupId IN :testUsers ];
			System.assertEquals( 1, actualMatterShares.size(), 'All Users in Seoul Secrecy Group except the ones in Request Entitlement Revoked list should have access to the Matter' );

			//Grant access for new user
			Request_Entitlement__c testGrantRE = TestingUtils.createRequestEntitlements( 1, seoulSecrecyRequest.Id, existingRestOfWorldGMUser.Id, 'Grant', true )[0];
			testRequest = [ SELECT Id, Secrecy_Group__c, Sharing_Method__c FROM Request__c WHERE Id = :seoulSecrecyRequest.Id ];
			Axiom_Utilities.grantGMsSharingToRequestAndMatter( new List<Request__c>{ testRequest }, null );
			actualRequestShares = [ SELECT Id FROM Request__Share WHERE UserOrGroupId IN :testUsers ];
			System.assertEquals( 2, actualRequestShares.size(), 'The User with Grant Access should be added to the list to view the Request' );

			actualMatterShares = [ SELECT Id FROM Matter__Share WHERE UserOrGroupId IN :testUsers ];
			System.assertEquals( 2, actualMatterShares.size(), 'The User with Grant Access should be added to the list to view the Matter' );

			//Change secrecy group to NONE
			Legal_Entity__c testSingaporeLegalEntity = TestingUtils.createLegalEntities( 1, 'SingaporeLegalEntity', testAccount.Id, 'Singapore Secrecy Group', true )[0];
			Legal_Entity_Role__c testSingaporeLER = TestingUtils.createLegalEntities( 1, seoulSecrecyRequest.Id, testSingaporeLegalEntity.Id, true )[0];

			testRequest = [ SELECT Id, Secrecy_Group__c, Sharing_Method__c FROM Request__c WHERE Id = :seoulSecrecyRequest.Id ];
			Axiom_Utilities.grantGMsSharingToRequestAndMatter( new List<Request__c>{ testRequest }, null );
			actualRequestShares = [ SELECT Id FROM Request__Share WHERE UserOrGroupId IN :testUsers ];
			System.assertEquals( 5, actualRequestShares.size(), 'After changing the Secrecy Group to null, only Users who should have access to the Request should be the ones not in Request Entitlement Revoked list' );

			actualMatterShares = [ SELECT Id FROM Matter__Share WHERE UserOrGroupId IN :testUsers ];
			System.assertEquals( 5, actualMatterShares.size(), 'After changing the Secrecy Group to null, only Users who should have access to the Matter should be the ones not in Request Entitlement Revoked list' );

			//Change secrecy group to Singapore
			delete testSeoulLER;

			testRequest = [ SELECT Id, Secrecy_Group__c, Sharing_Method__c FROM Request__c WHERE Id = :seoulSecrecyRequest.Id ];
			Axiom_Utilities.grantGMsSharingToRequestAndMatter( new List<Request__c>{ testRequest }, null );
			actualRequestShares = [ SELECT Id FROM Request__Share WHERE UserOrGroupId IN :testUsers ];
			System.assertEquals( 3, actualRequestShares.size(), 'After changing the Secrecy Group to Singapore, all Users in Singapore Secrecy Group and Users with Grant Access should have access to the Request' );

			actualMatterShares = [ SELECT Id FROM Matter__Share WHERE UserOrGroupId IN :testUsers ];
			System.assertEquals( 3, actualMatterShares.size(), 'After changing the Secrecy Group to Singapore, all Users in Singapore Secrecy Group and Users with Grant Access should have access to the Matter' );

		Test.stopTest();
	}

	static testMethod void testGrantOrRevokeAccessToRequest_REVOKE()
	{
		Account testAccount = TestingUtils.createAccounts( 1, 'Test Legal Entity', true )[0];

		List<User> testAxiomAssignorUsers = TestingUtils.createUsers( 3, 'Axiom Assignor', null, true );

		Group seoulSecrecyGroup = [ SELECT Id FROM Group WHERE DeveloperName = 'Seoul_Secrecy_Client_Onboarding' ];
		List<GroupMember> testGroupMembers = new List<GroupMember>();
		for( User aUser : testAxiomAssignorUsers )
		{
			testGroupMembers.add( TestingUtils.createGroupMembers( 1, seoulSecrecyGroup.Id, aUser.Id, false )[0] );
		}
		insert testGroupMembers;

		Request__c testRequest = TestingUtils.createRequests( 1, 'Test Request', true )[0];
		Matter__c testMatter = TestingUtils.createMatters( 1, 'Test Matter', testRequest.Id, true )[0];
		Legal_Entity__c testLegalEntity = TestingUtils.createLegalEntities( 1, 'SeoulLegalEntity', testAccount.Id, 'Seoul Secrecy Group', true )[0];
		Legal_Entity_Role__c testLegalEntityRole = TestingUtils.createLegalEntities( 1, testRequest.Id, testLegalEntity.Id, true )[0];

		Test.startTest();

			//Revoke access
			Axiom_Utilities.grantOrRevokeAccessToRequest( 'Revoke', testRequest.Id, new Set<Id>{ testAxiomAssignorUsers[0].Id } );
			List<Request_Entitlement__c> actualRequestEntitlements = [ SELECT Id FROM Request_Entitlement__c WHERE Request__c = :testRequest.Id ];
			System.assertEquals( 1, actualRequestEntitlements.size(), 'A Request Entitlement should be created' );

			List<Request__Share> actualRequestShares = [ SELECT Id FROM Request__Share WHERE UserOrGroupId IN :testAxiomAssignorUsers ];
			System.assertEquals( 2, actualRequestShares.size(), 'Request shares should be created for the Users that are not revoked' );

			List<Matter__Share> actualMatterShares = [ SELECT Id FROM Matter__Share WHERE UserOrGroupId IN :testAxiomAssignorUsers ];
			System.assertEquals( 2, actualMatterShares.size(), 'Matter shares should be created for the Users that are not revoked' );

			//Grant access
			User testUser = TestingUtils.createUsers( 1, 'Axiom Assignor', null, true )[0];
			Axiom_Utilities.grantOrRevokeAccessToRequest( 'Grant', testRequest.Id, new Set<Id>{ testUser.Id } );
			actualRequestEntitlements = [ SELECT Id FROM Request_Entitlement__c WHERE Request__c = :testRequest.Id ];
			System.assertEquals( 2, actualRequestEntitlements.size(), 'Additional Request Entitlement should be added' );

			actualRequestShares = [ SELECT Id FROM Request__Share WHERE UserOrGroupId IN :testAxiomAssignorUsers OR UserOrGroupId = :testUser.Id ];
			System.assertEquals( 3, actualRequestShares.size(), 'Additional Request shares should be created' );

			actualMatterShares = [ SELECT Id FROM Matter__Share WHERE UserOrGroupId IN :testAxiomAssignorUsers OR UserOrGroupId = :testUser.Id ];
			System.assertEquals( 3, actualMatterShares.size(), 'Additional Matter shares should be created' );

			//Revoke access with existing request entitlement
			Axiom_Utilities.grantOrRevokeAccessToRequest( 'Revoke', testRequest.Id, new Set<Id>{ testAxiomAssignorUsers[1].Id } );
			actualRequestEntitlements = [ SELECT Id FROM Request_Entitlement__c WHERE Request__c = :testRequest.Id ];
			System.assertEquals( 3, actualRequestEntitlements.size(), 'Additional Request Entitlement should be added' );

			actualRequestShares = [ SELECT Id FROM Request__Share WHERE UserOrGroupId IN :testAxiomAssignorUsers OR UserOrGroupId = :testUser.Id ];
			System.assertEquals( 2, actualRequestShares.size(), 'Revoked User should no longer have accessibility to the Request' );

			actualMatterShares = [ SELECT Id FROM Matter__Share WHERE UserOrGroupId IN :testAxiomAssignorUsers OR UserOrGroupId = :testUser.Id ];
			System.assertEquals( 2, actualMatterShares.size(), 'Revoked User should no longer have accessibility to the Matter' );

		Test.stopTest();
	}
*/
}