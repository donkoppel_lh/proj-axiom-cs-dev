@isTest
global class Axiom_TestUpdateMattersBatch
{
    public static testMethod void testUpdateMattersBatch()
    {
        Axiom_Utilities.generateTestData();
        Matter__c testMatter = [select status__c from matter__c limit 1];
        testMatter.status__c = 'In Progress';
        testMatter.First_Substantive_Response__c = null;
        testMatter.Stop_Time__c = null;
        update testMatter;
        
        Test.startTest();
        list<Matter__c> UnneededMatters = [select id from matter__c where id != :testMatter.id];
        delete UnneededMatters;
        
        String CRON_EXP = '0 0 0 15 3 ? 2022';
        
        String jobId = System.schedule('ScheduleApexClassTest',
                        CRON_EXP, 
                        new Axiom_UpdateMattersBatch());        
        Test.stopTest();
 
    }
}