@isTest
global class Axiom_TestVersionController
{
    @isTest
    public static void testVersions()
    {
        AxiomSectionFields__c section1 = new AxiomSectionFields__c();
        section1.name = 'section1Name';
        section1.Field_API_Name__c = 'name';
        section1.Object_Name__c = 'version__c';
        section1.Section_Number__c = 1;
        insert section1;
        
        Axiom_Utilities.generateTestData();
        
        Version__c thisVersion = [select id, name from version__c limit 1];
        thisVersion.name = 'My Version'; 
        update thisVersion;
        
        Test.StartTest();
        
        
        ApexPages.StandardController sc = new ApexPages.StandardController(thisVersion);
             
        PageReference pageRef = Page.Axiom_VersionProgress;
        Test.setCurrentPage(pageRef);
        Axiom_VersionController controller = new Axiom_VersionController(sc);
        system.debug(controller.completedTaskPercent);

        map<string,string> subjectToRequestMap = Axiom_TaskController.getTaskSubjectToRequestFieldMap();
        

        
        //create a new task for each of the trigger status values
        list<task> tasksToCreate = new list<task>();
        for(string thisStatus : subjectToRequestMap.keySet())
        {
            task thisTask = new task();
            thisTask.whatId = thisVersion.id;
            thisTask.subject = thisStatus;
            thisTask.ownerId = userInfo.getUserId();
            thisTask.status = 'open';
            thisTask.priority = 'normal';
            thisTask.Apex_Context__c = true;
            thisTask.section__c = 1;
            tasksToCreate.add(thisTask);
        }     
        
        insert tasksToCreate;
        
        tasksToCreate[0].status = 'Completed';
        update tasksToCreate[0];
        
        Test.StopTest();
    }
}