global class ActivityPlan_TaskTriggerHandler
{
    //A task created by activity plans can have field update rules attached. Those are simply conditions that when met
    //will cause their parent object to update. They take the form of
    //When field [x] on the task is [logical condition] then set field [y] on the tasks parent object to value [z]
    //so this trigger handles that. It takes a list of tasks that were updated, checks to see what if any rules are defined for the template
    //that created that task. Then if the task matches the conditions for any of those rules, the associated update of the parent object is performed.
    //currently each update rule only supports one logical condition.
    public static list<sObject> handleFieldUpdates(map<id,task> tasks, map<id,task> oldTasks)
    {
        ActivityPlans_Utilities utils = new ActivityPlans_Utilities();
        
        system.debug('\n\n\n\n------------------------------------------------------- BEGINNING FIELD UPDATE RULE PROCESSING!');
        
        set<id> taskTemplateIds = new set<id>();
        list<sobject> updateObjects = new list<sobject>();
        oldTasks = oldTasks != null ? oldTasks : new map<id,task>();
        
        //get the template ids of all the tasks
        for(Task thisTask : Tasks.values())
        {
            taskTemplateIds.add(thisTask.Created_by_Template__c);
        }
        
        //find the template and rule info for the tasks that were created by activity plans.
        map<id,Activity_Plan_Task_Template__c> updateRules = new Map<ID, Activity_Plan_Task_Template__c>([select
                                                             Name,
                                                             Activity_Plan__c,
                                                             Assigned_To_Type__c,
                                                             Assigned_To_Value__c,
                                                             Due_Date_Type__c,
                                                             Due_Date_Value__c,
                                                             Priority__c,
                                                             Related_To_Type__c,
                                                             Related_To_Value__c,
                                                             Task_Status__c,
                                                             Record_Type_Name__c,
                                                             Allow_Duplicate_Tasks__c,
                                                             Depends_On__c,
                                                             Depends_On_Completion_Statuses__c,
                                                             Depends_On__r.Name,
                                                             Description__c,
                                                             Notification_Email_Template__c,
                                                             Task_Weight__c,
                                                             Due_Date_Offset_Source__c,
                                                             Order__c,
                                                             Substantive_Response__c,
                                                             (select Activity_Plan__c,
                                                                 Comparison_Value__c,
                                                                 Comparison_Field_Name__c,
                                                                 Comparison_Type__c,
                                                                 Activity_Plan_Task_Template__c,                                                        
                                                                 Logical_Operator__c,
                                                                 Assignment_Field_Name__c,
                                                                 Assignment_Type__c,
                                                                 Assignment_Value__c,
                                                                 Trigger__c,
                                                                 Active__c
                                                              from Activity_Plan_Field_Updates__r
                                                              where Active__c = true)
                                                             from Activity_Plan_Task_Template__c   
                                                             Where id in :taskTemplateIds
                                                         ]);
        
        //iterate over each task
        for(Task thisTask :tasks.values())
        {

                            
            system.debug('\n\n\n\n-------------------------------------------- EVALUATING RULES ATTACHED TO THIS TASK!');
            system.debug(thisTask);
            
            //if this task wasn't created by a template, or the template no longer exists, continue (skip this iteration) of the loop
            if(thisTask.Created_by_Template__c == null || !updateRules.containsKey(thisTask.Created_by_Template__c))
            {
                system.debug('\n\n\n\n-------------------------------------------- TASK WAS NOT CREATED BY ACTIVITY PLANS. SKIPPING!!');
                continue;
            }
            Task oldTask = oldTasks.containsKey(thisTask.Id) ? oldTasks.get(thisTask.Id) : null;
             
            //get the template details by looking it up in the map we queried for above
            Activity_Plan_Task_Template__c thisTemplate = updateRules.get(thisTask.Created_by_Template__c);
            
            //create an instance of the parent object by creating a new sObject of the proper type using the whatId from the task.
            sObject parentObject = thisTask.whatId.getSObjectType().newSobject(thisTask.whatId);

                        
            boolean hasUpdates = false;
            //loop over all the rules 
            for(Activity_Plan_Field_Update__c thisRule : thisTemplate.Activity_Plan_Field_Updates__r)
            {
                system.debug('\n\n\n\n-------------------------------------------- FOUND AN ACTIVE FIELD UPDATE RULE TO EVALUATE!');
                system.debug(thisRule);
                
                //deduce what the value is we are comparing against.   
                object compareVal;
                
                if(thisRule.Comparison_Type__c == 'Static Value' || thisRule.Comparison_Type__c == '')
                {
                    compareVal = thisRule.Comparison_Value__c;
                    
                    //special case handler for if the user wants to compare against null. Since you cannot leave the compareVal field blank
                    //users can enter the word null to do an actual null comparison.
                    if(string.valueOf(compareVal).toLowerCase() == 'null')
                    {
                        system.debug('\n\n\n\n-----------------------COMPARISON VALUE IS NULL. SETTING TO EMPTY STRING!');
                        compareVal = null;
                    }    
                    
                    //special case handler for user wants to compare against blank. Since you cannot leave the compareVal field blank, users
                    //can enter a pair of single quotes to do a compare against a blank field value.
                    if(compareVal == '\'\'')
                    {
                        system.debug('\n\n\n\n-------------------------COMPARISON VALUE IS EMPTY STRING. SETTING TO EMPTY STRING!');
                        compareVal = '';
                    }
                }     
                else if(thisRule.Comparison_Type__c == 'Object Field Value')
                {
                    compareVal = thisTask.get(thisRule.Comparison_Value__c);
                }        
                
                system.debug('\n\n\n\n----------------------- COMPARE FIELD: ' + thisRule.Comparison_Field_Name__c);
                system.debug('\n\n\n\n----------------------- COMPARE FIELD VALUE: ' + thisTask.get(thisRule.Comparison_Field_Name__c));
                system.debug('\n\n\n\n----------------------- LOGICAL: ' + thisRule.Logical_Operator__c);
                system.debug('\n\n\n\n----------------------- COMPARE VAL: ' + compareVal);
                system.debug('\n\n\n\n----------------------- DYNAMIC IF RESULT: ' + ActivityPlansController.dynamicIf(thisTask.get(thisRule.Comparison_Field_Name__c),  thisRule.Logical_Operator__c, compareVal));
                system.debug('\n\n\n\n----------------------- OLD TASK: ' + oldTask);
                system.debug('\n\n\n\n----------------------- TRIGGER: ' + thisRule.Trigger__c);
                
                //use the dynamic if engine to figure out if our logical condition is true
                if(ActivityPlansController.dynamicIf(thisTask.get(thisRule.Comparison_Field_Name__c), 
                                                     thisRule.Logical_Operator__c,
                                                     compareVal)                                                     
                                                     &&       
                   //if we don't have an old task to compare a value against
                   //or we want to run this update every time the rule is true
                   //or if the previous value did not pass the test and the current value does then run the update                                                                              
                   (oldTask == null || thisRule.Trigger__c == 'Every time a record is updated' ||
                   (thisRule.Trigger__c == 'Only when the criteria was not met previously' &&
                   !ActivityPlansController.dynamicIf(oldTask.get(thisRule.Comparison_Field_Name__c), 
                                                     thisRule.Logical_Operator__c,
                                                     compareVal))))
                {
                    object value;
                    
                    system.debug('\n\n\n\n----------------------------- Assignment Type: ' + thisRule.Assignment_Type__c);
                    //figure out what value we are actually going to write to the parent object.
                    if(thisRule.Assignment_Type__c== 'Object Field Value')
                    {
                        value = thisTask.get((string) thisRule.get('Assignment_Field_Name__c'));
                    }
                    else if(thisRule.Assignment_Type__c== 'Static Value')
                    {
                       value = thisRule.get('Assignment_Value__c');
                    }
                    
                    //assign the value to the parent object
                    
                    system.debug('\n\n\n\n------------------------------------ RULE PASSED! ADDING VALUE TO PARENT OBJECT! Value of '+  thisRule.get('Assignment_Field_Name__c') + ' is ' + value );

                    //Since we don't know exactly what kind of field that we are updating is, we have a method that will deduce the fields type and attempt to
                    //cast the value to the correct type and write it to the parent object. Really makes me wish apex had some kind of dynamic casting.
                    
                    sObjectField assignField = thisTask.whatId.getSObjectType().getDescribe().fields.getMap().get(thisRule.Assignment_Field_Name__c);
                    parentObject = utils.putValueWithDynamicCasting(parentObject, (string) thisRule.get('Assignment_Field_Name__c'), assignField , value);
                                    
                    system.debug(parentObject);
                    hasUpdates = true;
                }
            }
            
            if(hasUpdates)
            {
                //add this object to the update list (man I really hope mixed DML works as well as they say it does)
                updateObjects.add(parentObject);        
            }
        }  
        
        if(!updateObjects.isEmpty())
        {
            update updateObjects;
        }
        return updateObjects;
    }
    
    public static void recheckActivityPlans(list<task> tasks)
    {
        system.debug('\n\n\n\n-------------------------------- RECHECKING ACTIVITY PLANS TO SEE IF ANY NEW TASKS NEED TO BE CREATED!');
        set<id> recordIds = new set<id>();
        for(task thisTask : tasks)
        {
            if(thisTask.Created_By_Template__c != null)
            {
                recordIds.add(thisTask.whatId);
            }
        }
        
        map<string,set<id>> objectsToCheck = new map<string,set<id>>();
        
        for(id recordId : recordIds)
        {
            string objectType = string.valueOf(recordId.getSObjectType());
            
            set<id> objects = objectsToCheck.containsKey(objectType) ? objectsToCheck.get(objectType) : new set<Id>();
            objects.add(recordId);
            objectsToCheck.put(objectType,objects);
        }
        
        system.debug('\n\n\n\n\n--------------------------------------  Sobject map!');
        system.debug(objectsToCheck);
        
        for(string objectType : objectsToCheck.keySet())
        {
            set<id> relatedObjectIds = objectsToCheck.get(objectType);
            
            string queryString =  ActivityPlans_Utilities.buildSelectAllStatment(objectType);
            queryString += ' where id in :relatedObjectIds';
            list<sObject> objectsWithData = database.query(queryString);
            
            ActivityPlansController.findMatchingActivityPlans(objectsWithData);
        }
    }
    public class applicationException extends Exception {}
}