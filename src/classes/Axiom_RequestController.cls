/******* Axiom Request Controller *********
Author Daniel Llewellyn/RedKite (dllewellyn@redkitetechnologies.com)
Date ~9/2013
Description Methods and properties related to the salesforce Request object.
**********************************************/

global class Axiom_RequestController
{
    //variable that tracks the related triggers amount of invocations to order to stop recurisve calls that cause governor limit errors
    public static integer invokeCount = 0;
    public static set<string> orgEntities = new set<string>{'Credit Suisse Entity', 'Client Entity'};
    public static set<string> externalEntities = new set<string> {'Counterparty'};
    /**
    * Uses business hour logic to calculate the number of days that passed to the first substantive response
    * for a request. Invoked via batch process Axiom_UpdateRequestBatch. Requests passed in must include createdDate and stop_time__c field. If stop time is provided it is used,
    * otherwise uses current date time. Does not perform actual update as it is meant to be invoked by the Axiom_UpdateRequestBatch
    * class.
    * @param requests list of requests to calculate days open for.
    * @return list of requests with days_open__c field calculated and ready to be updated.
    */
     public static list<Request__c> setNumberOfDaysToFsr(list<Request__c> requests)
     {
        for(Request__c thisRequest : requests)
        {
            datetime startTime = thisRequest.createdDate != null ? thisRequest.createdDate : dateTime.now();
            datetime stopTime = thisRequest.stop_time__c != null ? thisRequest.stop_time__c : dateTime.now();

            thisRequest.Days_Open__c = Axiom_Utilities.calculateBusinessDaysBetweenDates(startTime, stopTime);
        }
        return requests;
    }
    
    
    webservice static list<Matter__c> acceptRequest(id requestId, boolean multiEntityTemplate)
    {
        list<Matter__c> createdMatters = new list<Matter__c>();
        
        Request__c thisRequest = [select Id, Start_Time__c, Owner.Id from Request__c where Id = :requestId];
        boolean isAccepted = thisRequest.Start_Time__c != null ? true : false;
        boolean currentUserIsOwner = thisRequest.Owner.Id == UserInfo.getUserId() ? true : false;
        boolean permissionEnabled = true;
        Axiom_Utilities.remoteObject entitlmentResult = Axiom_Utilities.getUserEntitlement(UserInfo.getUserId(), 'Accept Request');
        //permissionEnabled = AxiomUtilities.isUserEntitlesForFunction(UserInfo.getUserId(), 'Accept Request');
        list<Legal_Entity_Role__c> legalEntityRoles = [select id from Legal_Entity_Role__c where Request_Name__c = :thisRequest.Id and Role__c = 'Credit Suisse Entity'];
        
        if(isAccepted)
        {
           throw new customException('Request Has Already Been Accepted');
        }
        if(!currentUserIsOwner)
        {
            throw new customException('Only the owner of a request may accept it');
        }
        if(!permissionEnabled)
        {
            throw new customException(entitlmentResult.message);
        }
        if(legalEntityRoles.isEmpty())
        {
            throw new customException('There are no legal entity roles defined for this request. Please define at least legal entity role and try again');
        }
        
        
        thisRequest.Start_Time__c = dateTime.Now();
        update thisRequest;
        
        list<Id> requestIds = new list<Id>();
        requestIds.add(requestId);
        createdMatters = createMattersFromRequest(requestIds, multiEntityTemplate);
        return createdMatters ;   
    }
    /**
    * When a request is accepeted it's starttime__c field is populated. When this happens all related matters need to have their owner changed to the owner of the request record
    * @param requests a list of requests whos matters need to get updated
    * @return list of matters that were updated
    **/
    public static list<Matter__c> changeMatterOwnersToRequestOwner(map<id,Request__c> oldMap, map<id,Request__c> newMap)
    {
        set<id> requestIds = new set<id>();
        for(Request__c thisRequest : newMap.values())
        {
            if(thisRequest.Owner.Type != 'queue')
            {   
                //we only want to update matter owners if the start time on the request was just changed. This means that if the request was inserted with a start time we need to
                //account for that, or if the request was just updated to have a start time, we account for that as well. So one if conidition to see if this is an insert with a start time
                //set by checking for existance of this record in the oldmap, if no entry found and the request has a start time, process it. If the oldmap DOES have an entry for this
                //request and the new value isn't the same as the old value, and it also isn't null then process it.
                if( (!oldMap.containsKey(thisRequest.id) && thisRequest.Start_Time__c != null) || 
                    (oldMap.containsKey(thisRequest.id) && oldMap.get(thisRequest.id).Start_Time__c != thisRequest.Start_Time__c && thisRequest.Start_Time__c != null))
                { 
                    requestIds.add(thisRequest.id);
                }
            }
        }
        
        list<matter__c> updateMatters = [select id, Request__r.ownerId from matter__c where request__c in :requestIds];
        
        for(Matter__c thisMatter : updateMatters)
        {
            thisMatter.ownerId = thisMatter.Request__r.OwnerId;
        }
        
        database.update(updateMatters,false);
        
        return updateMatters;
    }
    
    /**
    * When a user clicks a button on a request, matters must be generated. A request has a related object called an agreement title.
    * The basic math for this can be found below. The equation is PER request.
    * L = Number of Request Agreement Titles Related to the request
    * C = Number of Counter Parties (Number of Legal Entity Roles with Role__c of 'CounterParty')
    * S = Number of CS entities (Number of Legal Entity Roles with Role__c of 'Client Entity' or Role__c of 'Credit Suisse Entity')
    * M = Final Number of created matters
    * M = 1 + (L * C) + ( C * S * L)
    * @param requests, list of incoming request record ids. Because this can be called via button (and is hence is a webservice) it does not accept complex object types (ex: a list of sObjects/Requests)
    * @return a map of created matters keyed by their parent request id (cannot return map because of webservice limitations).
    **/
    webservice static list<Matter__c> createMattersFromRequest(list<Id> requestIds, boolean multipleEntityTemplate)
    {
        list<Matter__c> agreementMatters = new list<Matter__c>();
        list<Matter__c> dueDilligenceMatters = new list<Matter__c>();
        list<Matter__c> dataAdminMatters = new list<Matter__c>();
        list<Legal_Entity_Role__c> indirectPrincipalRoles = new list<Legal_Entity_Role__c>();
        list<Legal_Entity_Role__c> counterPartyRoles = new list<Legal_Entity_Role__c>();
        list<Legal_Entity_Role__c> nonCSEntityRoles = new list<Legal_Entity_Role__c>();
        list<Legal_Entity_Role__c> csEntityRoles = new list<Legal_Entity_Role__c>();
        
        //find the queue to assign the new matters to
        id queueId = null;
        list<Group > mattersQueue = [select Id from Group where name like  '%matter%' and Type = 'Queue'];
        if(!mattersQueue.isEmpty())
        {
            queueId = mattersQueue[0].Id;
        }
        
        list<Request__c> updateRequests = new list<Request__c>();
        
        //find all the valid matter types and construct a map of them
        list<RecordType> matterTypes = [select id, name, developerName from RecordType where sObjectType = 'Matter__c'];
        Map<string,RecordType> recordTypeMap = new Map<string,RecordType>();
        for(RecordType thisType : matterTypes)
        {
            recordTypeMap.put(thisType.developerName,thisType);
        }
        
        list<Matter__c> currentMatters = [select id from matter__c where request__c in :requestIds];
        delete currentMatters;
                
        list<Request__c> parentRequests = [Select Id, 
                                                  Name,
                                                  OwnerId,
                                                  Date_Requested__c,
                                                  Framesoft_Created_Date__c,
                                                  Counterparty_Legal_Id__c,
                                                  (Select Id, 
                                                          Name,
                                                          Description__c,
                                                          Agreement_Title__r.Master_Agreement_Type__c,
                                                          Agreement_Title__c,
                                                          Agreement_Title__r.Name,
                                                          CNID__c,
                                                          Agreement_Title__r.Class__c,
                                                          Master_Agreement_Type__c,
                                                          Request__r.Name
                                                   From Request_Agreement_Titles__r),
                                                  (Select Id,
                                                          Name,
                                                          Role__c,
                                                          Legal_Entity_Name__r.Country_of_Incorporation__c,
                                                          Legal_Entity_Name__r.Name,
                                                          Legal_Entity_Name__r.Legal_Entity_Name__c,
                                                          Legal_Entity_Name__r.CSID__c,
                                                          Legal_Id__c
                                                   From Legal_Entity_Roles__r)        
                                           From Request__c
                                           Where id in :requestIds];

        
        //iterate over each request
        for(Request__c thisRequest : parentRequests)
        {

            set<string> creditSuissEntities = new set<string>();
            set<string> counterPartyEntities = new set<string>();
            set<string> indirectPrincipalEntities = new set<string>();
            
            for(Legal_Entity_Role__c role : thisRequest.Legal_Entity_Roles__r)
            {
                if(role.Role__c == 'Indirect Principal')
                {
                    indirectPrincipalRoles.add(role);
                    indirectPrincipalEntities.add(role.Legal_Entity_Name__r.Legal_Entity_Name__c); 
                }
                if(orgEntities.contains(role.Role__c) )
                {
                    csEntityRoles.add(role);
                    creditSuissEntities.add(role.Legal_Entity_Name__r.Name); 
                }
                else if(externalEntities.contains(role.Role__c) )
                {
                    counterPartyEntities.add(role.Legal_Entity_Name__r.Legal_Entity_Name__c); 
                    counterPartyRoles.add(role);
                }
            }

            nonCSEntityRoles.addAll(new list<Legal_Entity_Role__c>(indirectPrincipalRoles));
            nonCSEntityRoles.addAll(new list<Legal_Entity_Role__c>(counterPartyRoles) );
                                    
            updateRequests.add(new Request__c(id=thisRequest.id,Start_Time__c=dateTime.now()));
            //first off, each request needs a data admin matter. So create that now and add it to the list
            dataAdminMatters.add(initMatterFromRequest(thisRequest, null, null, multipleEntityTemplate, New Matter__c(Matter_Type__c='Data Admin') )); 
                        

            for(Request_Agreement_Title__c thisRAT : thisRequest.Request_Agreement_Titles__r)
            {
                if(!multipleEntityTemplate)
                {
                    agreementMatters.addAll(findLegalEntityPermutations(thisRequest.Legal_Entity_Roles__r, 0, thisRequest, thisRAT, multipleEntityTemplate));
                }
                else
                {
                     
                    for(Legal_Entity_Role__c role : thisRequest.Legal_Entity_Roles__r)
                    {
                        if(externalEntities.contains(role.Role__c))
                        {                 
                            
                            Matter__c newMatter = new Matter__c(Matter_Type__c = 'Agreement');                                                                                                         
     						newMatter.Credit_Suisse_Entity__c= Axiom_Utilities.getCSVFieldValues(csEntityRoles, 'Legal_Entity_Name__r.Legal_Entity_Name__c', true,255);
                            agreementMatters.add(initMatterFromRequest(thisRequest, role, thisRAT, multipleEntityTemplate, newMatter ));                
                        }  
                    }
                }
            }                
            //now loop over every legal entity on this request so we can create one matter for them per request agreement title          
            for(Legal_Entity_Role__c thisLER : thisRequest.Legal_Entity_Roles__r)
            {

                //we need to create one due dilligence matter per counter party. So if this Legal Entity Role has a role of 'counterparty' create a matter for them and add it to the list
                if(externalEntities.contains(thisLER.Role__c) || thisLER.Role__c == 'Indirect Principal')
                {
                    dueDilligenceMatters.add(initMatterFromRequest(thisRequest, thisLER, null, multipleEntityTemplate, New Matter__c(Matter_Type__c='Diligence'))); 
                    if(thisLER.Role__c == 'Indirect Principal')
                    {
                        indirectPrincipalRoles.add(thisLER);
                    }              
                }                        
            }
            
            //final modifications to data admin matters
            for(Matter__c thisMatter : dataAdminMatters)
            {
                thisMatter.recordTypeId = recordTypeMap.get('Data_Admin').id;
                set<string> counterPartyEntitiesWithIndirectPrincipals = new set<string>(counterPartyEntities);
                counterPartyEntitiesWithIndirectPrincipals.addAll(indirectPrincipalEntities);
                thisMatter.Counterparty__c = String.Join(new list<string>(counterPartyEntitiesWithIndirectPrincipals),',');
		        thisMatter.Counterparty__c = thisMatter.Counterparty__c.trim().right(1) == ',' ? thisMatter.Counterparty__c.left(thisMatter.Counterparty__c.trim().length()-1) : thisMatter.Counterparty__c;      
				thisMatter.Counterparty__c = thisMatter.Counterparty__c.length() > 255 ? thisMatter.Counterparty__c.substring(0,255) : thisMatter.Counterparty__c;

                thisMatter.Credit_Suisse_Entity__c= Axiom_Utilities.getCSVFieldValues(csEntityRoles, 'Legal_Entity_Name__r.Legal_Entity_Name__c', true,255);
            
                thisMatter.Description__c = Axiom_Utilities.getCSVFieldValues(thisRequest.Request_Agreement_Titles__r, 'Description__c', false,255);
                thisMatter.CSID__c = Axiom_Utilities.getCSVFieldValues(nonCSEntityRoles, 'Legal_Entity_Name__r.CSID__c', true,255);
                thisMatter.Counterparty_Legal_Id__c = Axiom_Utilities.getCSVFieldValues(nonCSEntityRoles, 'Legal_Id__c', true,255);
                thisMatter.Country_of_Incorporation_c__c = Axiom_Utilities.getCSVFieldValues(nonCSEntityRoles, 'Legal_Entity_Name__r.Country_of_Incorporation__c', true,255);
                thisMatter.Master_Agreement_Type__c = Axiom_Utilities.getCSVFieldValues(thisRequest.Request_Agreement_Titles__r, 'Master_Agreement_Type__c', true,255);
                thisMatter.Agreement_Title_Text__c = Axiom_Utilities.getCSVFieldValues(thisRequest.Request_Agreement_Titles__r, 'Agreement_Title__r.Name', false,255);
                thisMatter.CNID__c = Axiom_Utilities.getCSVFieldValues(thisRequest.Request_Agreement_Titles__r, 'CNID__c', false,255);
                thisMatter.Request_Type__c = Axiom_Utilities.getCSVFieldValues(thisRequest.Request_Agreement_Titles__r, 'Agreement_Title__r.Class__c', true,255);
                thisMatter.OwnerId = queueId != null ? queueId : thisRequest.ownerId;

                thisMatter.Name = thisMatter.Matter_Type__c + '-' + Axiom_Utilities.getCSVFieldValues(counterPartyRoles, 'Legal_Entity_Name__r.CSID__c', true,255) + '-' + Axiom_Utilities.getCSVFieldValues(thisRequest.Request_Agreement_Titles__r, 'Agreement_Title__r.Name', false,255).replace(',','-');
                thisMatter.Name = thisMatter.Name.Replace('null','').replaceAll('[-]{2,}','-').trim();
                thisMatter.Name = thisMatter.name.length() >= 80 ? thisMatter.name.left(79).trim() : thisMatter.Name;   
                thisMatter.Name = thisMatter.name.right(1) == '-' ? thisMatter.name.left(thisMatter.name.length()-1) : thisMatter.name;
                thisMatter.Request_Date__c = thisRequest.Framesoft_Created_Date__c;     
            }

            //final modifications to due dilligence matters
            for(Matter__c thisMatter : dueDilligenceMatters)
            {

                thisMatter.recordTypeId = recordTypeMap.get('Due_Dilligence').id;
                thisMatter.Credit_Suisse_Entity__c= Axiom_Utilities.getCSVFieldValues(csEntityRoles, 'Legal_Entity_Name__r.Legal_Entity_Name__c', true,255);
                    
                thisMatter.Description__c = Axiom_Utilities.getCSVFieldValues(thisRequest.Request_Agreement_Titles__r, 'Description__c', false,255);
                thisMatter.Master_Agreement_Type__c = Axiom_Utilities.getCSVFieldValues(thisRequest.Request_Agreement_Titles__r, 'Master_Agreement_Type__c', true,255);
                thisMatter.Agreement_Title_Text__c = Axiom_Utilities.getCSVFieldValues(thisRequest.Request_Agreement_Titles__r, 'Agreement_Title__r.Name', false,255);
                thisMatter.CNID__c = Axiom_Utilities.getCSVFieldValues(thisRequest.Request_Agreement_Titles__r, 'CNID__c', true,255);
                thisMatter.Request_Type__c = Axiom_Utilities.getCSVFieldValues(thisRequest.Request_Agreement_Titles__r, 'Agreement_Title__r.Class__c', true,255);
                thisMatter.OwnerId = queueId != null ? queueId : thisRequest.ownerId;

                thisMatter.Name = thisMatter.Matter_Type__c + '-' + thisMatter.CSID__c + '-' + Axiom_Utilities.getCSVFieldValues(thisRequest.Request_Agreement_Titles__r, 'Agreement_Title__r.Name', false,255).replace(',','-');
                thisMatter.Name = thisMatter.Name.Replace('null','').replaceAll('[-]{2,}','-').trim();
                thisMatter.Name = thisMatter.name.length() >= 80 ? thisMatter.name.left(79).trim() : thisMatter.Name;   
                thisMatter.Name = thisMatter.name.right(1) == '-' ? thisMatter.name.left(thisMatter.name.length()-1) : thisMatter.name;
                thisMatter.Request_Date__c = thisRequest.Framesoft_Created_Date__c;      
            }    
            
            //final modications on agreement matters
            for(Matter__c thisMatter : agreementMatters)
            {
                thisMatter.recordTypeId = recordTypeMap.get('Agreement').id;
                thisMatter.OwnerId = queueId != null ? queueId : thisRequest.ownerId;
                thisMatter.Request_Date__c = thisRequest.Framesoft_Created_Date__c; 

				if(thisMatter.Counterparty__c != null){
					set<string> counterparties = new set<string>(thisMatter.Counterparty__c.split(','));
					counterparties.addAll(Axiom_Utilities.getCSVFieldValues(indirectPrincipalRoles, 'Legal_Entity_Name__r.Legal_Entity_Name__c', true, 255).split(','));
					thisMatter.Counterparty__c = String.Join(new list<string>(counterparties),',');
			        thisMatter.Counterparty__c = thisMatter.Counterparty__c.trim().right(1) == ',' ? thisMatter.Counterparty__c.left(thisMatter.Counterparty__c.trim().length()-1) : thisMatter.Counterparty__c;      
					thisMatter.Counterparty__c = thisMatter.Counterparty__c.length() > 255 ? thisMatter.Counterparty__c.substring(0,255) : thisMatter.Counterparty__c;
				}

				if(thisMatter.Counterparty_Legal_Id__c != null){
					set<string> counterpartyIds = new set<string>(thisMatter.Counterparty_Legal_Id__c.split(','));
					counterpartyIds.addAll(Axiom_Utilities.getCSVFieldValues(indirectPrincipalRoles, 'Legal_Id__c', true, 255).split(','));
					thisMatter.Counterparty_Legal_Id__c = String.Join(new list<string>(counterpartyIds),',');
			        thisMatter.Counterparty_Legal_Id__c = thisMatter.Counterparty_Legal_Id__c.trim().right(1) == ',' ? thisMatter.Counterparty_Legal_Id__c.left(thisMatter.Counterparty_Legal_Id__c.trim().length()-1) : thisMatter.Counterparty_Legal_Id__c;      
					thisMatter.Counterparty_Legal_Id__c = thisMatter.Counterparty_Legal_Id__c.length() > 255 ? thisMatter.Counterparty_Legal_Id__c.substring(0,255) : thisMatter.Counterparty_Legal_Id__c;
				}
                
                if(thisMatter.Country_of_Incorporation_c__c != null)
                {
                    set<string> countries = new set<string>(thisMatter.Country_of_Incorporation_c__c.split(','));
                    countries.addAll(Axiom_Utilities.getCSVFieldValues(indirectPrincipalRoles, 'Legal_Entity_Name__r.Country_of_Incorporation__c', true,255).split(','));
                    thisMatter.Country_of_Incorporation_c__c = String.Join(new list<string>(countries),',');
			        thisMatter.Country_of_Incorporation_c__c = thisMatter.Country_of_Incorporation_c__c.trim().right(1) == ',' ? thisMatter.Country_of_Incorporation_c__c.left(thisMatter.Country_of_Incorporation_c__c.trim().length()-1) : thisMatter.Country_of_Incorporation_c__c;      
                    thisMatter.Country_of_Incorporation_c__c = thisMatter.Country_of_Incorporation_c__c.length() > 255 ? thisMatter.Country_of_Incorporation_c__c.substring(0,255) : thisMatter.Country_of_Incorporation_c__c; 
                }
                
                if(thisMatter.CSID__c != null)
                {
                    set<string> CSID = new set<string>(thisMatter.CSID__c.split(','));
                    CSID.addAll(Axiom_Utilities.getCSVFieldValues(indirectPrincipalRoles, 'Legal_Entity_Name__r.CSID__c', true,255).split(','));
                    thisMatter.CSID__c= String.Join(new list<string>(CSID),',');
			        thisMatter.CSID__c = thisMatter.CSID__c.trim().right(1) == ',' ? thisMatter.CSID__c.left(thisMatter.CSID__c.trim().length()-1) : thisMatter.CSID__c;                      
                    thisMatter.CSID__c= thisMatter.CSID__c.length() > 255 ? thisMatter.CSID__c.substring(0,255) : thisMatter.CSID__c; 
                }
                                
         
            }      

        }


        list<Matter__c> allMatters = new list<Matter__c>();
        allMatters.addAll(dataAdminMatters);
        allMatters.addAll(dueDilligenceMatters);
        allMatters.addAll(agreementMatters);
            
        insert allMatters;
        update updateRequests;
        return allMatters;
    }

    
    public static Matter__c initMatterFromRequest(Request__c parentRequest, Legal_Entity_Role__c LER, Request_Agreement_Title__c RAT, boolean multiTemplate, Matter__c newMatter )
    {
        
       
        newMatter = newMatter != null ? newMatter : new Matter__c();
        try
        {
            
            newMatter.Description__c = RAT != null ? RAT.Description__c : null;
            newMatter.Request__c = parentRequest.Id;
            newMatter.Agreement_Number__c = parentRequest.Name;
            newMatter.CSID__c = LER != null ? LER.Legal_Entity_Name__r.CSID__c: null;
            newMatter.Country_of_Incorporation_c__c = LER != null ? LER.Legal_Entity_Name__r.Country_of_Incorporation__c : null;
            newMatter.Master_Agreement_Type__c = RAT != null ? RAT.Master_Agreement_Type__c : null;
            newMatter.Agreement_Title_Text__c = RAT != null ? RAT.Agreement_Title__r.Name : null;
            newMatter.CNID__c = RAT !=null ? RAT.CNID__c : null;
            newMatter.Request_Type__c = RAT != null ? RAT.Agreement_Title__r.class__c : null;
            newMatter.CounterParty__c = LER != null ? LER.Legal_Entity_Name__r.Legal_Entity_Name__c : null;
            newMatter.CounterParty_Legal_Id__c = LER != null ? LER.Legal_Id__c : null;
            newMatter.stage__c = null;
            newMatter.step__c = null;
            newMatter.status__c = null;        
    
            string ratName = RAT != null ? RAT.Agreement_Title__r.Name : '';
            newMatter.Name = newMatter.Matter_Type__c + '-' + newMatter.CSID__c + '-' + ratName ;
            newMatter.Name = newMatter.Name.Replace('null','').replaceAll('[-]{2,}','-');
            newMatter.Name = newMatter.name.length() >= 80 ? newMatter.name.left(79) : newMatter.Name;
            newMatter.Name = newMatter.name.right(1) == '-' ? newMatter.name.left(newMatter.name.length()-1) : newMatter.name;  
        }
        catch(exception e)
        {
            system.debug('\n\n\n ------- Error initializing matter from request data');
            system.debug(e);
        }
        return newMatter;
    }
    
    public static list<Matter__c> findLegalEntityPermutations(list<Legal_Entity_Role__c> sourceRoles, 
                                                              integer index, 
                                                              request__c parentRequest,
                                                              Request_Agreement_Title__c thisRAT,
                                                              boolean multiTemplate)
    {      
        list<Matter__c> generatedMatters = new list<Matter__c>();
        
        Legal_Entity_Role__c role1 = sourceRoles[index];

        if(orgEntities.contains(role1.Role__c))
        { 
            for(Legal_Entity_Role__c role2: sourceRoles)
            {
                if(externalEntities.contains(role2.Role__c))
                {         
                    Matter__c newMatter = new Matter__c(Matter_Type__c = 'Agreement');
                    newMatter.Credit_Suisse_Entity__c = role1.Legal_Entity_Name__r.Legal_Entity_Name__c;                   
                    newMatter.CounterParty__c  = role2.Legal_Entity_Name__r.Name;                     
                    newMatter = initMatterFromRequest(parentRequest, role2, thisRAT, multiTemplate, newMatter);
                                      
                    generatedMatters.add(newMatter);
                }
            }
        }
        if(index < sourceRoles.size()-1)
        {
            index = index + 1;
            generatedMatters.addAll(findLegalEntityPermutations(sourceRoles, index, parentRequest, thisRAT, multiTemplate));
        }

        return generatedMatters;

    }
    

	//This method applies Secrecy Group updates to Matter records where the Secrecy Group has been changed on the Request.
	//This is needed to maintain the Secrecy Group value on the Matter which is used to drive data visibility 
	public static void updateMatterSecrecyGroup(List<Request__c> newList, List<Request__c> oldList){
		Map<Id, String> requestSecrecyGroupChanged = new Map<Id, String>();	//Map of Request records where Secrecy Gtoup has been modified
		Map<Id, String> requestSharingMethodChanged = new Map<Id, String>();	//Map of Request records where Secrecy Gtoup has been modified
		
		//Loop through all updated Legal Entities; add to the map and records where the Secrecy Group has been changed
		for(integer i = 0; i< newList.size(); i++){
			if (newList[i].Secrecy_Group__c <> oldList[i].Secrecy_Group__c || newList[i].Sharing_Method__c <> oldList[i].Sharing_Method__c)
				requestSecrecyGroupChanged.put(newList[i].Id, newList[i].Secrecy_Group__c);
				requestSharingMethodChanged.put(newList[i].Id, newList[i].Sharing_Method__c);
		}
		
		//For all Requests where the Secrecy Group has been changed, find all of their child Matter records and 
		//update the Secrecy Group on those records
		if(requestSecrecyGroupChanged.size() > 0){
			List<Matter__c>matterSecrecyGroupChanged = [SELECT Id, Request__c, Secrecy_Group_Name__c, Sharing_Method_Name__c 
																			FROM Matter__c
																			WHERE Request__c IN :requestSecrecyGroupChanged.keyset()];

			for (Matter__c matter : matterSecrecyGroupChanged){
				matter.Secrecy_Group_Name__c = requestSecrecyGroupChanged.get(matter.Request__c);
				matter.Sharing_Method_Name__c = requestSharingMethodChanged.get(matter.Request__c);
			}
			
			if (matterSecrecyGroupChanged.size() > 0){
				update matterSecrecyGroupChanged;
			}	
		}
	}
        
    public class customException extends Exception {}

}