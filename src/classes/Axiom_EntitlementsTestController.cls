public class Axiom_EntitlementsTestController
{
    public boolean entitlementsEnabled{get;set;}
    public List<SelectOption> users {get;set;}
    public List<SelectOption> functions {get;set;}
    public List<Group_Entitlement__c> allEntitlmentsForFunction {get;set;}
    
    public Axiom_Utilities.remoteObject entitlementResponse {get;set;}
    
    public id userId {get;set;}
    public string functionName {get;set;}
    public Axiom_EntitlementsTestController()
    {
        users = new list<selectOption>();
        functions = new list<selectOption>();
        entitlementsEnabled = Axiom_Misc_Settings__c.getOrgDefaults().Enable_Entitlements_Engine__c;
        for(User thisUser :[select name, id from user order by name])
        {
            users.add(new SelectOption(thisUser.id, thisUser.Name));
        }
        for(AggregateResult thisFunction : [select name from Function__c group By Name order by name])
        {
            functions.add(new SelectOption((string) thisFunction.get('Name'), (string) thisFunction.get('Name')));
        }
    }
    
    public pageReference getUserIsEntitlement()
    {
        entitlementResponse  = Axiom_Utilities.getUserEntitlement(userId, functionName);
        allEntitlmentsForFunction = [select Name, Function__r.Name, Id, Group_Id__c, Case_Safe_Group_Id__c, Group_Name__c from Group_Entitlement__c where function__r.Name = :functionName];
        return null;
    }
}