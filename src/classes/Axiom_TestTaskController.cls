@isTest
global class Axiom_TestTaskController
{
    //when a task with a certain status is completed, if that task is related to a request, a given field should be updated on that request to 
    //have the 'completed' status. This mapping of task statuses to request fields is provided via the getTaskSubjectToRequestFieldMap in the 
    //Axiom_TaskController class. In the future this should probably be abstracted to a custom setting.
    @isTest
    global static void testUpdateRequestFieldsFromTasks()
    {
        Axiom_Utilities.generateTestData();
        Test.startTest();

        //get the list of all task statuses that should update a field on the request
        map<string,string> subjectToRequestMap = Axiom_TaskController.getTaskSubjectToRequestFieldMap();
        Request__c parentRequest = [select id from Request__c limit 1];

        list<Task> tasksToCreate = new list<Task>();
        //create a task for each field type
        for(string thisStatus : subjectToRequestMap.keySet())
        {
            task thisTask = new task();
            thisTask.whatId = parentRequest.id;
            thisTask.subject = thisStatus;
            thisTask.ownerId = userInfo.getUserId();
            thisTask.status = 'completed';
            thisTask.priority = 'normal';
            thisTask.Apex_Context__c = true;
            tasksToCreate.add(thisTask);
            
        }   

        //insert the tasks which should cause the update trigger to fire     
        insert tasksToCreate;

        //query the fields on the request that should be updated from those tasks
        Id requestId = parentRequest.Id;
        string queryString = 'select id, ' + string.valueOf(subjectToRequestMap.values()).replace('(','').replace(')','') + ', name from request__c  where id =:requestId limit 1';
        
        //get the request made by the test data generator tool
        Request__c parentRequestAssert = database.query(queryString);

        Axiom_TaskController.UpdateRequestFieldsFromTasks(tasksToCreate);
        //assert that each field on the request has been updated with the new completed value
        for(string thisStatus : subjectToRequestMap.values())
        {

            //system.assertEquals('completed',parentRequestAssert.get(thisStatus));
        }
        Test.stopTest();

    }

    @isTest
    global static void testUpdateMatterFromFinalizeTask()
    {
        Axiom_Utilities.generateTestData();
        Test.startTest();

        list<AxiomSectionFields__c> sectionFields = new list<AxiomSectionFields__c>();
        AxiomSectionFields__c section1  = new AxiomSectionFields__c();
        section1.Object_Name__c = 'version__c';
        section1.section_number__c = 1;
        section1.Field_Api_Name__c = 'stage__c';
        //get the list of all task statuses that should update a field on the request

        Version__c parentVersion = [select id, matter__c from Version__c limit 1];

        list<Task> tasksToCreate = new list<Task>();
        //create a task for each field type
        map<string,string> subjectToRequestMap = Axiom_TaskController.getTaskSubjectToRequestFieldMap();
        for(string thisStatus : subjectToRequestMap.keySet())
        {
            task thisTask = new task();
            thisTask.whatId = parentVersion.id;
            thisTask.subject = thisStatus + ' [1]';
            thisTask.ownerId = userInfo.getUserId();
            thisTask.status = 'completed';
            thisTask.section__c = 1;
            thisTask.priority = 'normal';
            thisTask.Apex_Context__c = true;
            tasksToCreate.add(thisTask);
            
        }   

        //insert the tasks which should cause the update trigger to fire     
        insert tasksToCreate;


        Axiom_TaskController.updateMatterFromFinalizeTask(tasksToCreate);
        
        Test.stopTest();        
    }
    @isTest
    global static void testTaskController()
    {
        Axiom_TaskController controller = new Axiom_TaskController();
        
        
        
        AxiomTaskUpdate__c update1 = new AxiomTaskUpdate__c();
        update1.Name = 'RequestStageUpdate';
        update1.Field_To_Update__c = 'Stage__c';
        update1.New_Value__c = 'completed';
        update1.Related_Object_Type__c = 'Request';
        update1.Task_Subject__c = 'Request On Hold';
        update1.Task_Statuses__c = 'completed';
        insert update1;
        
        //generate some test data
        Axiom_Utilities.generateTestData();

        Matter__c parentMatter = [select id from matter__c limit 1];
        Contact clientContact = [select id from contact limit 1];
                 
        //figure out what fields need to get modified
        map<string,string> subjectToRequestMap = Axiom_TaskController.getTaskSubjectToRequestFieldMap();
        
        string queryString = 'select id, ' + string.valueOf(subjectToRequestMap.values()).replace('(','').replace(')','') + ', name from request__c limit 1';
        
        //get the request made by the test data generator tool
        Request__c parentRequest = database.query(queryString);
        
        Test.StartTest();
        
        //make sure all the fields on this object that are supposed to get updated by tasks are null
        for(string thisField : subjectToRequestMap.values())
        {
            system.assertEquals('none',string.valueOf(parentRequest.get(thisField)).toLowerCase());
        }
        
        //create a new task for each of the trigger status values
        list<task> tasksToCreate = new list<task>();

        list<recordType> clockStopper = [select id from recordType where sObjectType = 'task' and name = 'Clockstopper']; 
        for(string thisStatus : subjectToRequestMap.keySet())
        {
            task thisTask = new task();
            thisTask.whatId = parentMatter.id;
            thisTask.subject = thisStatus + ' [1]';
            thisTask.ownerId = userInfo.getUserId();
            thisTask.status = 'open';
            thisTask.priority = 'normal';
            thisTask.Apex_Context__c = true;
            if(!clockStopper.isEmpty())
            {
                thisTask.recordTypeId = clockstopper[0].id;
            }
            tasksToCreate.add(thisTask);
            
        }        
        insert tasksToCreate;

        //get the request made by the test data generator tool
        parentRequest = database.query(queryString);
        
        //make sure all the fields on this object that are supposed to get updated by tasks are now completed
        for(string thisField : subjectToRequestMap.values())
        {
            //system.assertEquals('completed',string.valueOf(parentRequest.get(thisField)).toLowerCase());
        }    
          
        
        //Test task page VF components
        Axiom_TaskController taskController = new Axiom_TaskController();
        
        ApexPages.StandardController tc = new ApexPages.standardController(tasksToCreate[0]);
        Axiom_TaskController taskControllerExtension = new Axiom_TaskController(tc);
        
        PageReference pageRef = Page.Axiom_TaskClockstoppers;
        pageRef.getParameters().put('id', String.valueOf(tasksToCreate[0].Id));
        Test.setCurrentPage(pageRef);
                
        //find all related clockstoppers
        list<Clockstopper__c> stoppers = taskControllerExtension.relatedClockstoppers;

        //pageRef = Page.Axiom_MatterTaskView;
        //pageRef.getParameters().put('id', String.valueOf(parentMatter.Id));
        //Test.setCurrentPage(pageRef);
        taskControllerExtension.relationField = 'whatId';        
        list<Task> tasks = taskControllerExtension.relatedTasks;
        
        //Test some of the misc methods on the task controller
        
        Axiom_TaskController.updateRequestFieldsFromTasks(tasksToCreate);
        Axiom_TaskController.setNumberOfDaysOpen_before(tasksToCreate);
        Axiom_TaskController.updateMatterFromFinalizeTask(tasksToCreate);
        Axiom_TaskController.updateMatterNumberOfDaysToFSR(tasksToCreate);
        
        
        tasksToCreate[0].status = 'Completed';
        update tasksToCreate[0];
        
        //Test some of the misc methods on the task controller
        
        Axiom_TaskController.updateRequestFieldsFromTasks(tasksToCreate);
        Axiom_TaskController.setNumberOfDaysOpen_before(tasksToCreate);
        Axiom_TaskController.updateMatterFromFinalizeTask(tasksToCreate);
        Axiom_TaskController.updateMatterNumberOfDaysToFSR(tasksToCreate);   
        
        delete tasksToCreate;
        
        Test.StopTest();     
    }
}