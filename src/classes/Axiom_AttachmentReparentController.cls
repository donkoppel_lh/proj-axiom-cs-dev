public class Axiom_AttachmentReparentController 
{

    public id sourceId{get;set;}
    public id targetId{get;set;}
       
    public list<selectOption> sourceAttachmentNames{get;set;}
    public list<selectOption> targetAttachmentNames{get;set;}
    public string infoMessage{get;set;}
    public map<Id,Attachment> attachmentMap = new map<Id,Attachment>();
    
    /**
    * Constructor. Sets the sourceId and targetId variables for the rest of the class to use by reading them from the URL, or 
    * leaving them be if they've been set by invoker. Calls getAttachmentListData()
    * @return void
    **/   
        
    public Axiom_AttachmentReparentController()
    {
        if(sourceId == null)
        {
            sourceId = apexpages.currentpage().getparameters().get('sourceRecordId');
        }
        if(targetId == null)
        {
            targetId = apexpages.currentpage().getparameters().get('targetRecordId');
        }
                
        getAttachmentListData();        
    } 
    
    /**
    * Figured out what attachments are currently on each object (source and target). Creates select lists for the UI
    * @return void
    **/     
    public void getAttachmentListData()
    {
        try
        {
            sourceAttachmentNames = new list<selectOption>();
            targetAttachmentNames = new list<selectOption>();
                            
            list<Attachment> sourceAttachmentsList = [select name, id, parentId from Attachment where (parentId = :sourceId or parentId = :targetId)];
            
            for(Attachment attachment : sourceAttachmentsList)
            {
                attachmentMap.put(attachment.id,attachment);
                
                if(attachment.parentId == sourceId)
                {
                    sourceAttachmentNames.add(new selectOption(attachment.Id, attachment.Name));
                }
                else
                {
                    targetAttachmentNames.add(new selectOption(attachment.Id, attachment .Name));
                }
            } 
        }
        catch(exception e)
        {
            infoMessage = e.getMessage() + ' on line ' + e.getLineNumber();
            system.debug('\n\n\n----------------------- ERROR!: ' + infoMessage);
        }
    }

    /**
    * Figures out which attachments the user has selected to have their parent changed. Passed them to the async/@future reparentRecord method.
    * @return pageReference (null)
    **/    
    public pageReference save()
    {
        try
        {                    
            //The first thing we need to do is figure out what attachments have had their parent changed so we can then query for them again, this time getting their body content since we need that for
            //cloneing. So we will look at both lists of currently selected select options and see what if any entries in those lists do not have the matching parent ID for that list. Any selected option
            //that doesn't have a parent ID that matches the parent ID for that list has been moved, and needs to get queried for/cloned.  
            for(selectOption thisOption : sourceAttachmentNames)
            {
                attachment thisAttachment = attachmentMap.get(thisOption.getValue());
                
                //Since we are looking at the list of options that is now on the source object if the current attachments parent is NOT the source object, then that means
                //the parent has changed and we have to perform the clone/update/delete process          
                system.debug('\n\n\n------------------- Checking to see if ' + thisOption.getValue() + ' is not equal to ' + sourceId);
                if(thisAttachment.parentId != sourceId)
                {           
                    Axiom_AttachmentReparentController.reparentRecord(thisOption.getValue(), sourceId);
                }
            }
    
            for(selectOption thisOption : targetAttachmentNames)
            {
                attachment thisAttachment = attachmentMap.get(thisOption.getValue());
                
                //Since we are looking at the list of options that is now on the target object if the current attachments parent is NOT the target object, then that means
                //the parent has changed and we have to perform the clone/update/delete process                
                if(thisAttachment.parentId != targetId)
                {           
                    Axiom_AttachmentReparentController.reparentRecord(thisOption.getValue(), targetId);
                }
            } 
            infoMessage = 'Attachments have been queued for reparenting. This process may take up to a few minutes. Please use the refresh button to check the progress';

        }
        catch(exception e)
        {
            infoMessage = e.getMessage() + ' on line ' + e.getLineNumber();
            system.debug('\n\n\n----------------------- ERROR!: ' + infoMessage);
        }              
        return null;
    }
    
    /**
    * Performs the re-parenting of an attachment by querying for it based on id, cloning it, changing the parent id field, inserting it and deleting the original.
    * This is an @future method because the heap size of an @future method is much larger (16mb as opposed to 6mb for a non future) so it allowes much larger files
    * to be re-parented. The only "drawback" of this is that instead of the user knowing exactly when all the reparenting is completed, the operations all just get queued
    * so they don't know. Not a big deal, just a minor annoyance.
    * @param attachmentId the Id of the attachment which to change the parent of
    * @param newParentId the Id of the record to change the parent to
    * @return void 
    **/
    @future
    public static void reparentRecord(id attachmentId, id newParentId)
    {       
        Attachment thisAttachment = [select name, id, contentType, bodyLength, body, parentId from Attachment where Id = :attachmentId];
       
        Attachment newAttachment= thisAttachment.clone();

        newAttachment.parentId = newParentId;
        
        insert newAttachment;
        
        delete thisAttachment;         
    }
}