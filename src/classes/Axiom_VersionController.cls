/******* Axiom Version Controller *********
@Author: Daniel Llewellyn/RedKite (dllewellyn@redkitetechnologies.com)
@Date: ~9/2013
@Description: Methods and properties related to the salesforce Version object.
**********************************************/
global class Axiom_VersionController
{

    public version__c thisVersion;
    
    /**
    * property that returns as a percent the number of completed tasks related to this version. Used by the versionProgress visualforce page to render the progressbar.
    */
    public double completedTaskPercent{get{
        return Axiom_Utilities.getCompletedTaskPercent(thisVersion.id);
    }set;}
    
    /**
    * init puts controller/version in public scope
    */
    public Axiom_VersionController(ApexPages.StandardController controller) {
        thisVersion = (version__c) controller.getRecord();
    }

    /**
    * this method will look at all the fields on the a version record and evaluate if they have been filled out (are not null).
    * each field may belong to a certain section in the page layout, and upon the completion of all fields in a section a related task is updated
    * to complete. Since Apex cannot access page layout meta data directly and hence we don't actually know what fields are in what section what we do
    * instead is look at the at a custom setting that contains names of fields and what section they belong to. When a section is completed we look for a task
    * related to this record with a matching section number (a custom field) and update it to being completed.
    * @param versions a list of versions to look to evaluate task completion on and update if all tasks in a given section are complete.
    * @return null
    **/
    public static void updateTaskOnSectionComplete(list<version__c> versions)
    {        
        //create a map that contains the id of the version record with another map that contains the section names and if they are completed
        Map<id,map<integer,boolean>> versionSectionCompleteMap = getSobjectSectionCompleteMap(versions);

        //container to hold all the tasks that need to get updated
        list<task> updateTasks = new list<task>();
        
        //so now after that we have a map which contains all the versions and all their sections letting us know if they are complete or not. So now we need to find all the tasks
        //that belong to all the versions so they can be updated if required. If a versions section is complete, the task 'related' to that section must be marked as complete if it is not already
        list<task> versionTasks = [select id, status, whatId, Section__c from task where whatid in : versionSectionCompleteMap.keySet() and status != 'completed' and Section__c != 0 ];
                
        //iterate over all the tasks
        for(task thisTask : versionTasks)
        {
            boolean taskSectionIsCompleted = versionSectionCompleteMap.get(thisTask.whatId).get((integer) thisTask.Section__c);

            if(taskSectionIsCompleted)
            {                
                thisTask.status = 'Completed';
                //this flag is used to bypass the validation rule that prevents regular users from closing a versioned task.
                //the validation rule says 'if not apex contect and the status is completed, then error'. Since the apex context flag isn't
                //show on the page layout and isn't modifiable by users it's always false. There is also a workflow rule that sets it being false 
                //after a record is updated so this true instantly becomes false after update to prevents users from editing the record in the future.
                thisTask.Apex_Context__c = true;
                updateTasks.add(thisTask);
            }
        }
                
        //if we have any tasks to update, do that now.
        if(!updateTasks.isEmpty())
        {
            //database.update(updateTasks,false);
            update updateTasks;
        }
        
    }
    
    /*
    For logical separation of the process fields on the version object are divided into 'sections'. On the page layout they
    are literally in different sections, and we want to be able to do certain things when all fields in a section are completed,
    such as send an email. However since they page layout data is not accessable in Apex, we instead store the fields and the section
    they belong in in a custom setting. When a version is updated we query that table, find all the fields and the sections they belong
    in, loop over then object data itself and see if all the fields in a the sections are filled out. This method returns the result
    of that calculation. 
    @param list of objects to find section completion data for.
    @return a map of object ids where the value is a map of section number and a boolean of whether it is complete or not.
    */
    public static Map<id,map<integer,boolean>> getSobjectSectionCompleteMap(list<sobject> sObjects)
    {          
        //map of object ids to a map of sections names and if they are complete or not. Kinda complicated, but efficient.
        Map<id,map<integer,boolean>> objectToSectionsCompletedMap = new Map<id,map<integer,boolean>>();  
            
        //figure out what kind of object this is.
        string sObjectType = sObjects.getSObjectType().getDescribe().getName();
        
        //find all the section/field defenition custom setting rows for this object type
        list<AxiomSectionFields__c> objectSections = [select Field_API_Name__c, Object_Name__c, Section_Number__c from AxiomSectionFields__c where Object_Name__c = :sObjectType];
               
        //first lets get all the fields on the version object.
        Map<String, Schema.SObjectField> objectFields = Schema.getGlobalDescribe().get(sObjectType).getDescribe().fields.getMap();
        
        //now we'll create a map, containing the section name to a list of all fields within the section.
        Map<integer, list<string>> objectSectionsToFieldsMap = new Map<integer, list<string>>();
        
        //create a reference to the values in the versionFields map for faster iteration (looping over a method is slower than just looping over a variable)
        List<Schema.SObjectField> objectFieldRefs = objectFields.values();

        for(AxiomSectionFields__c thisSectionField : objectSections)
        {  
            list<string> sectionsFields = objectSectionsToFieldsMap.containsKey((integer) thisSectionField.Section_Number__c) ? objectSectionsToFieldsMap.get((integer) thisSectionField.Section_Number__c) : new list<string>();
            sectionsFields.add(thisSectionField.Field_API_Name__c);
            objectSectionsToFieldsMap.put((integer) thisSectionField.Section_Number__c,sectionsFields);
        }    
                
        //alright so now we know all the fields and what sections they belong to. So now we need to iterate over every object, figure out if it has all it's sections filled out.
        //if so, then update it's related task.
        
        
        //create the default section completion map, basically saying that all sections are complete. Then when we iterate over each objects's section data
        //we can test and see if the section actually is complete. We init to true because ONE null can make the whole section incomplete but they ALL have to be 
        //filled in to make the section complete. So it's easier to assume complete and prove wrong than to assume assume incomplete and prove true.
        map<integer, boolean> defaultSectionCompletion = new map<integer,boolean>();
        for(integer sectionName : objectSectionsToFieldsMap.keySet())
        {
            defaultSectionCompletion.put(sectionName,true);
        }
        
        //iterate over all the objects
        for(sobject thisObject : sObjects)
        {
            //set all the sections for this object to being completed/true
            objectToSectionsCompletedMap.put((id) thisObject.get('id'),defaultSectionCompletion.clone());
            
            //iterate every section for this object
            for(integer sectionName : objectSectionsToFieldsMap.keySet())
            {
                //iterate every field in this section for this object
                for(string fieldName : objectSectionsToFieldsMap.get(sectionName))
                {
                    //if the value of this field on this object is null that means the section is not complete and should be marked as false.
                    //once we know a section isn't complete there is no reason to continue iteration over it since it cannot be made true again so we break
                    //the field loop and move to the next section
                    if(thisObject.get(fieldName) == null)
                    {
                        objectToSectionsCompletedMap.get((id) thisObject.get('id') ).put(sectionName,false);
                        break;
                    }
                }
            }
            
        }
           
        return objectToSectionsCompletedMap;
         
    }
}