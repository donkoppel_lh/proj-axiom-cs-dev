@isTest
global class ActivityPlans_Test
{
    global static Activity_Plan__c createTestPlan()
    {
        //first create a new activity plan
        Activity_Plan__c thisPlan = new Activity_Plan__c();
        thisPlan.active__c = true;
        thisPlan.Allow_Duplicate_Tasks__c = false;
        thisPlan.Object_Types__c = 'Account';
        
        insert thisPlan;
        
        list<EmailTemplate> emailTemplates = [select developername, name, TemplateType from EmailTemplate where isActive = true and TemplateType = 'visualforce'];
        
        Activity_Plan_Task_Template__c taskTemplate = new Activity_Plan_Task_Template__c();
        taskTemplate.Name = 'Template 1';
        taskTemplate.Activity_Plan__c = thisPlan.Id;
        taskTemplate.Description__c = 'this is a test of task template engine on [object.name]';
        taskTemplate.Assigned_To_Type__c = 'Field Value';
        taskTemplate.Assigned_To_Value__c = 'OwnerId';
        taskTemplate.Due_Date_Type__c = 'Day Offset';
        taskTemplate.Due_Date_Value__c = '0';
        taskTemplate.Priority__c = 'High';
        taskTemplate.Related_To_Type__c = 'Field Value';
        taskTemplate.Related_To_Value__c = 'Id';
        taskTemplate.Task_Status__c = 'In Progress';
        taskTemplate.Depends_On_Completion_Statuses__c = '';
        taskTemplate.Order__c = 1;
        taskTemplate.Substantive_Response__c = true;
        taskTemplate.Task_Weight__c = 1;
        if(!emailTemplates.isEmpty())
        {
            taskTemplate.Notification_Email_Template__c = emailTemplates[0].Id;
        }
        
        insert taskTemplate;

        //create a field update rule (completely non-sensical rule of when a task is completed update the billing state on the account to NY)
        Activity_Plan_Field_Update__c thisUpdateRule = new Activity_Plan_Field_Update__c();
        thisUpdateRule.Active__c = true;
        thisUpdateRule.Activity_Plan__c = thisPlan.Id;
        thisUpdateRule.Activity_Plan_Task_Template__c = taskTemplate.id;
        thisUpdateRule.Assignment_Field_Name__c = 'type';
        thisUpdateRule.Assignment_Type__c = 'Static Value';
        thisUpdateRule.Assignment_Value__c = 'Prospect';
        thisUpdateRule.Comparison_Field_Name__c = 'Status';
        thisUpdateRule.Comparison_Type__c = 'Static Value';
        thisUpdateRule.Comparison_Value__c = 'Completed';
        thisUpdateRule.Logical_Operator__c = 'Equal';
        thisUpdateRule.Trigger__c = 'Every time a record is updated';
        
        insert thisUpdateRule;
        
        Activity_Plan_Task_Template__c taskTemplate2 = new Activity_Plan_Task_Template__c();
        taskTemplate2.Name = 'Template 2';
        taskTemplate2.Activity_Plan__c = thisPlan.Id;
        taskTemplate2.Description__c = 'this is a test of task template engine on [object.name]';
        taskTemplate2.Assigned_To_Type__c = 'Field Value';
        taskTemplate2.Assigned_To_Value__c = 'OwnerId';
        taskTemplate2.Due_Date_Type__c = 'Day Offset';
        taskTemplate2.Due_Date_Value__c = '0';
        taskTemplate2.Priority__c = 'High';
        taskTemplate2.Related_To_Type__c = 'Field Value';
        taskTemplate2.Related_To_Value__c = 'Id';
        taskTemplate2.Task_Status__c = 'In Progress';
        taskTemplate2.Depends_on__c = taskTemplate.id;
        taskTemplate2.Depends_On_Completion_Statuses__c = 'Completed';
        taskTemplate2.Order__c = 1;
        taskTemplate2.Substantive_Response__c = true;
        taskTemplate2.Task_Weight__c = 1;
        if(!emailTemplates.isEmpty())
        {
            taskTemplate2.Notification_Email_Template__c = emailTemplates[0].Id;
        }
        
        insert taskTemplate2;
                
        Activity_Plan_Rule__c thisRule = new Activity_Plan_Rule__c();
        thisRule.Activity_Plan__c = thisPlan.Id;
        thisRule.Comparison_Value__c = 'MN';
        thisRule.Field_Name__c = 'billingstate';
        thisRule.Logical_Operator__c = 'Equal';
        thisRule.Comparison_Type__c = 'Static Value';
        insert thisRule;    
        
        list<Activity_Plan__c> planData = [select 
                                                    name,
                                                    id,
                                                    Active__c,
                                                    Object_Types__c,
                                                    (select
                                                         Activity_Plan__c,
                                                         Comparison_Value__c,
                                                         Comparison_Field_Name__c,
                                                         Comparison_Type__c,
                                                         Activity_Plan_Task_Template__c,                                                        
                                                         Logical_Operator__c,
                                                         Assignment_Field_Name__c,
                                                         Assignment_Type__c,
                                                         Assignment_Value__c                                                        
                                                     from
                                                         Activity_Plan_Field_Updates__r),
                                                    (select 
                                                         Name,
                                                         Activity_Plan__c,
                                                         Assigned_To_Type__c,
                                                         Assigned_To_Value__c,
                                                         Due_Date_Type__c,
                                                         Due_Date_Value__c,
                                                         Priority__c,
                                                         Related_To_Type__c,
                                                         Related_To_Value__c,
                                                         Task_Status__c,
                                                         Record_Type_Name__c,
                                                         Allow_Duplicate_Tasks__c,
                                                         Depends_On__c,
                                                         Depends_On_Completion_Statuses__c,
                                                         Depends_On__r.Name,
                                                         Description__c,
                                                         Notification_Email_Template__c,
                                                         Task_Weight__c,
                                                         Due_Date_Offset_Source__c,
                                                         Order__c,
                                                         Substantive_Response__c 
                                                     from 
                                                         Activity_Plan_Task_Templates__r
                                                         order by createdDate),
                                                         
                                                    (select
                                                         Activity_Plan__c,
                                                         Comparison_Value__c,
                                                         Activity_Plan_Task_Template__c,
                                                         Field_Name__c,
                                                         Logical_Operator__c,
                                                         Comparison_Type__c
                                                     from
                                                         Activity_Plan_Rules__r)
                                                 from Activity_Plan__c];        
        return planData[0];
    }
    
    @isTest
    global static void testCreateTask()
    {   

        Activity_Plan__c thisPlan = ActivityPlans_Test.createTestPlan();
        Activity_Plan_Task_Template__c taskTemplate = thisPlan.Activity_Plan_Task_Templates__r[0];
        Activity_Plan_Task_Template__c taskTemplate2 = thisPlan.Activity_Plan_Task_Templates__r[1];
        Activity_Plan_Rule__c thisRule = thisPlan.Activity_Plan_Rules__r[0];
                
        Test.StartTest();
        
        Account thisAccount = new Account();
        thisAccount.name = 'My Test Account';
        thisAccount.billingstate = 'MN';
        thisAccount.ownerId = UserInfo.getUserId();
        insert thisAccount;
        
        list<account> accountList = new list<account>();
        accountList.add(thisAccount);
        ActivityPlansController.findMatchingActivityPlans(accountList);
        
        Test.StopTest();
               
        list<task> taskData = [select id, subject, activitydate, priority, Created_by_Template__c, status, ownerId from task where whatId = :thisAccount.id];                                                         
        system.assertEquals(1,taskData.size());
        system.assertEquals(taskTemplate.Task_Status__c,taskData[0].status);
    }

    @isTest
    global static void testFieldUpdateRule()
    {   

        Activity_Plan__c thisPlan = ActivityPlans_Test.createTestPlan();

                
        Test.StartTest();
        
        Account thisAccount = new Account();
        thisAccount.name = 'My Test Account';
        thisAccount.billingstate = 'MN';
        thisAccount.ownerId = UserInfo.getUserId();
        insert thisAccount;
        
        list<account> accountList = new list<account>();
        accountList.add(thisAccount);
        ActivityPlansController.findMatchingActivityPlans(accountList);
        
        
        //find the task that should have been created by the activity plan
        list<task> taskData = [select id, subject, activitydate, priority, Created_by_Template__c, status, ownerId from task where whatId = :thisAccount.id];
        system.assertEquals(1,taskData.size());
        
        //update the task status to completed, which should cause the field update rule to fire that will update the accounts type to prospect
        taskData[0].Status = 'Completed';
        update taskData[0];
        
        Test.StopTest();
        
        thisAccount = [select id, billingState, type from Account where id = :thisAccount.id];
        
        system.assertEquals('Prospect',thisAccount.type);
    }

    @isTest
    global static void testTaskDependency()
    {   

        Activity_Plan__c thisPlan = ActivityPlans_Test.createTestPlan();
                
        Test.StartTest();
        
        Account thisAccount = new Account();
        thisAccount.name = 'My Test Account';
        thisAccount.billingstate = 'MN';
        thisAccount.ownerId = UserInfo.getUserId();
        insert thisAccount;
        
        list<account> accountList = new list<account>();
        accountList.add(thisAccount);
        ActivityPlansController.findMatchingActivityPlans(accountList);
        
        
        //find the task that should have been created by the activity plan
        list<task> taskData = [select id, subject, activitydate, priority, Created_by_Template__c, status, ownerId from task where whatId = :thisAccount.id];
        system.assertEquals(1,taskData.size());
        
        //update the task status to completed, which should cause the dependency to be completed and create the second task
        taskData[0].Status = 'Completed';
        
        system.debug('\n\n\n\n---------------------------------- UPDATING TASK NOW!');
        system.debug(taskData[0]);
        update taskData[0];
        
        Test.StopTest();
        
        //since the second task template depends on the first to be completed before it creates a task, now that second task should exist
        taskData = [select id, subject, activitydate, priority, Created_by_Template__c, status, ownerId from task where id = :taskData[0].id];
        
        //there should be two tasks for that account now instead of just one
        
        //system.assertEquals(2,taskData.size());
    }
            
    @isTest
    global static void testDynamicIf()
    {            

        
        system.assertEquals(ActivityPlansController.dynamicIf(1,'equal',1),true);
        system.assertEquals(ActivityPlansController.dynamicIf(1,'notequal',2),true);
        system.assertEquals(ActivityPlansController.dynamicIf(1,'greaterthan',0),true);
        system.assertEquals(ActivityPlansController.dynamicIf(1,'lessthan',3),true);
        system.assertEquals(ActivityPlansController.dynamicIf('mystring','contains','string'),true);
        system.assertEquals(ActivityPlansController.dynamicIf('mystring','doesnotcontain','bannana'),true);
    }
    @isTest
    global static void testTaskTemplateVFOverride()
    {    
        Activity_Plan__c thisPlan = ActivityPlans_Test.createTestPlan();   
        Activity_Plan_Task_Template__c taskTemplate = thisPlan.Activity_Plan_Task_Templates__r[0];
        Activity_Plan_Task_Template__c taskTemplate2 = thisPlan.Activity_Plan_Task_Templates__r[1];
        Activity_Plan_Rule__c thisRule = thisPlan.Activity_Plan_Rules__r[0];
        
        Test.StartTest();
        
        //Test Page Overrides
        
        ApexPages.StandardController sc = new ApexPages.standardController(taskTemplate);
        ActivityPlans_TaskTemplateController templateController = new  ActivityPlans_TaskTemplateController(sc);

        PageReference pageRef = Page.ActivityPlans_TaskTemplate;
        pageRef.getParameters().put('id', taskTemplate.Id);
        Test.setCurrentPage(pageRef);    
        
        templateController.selectedTaskStatus = 'closed';
        templateController.selectedTaskPriority = 'high';
        templateController.selectedAssignedToVal = 'Static Value';
        templateController.selectedRelatedToVal = 'OwnerId';

     
        templateController.save();
        
        
    }
    @isTest
    global static void testRuleVFOverride()
    {
        Activity_Plan__c thisPlan = ActivityPlans_Test.createTestPlan();   
        Activity_Plan_Task_Template__c taskTemplate = thisPlan.Activity_Plan_Task_Templates__r[0];
        Activity_Plan_Task_Template__c taskTemplate2 = thisPlan.Activity_Plan_Task_Templates__r[1];
        Activity_Plan_Rule__c thisRule = thisPlan.Activity_Plan_Rules__r[0];
        
        Test.StartTest();
                     
        // Test Rule UI Override
        ApexPages.StandardController sc2 = new ApexPages.standardController(thisRule);
        ActivityPlans_RuleController ruleController = new ActivityPlans_RuleController(sc2);

        PageReference pageRef = Page.ActivityPlans_Rule;
        pageRef.getParameters().put('id', thisRule.Id);
        Test.setCurrentPage(pageRef);    
        
        ruleController.save();  
        
        list<Activity_Plan_Rule__c> relatedRules = ruleController.relatedRules;
        
        ruleController.thisrule.Comparison_Type__c = 'Object Field Value';
        ruleController.thisrule.Comparison_Value__c = 'Name';
        ruleController.selectedCompareField = 'Name';
        ruleController.save();
        
        string accountNameType = ActivityPlans_RuleController.getFieldType('Account','Name');
    }

    @isTest
    global static void testFieldUpdateVFOverride()
    {
        Activity_Plan__c thisPlan = ActivityPlans_Test.createTestPlan();   
        Activity_Plan_Task_Template__c taskTemplate = thisPlan.Activity_Plan_Task_Templates__r[0];
        Activity_Plan_Task_Template__c taskTemplate2 = thisPlan.Activity_Plan_Task_Templates__r[1];
        Activity_Plan_Field_Update__c thisRule = thisPlan.Activity_Plan_Field_Updates__r[0];
        
        Test.StartTest();
                     
        // Test Rule UI Override
        ApexPages.StandardController sc2 = new ApexPages.standardController(thisRule);
        ActivityPlans_FieldUpdateController ruleController = new ActivityPlans_FieldUpdateController(sc2);

        PageReference pageRef = Page.ActivtiyPlan_UpdateFieldRule;
        pageRef.getParameters().put('id', thisRule.Id);
        Test.setCurrentPage(pageRef);    
        
        ruleController.save();  
        
        
        ruleController.thisrule.Comparison_Type__c = 'Object Field Value';
        ruleController.thisrule.Comparison_Value__c = 'Name';
        ruleController.selectedCompareField = 'Name';
        ruleController.save();
        
        string accountNameType = ActivityPlans_FieldUpdateController.getFieldType('Account','Name');
    }
        
    @isTest
    global static void testLibraryTaskVFOverride()
    {        
        Activity_Plan__c thisPlan = ActivityPlans_Test.createTestPlan();   
        Activity_Plan_Task_Template__c taskTemplate = thisPlan.Activity_Plan_Task_Templates__r[0];
        Activity_Plan_Task_Template__c taskTemplate2 = thisPlan.Activity_Plan_Task_Templates__r[1];
        Activity_Plan_Rule__c thisRule = thisPlan.Activity_Plan_Rules__r[0];
        
        Test.StartTest();
                
        //Test Add Library Task Page

        ApexPages.StandardController sc3 = new ApexPages.standardController(thisPlan);
        ActivityPlans_AddLibraryTaskController addLibTaskController = new ActivityPlans_AddLibraryTaskController(sc3);

        PageReference pageRef = Page.ActivityPlans_AddLibraryTask;
        pageRef.getParameters().put('id', thisPlan.Id);
        Test.setCurrentPage(pageRef);    
        
        
        //Search for a task template
        addLibTaskController.searchText = taskTemplate2.Name; 
        addLibTaskController.search();
        
        //ensure one result
        system.assertEquals(addLibTaskController.planTemplates.size(),1);
        
        //mark the planes for cloning
        for(ActivityPlans_AddLibraryTaskController.taskTemplateWrapper thisPlanWrapper : addLibTaskController.planTemplates)
        {
            thisPlanWrapper.isChecked = true;
        }
        
        addLibTaskController.save();
                
        string fieldData = ActivityPlans_RuleController.getFieldType('Contact','Name'); 
    }
    
    @isTest(seeAllData=true)
    global static void testSendEmail()
    {
        list<EmailTemplate> emailTemplates = [select developername, name, TemplateType from EmailTemplate where isActive = true and TemplateType = 'visualforce'];
        list<contact> testContact = [select id, name, phone from contact];
        if(!emailTemplates.isEmpty())
        {
            ActivityPlansController.sendEmailTemplate(emailTemplates[0].id,UserInfo.getUserId(), testContact[0].id);
        }
    }
    
    
}