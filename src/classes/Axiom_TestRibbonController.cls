@isTest
public class Axiom_TestRibbonController {


    static testMethod void testBadCall()
    {
    	Test.StartTest();
    	    	   
    	Axiom_RibbonController.RibbonRequest req = new Axiom_RibbonController.RibbonRequest();
    	req.objname = 'XXX';
    	req.action = 'X';
    	req.id = '';    	       
    	Axiom_RibbonController.RibbonResponse rtn = Axiom_RibbonController.Dispatch(req);
        	    	
    	Test.StopTest();
    }
      
    static testMethod void testRequestAccept()
    {
    	Axiom_Utilities.generateTestData();
    	Test.StartTest();
    	
    	Request__c thisRequest = [select id,OwnerId,Start_Time__c from Request__c limit 1];
    	    	
    	Axiom_RibbonController.RibbonRequest req = new Axiom_RibbonController.RibbonRequest();
    	req.objname = 'Request__c';
    	req.action = 'Accept';
    	req.id = thisRequest.Id;    	       
    	
    	// Change owner and test
    	Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'test', Email='test@test.com', 
            EmailEncodingKey='UTF-8', LastName='Test', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='test123Akkk@axiomlawtest.com');            
        insert u;
        thisRequest.OwnerId = u.Id;
        Database.update(thisRequest);
        Axiom_RibbonController.RibbonResponse rtn = Axiom_RibbonController.Dispatch(req);
        
        // Now test success
        thisRequest.OwnerId = UserInfo.getUserId();
        Database.update(thisRequest);
    	rtn = Axiom_RibbonController.Dispatch(req);
    	    	
    	// Test already accepted - should be updated from the first test   	  	
    	rtn = Axiom_RibbonController.Dispatch(req);    	
    	
    	// Test Bad Id    	    	    	
    	req.id = '123456789';
    	rtn = Axiom_RibbonController.Dispatch(req);
    	
    	// Test Blank Id    	    	    	
    	req.id = '';
    	rtn = Axiom_RibbonController.Dispatch(req);
    	
    	
    	Test.StopTest();
    }
    
    static testMethod void testRequestReject()
    {
    	Axiom_Utilities.generateTestData();
    	Test.StartTest();
    	
    	Request__c thisRequest = [select id,Start_Time__c,OwnerId,Reason_for_Rejection__c from Request__c limit 1];
    	    	
    	Axiom_RibbonController.RibbonRequest req = new Axiom_RibbonController.RibbonRequest();
    	req.objname = 'Request__c';
    	req.action = 'Reject';
    	req.id = thisRequest.Id;    
    	
		// Change owner and test
    	Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'test', Email='test@test.com', 
            EmailEncodingKey='UTF-8', LastName='Test', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='test123Akkk@axiomlawtest.com');            
        insert u;
        thisRequest.OwnerId = u.Id;
        Database.update(thisRequest);
        Axiom_RibbonController.RibbonResponse rtn = Axiom_RibbonController.Dispatch(req);    	

		// Change back and test without a reason
		thisRequest.OwnerId = UserInfo.getUserId();
        Database.update(thisRequest);    		       
    	rtn = Axiom_RibbonController.Dispatch(req);
    	
    	// Test success
		thisRequest.Reason_for_Rejection__c ='Test Reason';
        Database.update(thisRequest);    		       
    	rtn = Axiom_RibbonController.Dispatch(req);
    	         	    
    	// Test already accepted
    	thisRequest.Start_Time__c = System.now(); 
        Database.update(thisRequest);   	
    	rtn = Axiom_RibbonController.Dispatch(req);    	
    	
    	// Test Bad Id    	    	    	
    	req.id = '123456789';
    	rtn = Axiom_RibbonController.Dispatch(req);
    	
    	// Test Blank Id    	    	    	
    	req.id = '';
    	rtn = Axiom_RibbonController.Dispatch(req);
    	
    	
    	
    	Test.StopTest();
    }    
 
  static testMethod void testMatterAccept()
    {
    	Axiom_Utilities.generateTestData();
    	Test.StartTest();
    	
    	Matter__c thisMatter = [select id,OwnerId from Matter__c limit 1];
    	
    	Axiom_RibbonController.RibbonRequest req = new Axiom_RibbonController.RibbonRequest();
    	req.objname = 'Matter__c';
    	req.action = 'Accept';
    	req.id = thisMatter.Id;
    	
    	// Change owner and test
    	Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'test', Email='test@test.com', 
            EmailEncodingKey='UTF-8', LastName='Test', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='test123Akkk@axiomlawtest.com');            
        insert u;
        thisMatter.OwnerId = u.Id;
        Database.update(thisMatter);
        Axiom_RibbonController.RibbonResponse rtn = Axiom_RibbonController.Dispatch(req);
    	       
		// Change user back    
		thisMatter.OwnerId = UserInfo.getUserId();
        Database.update(thisMatter);    		       
    	rtn = Axiom_RibbonController.Dispatch(req);      
    	
    	// Test already accepted - should be updated from the first test   	
    	rtn = Axiom_RibbonController.Dispatch(req);    	
    	
    	// Test Bad Id    	    	    	
    	req.id = '123456789';
    	rtn = Axiom_RibbonController.Dispatch(req);
    	
    	// Test Blank Id    	    	    	
    	req.id = '';
    	rtn = Axiom_RibbonController.Dispatch(req);
    	
    	Test.StopTest();
    }    
    
    static testMethod void testMatterDecline()
    {
    	Axiom_Utilities.generateTestData();
    	Test.StartTest();
    	
    	Matter__c thisMatter = [select id from Matter__c limit 1];
    	
    	Axiom_RibbonController.RibbonRequest req = new Axiom_RibbonController.RibbonRequest();
    	req.objname = 'Matter__c';
    	req.action = 'Decline';
    	req.id = thisMatter.Id;    	       

    	// Change owner and test
    	Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'test', Email='test@test.com', 
            EmailEncodingKey='UTF-8', LastName='Test', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='test123Akkk@axiomlawtest.com');            
        insert u;
        thisMatter.OwnerId = u.Id;
        Database.update(thisMatter);
        Axiom_RibbonController.RibbonResponse rtn = Axiom_RibbonController.Dispatch(req);
        
        // Change user back    
		thisMatter.OwnerId = UserInfo.getUserId();
        Database.update(thisMatter);    		       
    	rtn = Axiom_RibbonController.Dispatch(req);  
    	
		// Test already accepted - should be updated from the first test   	
    	rtn = Axiom_RibbonController.Dispatch(req);    	
    	
    	// Test Bad Id    	    	    	
    	req.id = '123456789';
    	rtn = Axiom_RibbonController.Dispatch(req);
    	
    	// Test Blank Id    	    	    	
    	req.id = '';
    	rtn = Axiom_RibbonController.Dispatch(req);    	
    	
    	Test.StopTest();
    }  
    
    static testMethod void testTaskAccept()
    {
    	Axiom_Utilities.generateTestData();
    	Test.StartTest();
    	    
    	Task thisTask = [select id,Created_by_Template__c from Task limit 1];    	
    	Axiom_RibbonController.RibbonRequest req = new Axiom_RibbonController.RibbonRequest();
    	req.objname = 'Task';
    	req.action = 'Accept';
    	req.id = thisTask.Id;
    	
    	// Change owner and test
    	Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'test', Email='test@test.com', 
            EmailEncodingKey='UTF-8', LastName='Test', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='test123Akkk@axiomlawtest.com');            
        insert u;
        thisTask.OwnerId = u.Id;
        thisTask.Created_by_Template__c = 'Test';
        Database.update(thisTask);
        Axiom_RibbonController.RibbonResponse rtn = Axiom_RibbonController.Dispatch(req);
    	
    	thisTask.OwnerId = UserInfo.getUserId();
        Database.update(thisTask);    		       
    	rtn = Axiom_RibbonController.Dispatch(req);     
    	
    	// Test already accepted - should be updated from the first test   	
    	rtn = Axiom_RibbonController.Dispatch(req); 
    	
    	// Test Bad Id    	    	    	
    	req.id = '123456789';
    	rtn = Axiom_RibbonController.Dispatch(req);
    	
    	// Test Blank Id    	    	    	
    	req.id = '';
    	rtn = Axiom_RibbonController.Dispatch(req);
    	
    	Test.StopTest();
    } 
    
        static testMethod void testTaskComplete()
    {
    	Axiom_Utilities.generateTestData();
    	Test.StartTest();
    	
    	Task thisTask = [select id from Task limit 1];
    	
    	Axiom_RibbonController.RibbonRequest req = new Axiom_RibbonController.RibbonRequest();
    	req.objname = 'Task';
    	req.action = 'Complete';
    	req.id = thisTask.Id;  
    	// Change owner and test
    	Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'test', Email='test@test.com', 
            EmailEncodingKey='UTF-8', LastName='Test', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='test123Akkk@axiomlawtest.com');            
        insert u;
        thisTask.OwnerId = u.Id;
        thisTask.Created_by_Template__c = 'Test';
        Database.update(thisTask);
        Axiom_RibbonController.RibbonResponse rtn = Axiom_RibbonController.Dispatch(req);    	
    	  	       
    	thisTask.OwnerId = UserInfo.getUserId();
        Database.update(thisTask);    		       
    	rtn = Axiom_RibbonController.Dispatch(req);  

		// Test already accepted - should be updated from the first test   	
    	rtn = Axiom_RibbonController.Dispatch(req); 
    	
    	// Test Bad Id    	    	    	
    	req.id = '123456789';
    	rtn = Axiom_RibbonController.Dispatch(req);
    	
    	// Test Blank Id    	    	    	
    	req.id = '';
    	rtn = Axiom_RibbonController.Dispatch(req);    	
    	
    	Test.StopTest();
    } 
    
     static testMethod void testTaskApprove()
    {
    	Axiom_Utilities.generateTestData();
    	Test.StartTest();
    	
    	Task thisTask = [select id,OwnerId,Been_Approved__c,Been_Rejected__c from Task limit 1];
    	
    	Axiom_RibbonController.RibbonRequest req = new Axiom_RibbonController.RibbonRequest();
    	req.objname = 'Task';
    	req.action = 'Approve';
    	req.id = thisTask.Id;
    		// Change owner and test
    	Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'test', Email='test@test.com', 
            EmailEncodingKey='UTF-8', LastName='Test', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='test123Akkk@axiomlawtest.com');            
        insert u;
        thisTask.OwnerId = u.Id;
        thisTask.Created_by_Template__c = 'Test';
        Database.update(thisTask);
        Axiom_RibbonController.RibbonResponse rtn = Axiom_RibbonController.Dispatch(req);  
		
		// change user back    	       
    	thisTask.OwnerId = UserInfo.getUserId();
        Database.update(thisTask);    		       
    	rtn = Axiom_RibbonController.Dispatch(req); 
    	
    	// check if its been rejected
    	thisTask.Been_Approved__c = false;
    	thisTask.Been_Rejected__c = true;
    	Database.update(thisTask);    		       
    	rtn = Axiom_RibbonController.Dispatch(req); 
    	
    	// and again with both as false - should be marked as closed
    	thisTask.Been_Approved__c = false;
    	thisTask.Been_Rejected__c = false;
    	Database.update(thisTask);    		       
    	rtn = Axiom_RibbonController.Dispatch(req); 
    	
    	// Test Bad Id    	    	    	
    	req.id = '123456789';
    	rtn = Axiom_RibbonController.Dispatch(req);
    	
    	// Test Blank Id    	    	    	
    	req.id = '';
    	rtn = Axiom_RibbonController.Dispatch(req);    	
    	
    	Test.StopTest();
    }   
    
     static testMethod void testTaskReject()
    {
    	Axiom_Utilities.generateTestData();
    	Test.StartTest();
    	
    	Task thisTask = [select id,OwnerId,Been_Approved__c,Been_Rejected__c from Task limit 1];
    	
    	Axiom_RibbonController.RibbonRequest req = new Axiom_RibbonController.RibbonRequest();
    	req.objname = 'Task';
    	req.action = 'Reject';
    	req.id = thisTask.Id;
    	       
		Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'test', Email='test@test.com', 
            EmailEncodingKey='UTF-8', LastName='Test', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='test123Akkk@axiomlawtest.com');            
        insert u;
        thisTask.OwnerId = u.Id;
        thisTask.Created_by_Template__c = 'Test';
        Database.update(thisTask);
        Axiom_RibbonController.RibbonResponse rtn = Axiom_RibbonController.Dispatch(req);  
		
		// change user back    	       
    	thisTask.OwnerId = UserInfo.getUserId();
        Database.update(thisTask);    		       
    	rtn = Axiom_RibbonController.Dispatch(req); 
    	
    	// check if its been rejected
    	thisTask.Been_Approved__c = true;
    	thisTask.Been_Rejected__c = false;
    	Database.update(thisTask);    		       
    	rtn = Axiom_RibbonController.Dispatch(req); 
    	
    	// and again with both as false - should be marked as closed
    	thisTask.Been_Approved__c = false;
    	thisTask.Been_Rejected__c = false;
    	Database.update(thisTask);    		       
    	rtn = Axiom_RibbonController.Dispatch(req); 

    	// Test Bad Id    	    	    	
    	req.id = '123456789';
    	rtn = Axiom_RibbonController.Dispatch(req);
    	
    	// Test Blank Id    	    	    	
    	req.id = '';
    	rtn = Axiom_RibbonController.Dispatch(req);
    	    	
    	Test.StopTest();
    }  
    
       static testMethod void testTaskClockstopper()
    {
    	Axiom_Utilities.generateTestData();
    	Test.StartTest();
    	
    	Task thisTask = [select id,OwnerId,isClosed from Task limit 1];
    	
    	Axiom_RibbonController.RibbonRequest req = new Axiom_RibbonController.RibbonRequest();
    	req.objname = 'Task';
    	req.action = 'Clockstopper';
    	req.id = thisTask.Id;
    	
		Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'test', Email='test@test.com', 
            EmailEncodingKey='UTF-8', LastName='Test', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='test123Akkk@axiomlawtest.com');            
        insert u;
        thisTask.OwnerId = u.Id;
        thisTask.Created_by_Template__c = 'Test';
        Database.update(thisTask);
        Axiom_RibbonController.RibbonResponse rtn = Axiom_RibbonController.Dispatch(req);  
		
		// change user back    	       
    	thisTask.OwnerId = UserInfo.getUserId();
        Database.update(thisTask);    		       
    	rtn = Axiom_RibbonController.Dispatch(req);     	
    	
    	// test isClosed
    	thisTask.Status = 'Completed';
        Database.update(thisTask);    		       
    	rtn = Axiom_RibbonController.Dispatch(req);     	
    	
    	// Test Bad Id    	    	    	
    	req.id = '123456789';
    	rtn = Axiom_RibbonController.Dispatch(req);
    	
    	// Test Blank Id    	    	    	
    	req.id = '';
    	rtn = Axiom_RibbonController.Dispatch(req);
    	
    	Test.StopTest();    	
    }  
}