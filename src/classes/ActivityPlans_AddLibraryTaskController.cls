public class ActivityPlans_AddLibraryTaskController {

    public list<taskTemplateWrapper> planTemplates{get;set;}
    public string searchText{get;set;}
    public id activityPlanId{get;set;}
    public Activity_Plan__c thisPlan{get;set;}
    public string selectAllString;
    public ActivityPlans_AddLibraryTaskController(){}
    public ActivityPlans_AddLibraryTaskController(ApexPages.StandardController controller) {
        thisPlan = (Activity_Plan__c) controller.getRecord();
        
        selectAllString = ActivityPlans_Utilities.buildSelectAllStatment('activity_plan_task_template__c');
        
        thisPlan = [select Name, 
                           Object_Types__c,
                           Active__c 
                    from Activity_Plan__c
                    where id = :thisPlan.id];
                     
        activityPlanId = thisPlan.id;

        list<Activity_Plan_Task_Template__c> templates = [select  id, 
                                                                 Name,
                                                                 Activity_Plan__c,
                                                                 Assigned_To_Type__c,
                                                                 Assigned_To_Value__c,
                                                                 Due_Date_Type__c,
                                                                 Due_Date_Value__c,
                                                                 Priority__c,
                                                                 Related_To_Type__c,
                                                                 Related_To_Value__c,
                                                                 Task_Status__c,
                                                                 Record_Type_Name__c,
                                                                 Allow_Duplicate_Tasks__c,
                                                                 Depends_On__c,
                                                                 Depends_On_Completion_Statuses__c,
                                                                 Depends_On__r.Name,
                                                                 Description__c,
                                                                 Notification_Email_Template__c,
                                                                 Task_Weight__c 
                                                          from Activity_Plan_Task_Template__c
                                                          where Activity_Plan__r.RecordType.Name = 'Template' and 
                                                                Activity_Plan__r.Object_Types__c =  :thisPlan.Object_Types__c
                                                          Order by name
                                                          limit 20];  
        planTemplates = taskTemplatesToWrapper(templates);                                                                      
    }

    public PageReference search() {
        planTemplates.clear();
        string objectTypes = thisPlan.object_types__c;
        String qry = 'select id, name, Priority__c, Depends_On__c, Task_Status__c,  Related_To_Value__c, Record_Type_Name__c, Due_Date_Value__c, Description__c from  Activity_Plan_Task_Template__c ' + 'where name LIKE \'%'+searchText+'%\' and Activity_Plan__r.RecordType.Name = \'Template\' and Activity_Plan__r.Object_Types__c =  :objectTypes order by name';
        list<activity_plan_task_template__c> templates = Database.query(qry);
        
        planTemplates = taskTemplatesToWrapper(templates);
        return null;
    }
      
    public pageReference save()
    {
        set<activity_plan_task_template__c> templateClones = new set<activity_plan_task_template__c>();
        map<id,activity_plan_task_template__c> sourceTemplateMap = new map<id,activity_plan_task_template__c>();
        
        //find which templates where added by the user and add them to our map.
        for(taskTemplateWrapper thisTemplate : planTemplates)
        {
            if(thisTemplate.isChecked)
            {
                sourceTemplateMap.put(thisTemplate.templateObject.Id,thisTemplate.templateObject);
            }
            
            thisTemplate.isChecked = false;
        }
        
        //use recursive function to ensure any depenencies are included. This will return a list of templates to be cloned with all depenencies included
        list<activity_plan_task_template__c>  sourceTemplates = getAllDependentTasks(sourceTemplateMap);
        
        //clone each template and add to list
        for(activity_plan_task_template__c thisTemplate : sourceTemplates)
        {
            activity_plan_task_template__c clonedTemplate = thisTemplate.clone(false,true,false,false);
            clonedTemplate.activity_plan__c = activityPlanId;
            templateClones.add(clonedTemplate);        
        }
        
        if(!templateClones.isEmpty())
        {
            system.debug('\n\n\n\n------------------- READY TO INSERT NEW TEMPLATES');
            integer counter = 1;
            for(activity_plan_task_template__c  thisTemplate : templateClones)
            {
                system.debug('\n\n\n\n START-------------------------------------START');
                system.debug('TEMPLATE ' + counter);
                system.debug(thisTEmplate);
                system.debug('\n\n\n\n  --------------------------------- ASSIGNED TO VALUE: ' + thisTEmplate.Assigned_To_Value__c);
                system.debug('\n\n\n\n STOP-------------------------------------STOP');
                counter++;
                
            }
            database.insert(new list<activity_plan_task_template__c>(templateClones),false);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,templateClones.size() + ' Templates Added to Activity Plan!'));
        }
        else
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,'Please Select Templates to Add'));
        }
        return null;
    }
    
    public list<activity_plan_task_template__c> getAllDependentTasks(map<id,activity_plan_task_template__c> sourceTemplates)
    {        
        system.debug('\n\n\n\n------------------- GET ALL DEPENDENT TASKS CALLED');
        //iterate all of our tasks
        boolean taskAdded = false;
        set<id> addedTemplateIds = new set<id>();
        for(activity_plan_task_template__c thisTemplate : sourceTemplates.values())
        {
            system.debug('\n\n\n\n-------------------- TEMPLATE DEPENDENT ON: ' + thisTemplate.Depends_On__c + ' TEMPLATE EXISTS IN MAP?: ' + sourceTemplates.containsKey(thisTemplate.Depends_On__c) );
            if(thisTemplate.Depends_On__c != null && !sourceTemplates.containsKey(thisTemplate.Depends_On__c))
            {
                system.debug('\n\n\n\n-------------------- FOUND MISSING DEPENDENT TASK!');
                addedTemplateIds.add(thisTemplate.Depends_On__c);
                taskAdded = true;
                
            }
        }
        
        system.debug('\n\n\n\n-------------- MISSING DEPENDENT TASK IDS: ' + addedTemplateIds);
        
        if(taskAdded && !addedTemplateIds.isEmpty())
        {
            string queryString = selectAllString + ' where id in :addedTemplateIds';
            
            list<activity_plan_task_template__c> templates = dataBase.query(queryString);
            
            for(activity_plan_task_template__c thisTemplate : templates)
            {
                system.debug('\n\n\n\n-------------------------- ADDING TASK TO PLAN VIA RECURSIVE FUNCTIOIN');
                system.debug(thisTemplate);
                sourceTemplates.put(thisTemplate.id,thisTemplate);
                getAllDependentTasks(sourceTemplates);
            }
            
        }

        return sourceTemplates.values();

    }
    
    public list<taskTemplateWrapper> taskTemplatesToWrapper(list<activity_plan_task_template__c> templates)
    {
        list<taskTemplateWrapper> templateWrappers = new list<taskTemplateWrapper>();
        for(activity_plan_task_template__c thisTemplate : templates)    
        {
            taskTemplateWrapper thisTemplateWrapper = new taskTemplateWrapper();
            thisTemplateWrapper.templateObject = thisTemplate;
            templateWrappers.add(thisTemplateWrapper);
        }      
        return templateWrappers;    
    }
    
    public class taskTemplateWrapper
    {
        public boolean isChecked{get;set;}
        public activity_plan_task_template__c templateObject{get;set;}
    }


}