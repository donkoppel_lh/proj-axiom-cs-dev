/******* Axiom Update Tasks Batch *********
@Author: Daniel Llewellyn/RedKite (dllewellyn@redkitetechnologies.com)
@Date: ~3/2014
@Description: Handles updating of a field on the task; number of days to first substantial response. Does this for any task where the isClosed flag is false. Meant to be scheduled hourly by the Axiom_Utilities.scheduleBatchJobs() method.
**********************************************/
global class Axiom_UpdateTaskBatch implements Database.Batchable < sObject > , Schedulable 
{
    /** Querystring that can be modified after class instantiation. Defaults to finding all tasks where the IsClosed flag is false. */
    public string queryString = 'select subject, whatId, id, createdDate, Days_Open__c,Status, Start_Time__c, Stop_Time__c, isClosed, First_Substantive_Response__c from Task where IsClosed = false or Stop_Time__c = null ALL ROWS';

      /** 
    * Finds next batch of records to send to the execute method in amounts dictated by the batch size (default 200)
    * @param ctx batch context. An instantiation of this class.
    * @return batch of records for the execute method to process
    **/   
    global Database.QueryLocator  start(Database.BatchableContext ctx)
    {
        return Database.getQueryLocator(queryString);
    }

    /**
    * Calls one method on the Axiom_TaskController class.
    * 1) Axiom_TaskController.updateMatterNumberofDaysToFSR to calculate the number of days to first substantial response.
    * see those methods for further description of their logic
    * @param ctx batch context. Is passed automatically by the system.
    * @param scope list of sObjects to operate on.
    * @return null
    **/ 
    global void execute(Database.BatchableContext ctx, List <sObject> scope)
    {       
        scope = Axiom_TaskController.setNumberOfDaysOpen_before(scope);        
        database.update(scope,false);  
        Axiom_TaskController.updateMatterNumberofDaysToFSR(scope);     
    }

    /**
    * Invoked after all batches have been completed. Nothing further needs to be done as this point so we 
    * just put an entry in the system log.
    **/
    global void finish(Database.BatchableContext ctx) 
    {
        System.debug(LoggingLevel.WARN,'Batch Process Finished');
      
    }    

    /**
    * This is the method is run when the scheduled job is triggered. It instantiates an instance of this
    * class, and executes the batch job.
    * @param sc SchedulableContext. Passed by salesforce when this scheduled job is triggered to run.
    * @return null
    **/
    global void execute(SchedulableContext SC) 
    {
        Axiom_UpdateTaskBatch updateBatch = new Axiom_UpdateTaskBatch();
        ID batchprocessid = Database.executeBatch(updateBatch, 50);
    }     
}