@isTest
global class Axiom_TestFileUploadController
{
    @isTest
    public static void testFileUploadController()
    {
        Axiom_Utilities.generateTestData();
        
        matter__c parentMatter = [select id, name from matter__c limit 1];
        
        Test.StartTest();
        PageReference pageRef = Page.Axiom_UploadFile;
        pageRef.getParameters().put('parent', parentMatter.id);

        Test.setCurrentPage(pageRef);   
                
        Axiom_FileUploadController controller = new Axiom_FileUploadController();
        controller.upload();
        
        Test.StopTest();
    }
    
}