@isTest
global class Axiom_TestUpdateRequestBatch
{
    public static testMethod void testUpdateRequestBatch()
    {
        Axiom_Utilities.generateTestData();
        Request__c testRequest = [select status__c from Request__c limit 1];
        testRequest.status__c = 'In Progress';
        update testRequest;
        
        Test.startTest();
        String CRON_EXP = '0 0 0 15 3 ? 2022';
        
        String jobId = System.schedule('ScheduleApexClassTest',
                        CRON_EXP, 
                        new Axiom_UpdateRequestBatch());        
        Test.stopTest();
 
    }
}